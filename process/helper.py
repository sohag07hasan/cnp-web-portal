import re, sys, time

#parse mgw licnese info to get overshoot
#DSP LICENSE; command in MGW
class MgwProcessor:
    def process_mml(self, filehandler ):
        lines = []
        nodes = []
        overshoots = []
        info = []
        try:
            sheet = filehandler.get_sheet()
            for row in sheet:
                lines.append(row[0])

            for i, line in enumerate(lines):
                try:
                    if line.find("DSP LICENSE:;") >= 0:
                        nodes.append(lines[i + 1])
                    elif line.find("Exceed times of license resources") >= 0:
                        overshoots.append(lines[i + 4])
                except:
                    pass

            for i, n in enumerate(nodes):
                dd = (re.sub('[^A-Z0-9]', '', n), re.sub('[^0-9]', '', overshoots[i]))
                info.append(dd)

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

        return info


#Citrix Output parser
class CitrixProcessor:

    #process Citrix output for 2G cell data preparation
    def process_citrix_output_bsc(self, filehandler):
        lines = []
        nodes = []
        cells = []
        final_data = [['Cell Name', 'BSC', 'LAC', 'Cell ID']]
        try:
            sheet = filehandler.get_sheet()
            for row in sheet:
                lines.append(row[0])

            bsc = ''
            for i, line in enumerate(lines):
                if line.find("*** Connected to") >= 0:
                    ll = line.split()
                    bsc = ll[3]

                elif line.find("CELL     CGI") >= 0:
                    ll = lines[i+1].split()
                    cell = ll[0]
                    try:
                        lc = ll[1].split('-')
                        lac =  int ( lc[2] )
                        cid =  int ( lc[3] )
                    except:
                        lac = 0
                        cid = 0

                    final_data.append( [cell, bsc, lac, cid] )
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        #filename = self.write_to_excel(final_data)
        return final_data

    #process 3G cell data
    def process_citrix_output_rnc(self, filehandler):
        final_data = [['Cell Name', 'RNC', 'LAC', 'RAC', 'URA']]
        interim_cell_data = {}
        interim_cell_ura = {}
        lines = []
        rnc = ""

        try:
            sheet = filehandler.get_sheet()
            for row in sheet:
                lines.append(row[0])

            for i, line in enumerate(lines):
                if line.find("Connected to") >=0 and line.find("SubNetwork=GPOSS_ROOT_MO_R,SubNetwork") >= 0:
                    try:
                        a = line.split(',')
                        rnc = a[1].split('=')[1]
                    except:
                        rnc = ''
                elif line.find("UtranCell=") >= 0 and line.find(" LocationArea=") >= 0 and line.find("RoutingArea="):
                    try:
                        a = line.split()
                        cell = a[0].split('=')[1]
                        bb = a[2].split(',')
                        interim_cell_data[cell] = [cell, rnc, int(bb[0].split('=')[1]), int(bb[1].split('=')[1])]
                        #final_data.append([cell, a[2].split('=')[1], a[3].split('=')[1]])
                    except:
                        print("Unexpected error:", sys.exc_info()[0])
                        raise
                elif line.find("UtranCell=") >= 0 and line.find("uraRef") >= 0:
                    try:
                        cell = line.split()[0].split("=")[1]
                        #ura = lines[i+1].split("=")[3]
                        interim_cell_ura[cell] = ['']
                    except:
                        print("Unexpected error:", sys.exc_info()[0])
                        raise
                else:
                    pass


            for cell, cell_data in interim_cell_data.items():
                if cell in interim_cell_ura:
                    accumulation = cell_data + interim_cell_ura[cell]
                else:
                    accumulation = cell_data + [""]

                final_data.append(accumulation)


        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

        return final_data