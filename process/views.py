from django.shortcuts import render
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import MML_DATA, CitrixData, SaveMMLDATA
from .helper import MgwProcessor, CitrixProcessor
from django.http import JsonResponse, HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
import django_excel as excel
from datetime import date
from report.models import Overshoot, SiteMeta
import sys


# Create your views here.
class ProcessMML(LoginRequiredMixin, View):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        context = {'title': 'MML Processor: MgW'}
        return render(request, 'process/index.html', {'form': MML_DATA, 'save_form': SaveMMLDATA, 'context': context})

    def post(self, request):
        context = {'title': 'MML Processor: MgW overshoot saving'}
        save_form = SaveMMLDATA(request.POST)
        if save_form.is_valid():
            ne_names = request.POST.getlist('ne_name')
            overshoot = request.POST.getlist('overshoot')
            for i in range(len(ne_names)):
                try:
                    mgw = Overshoot.objects.get( node_type = 'mgw', node_name = ne_names[i] )
                    mgw.overshoot_count = int(overshoot[i])
                    mgw.day = date.today()
                    mgw.save()
                except:
                    mgw = Overshoot(node_name=ne_names[i], node_type='mgw', overshoot_count=int(overshoot[i]), day=date.today())
                    mgw.save()

            #currently this meta value has no significant
            try:
                license = SiteMeta.objects.get(meta_key = 'mgw_license_update_time')
                license.meta_value = date.today().strftime('%Y-%m-%d')
                license.save()
            except:
                license = SiteMeta(meta_key='mgw_license_update_time', meta_value=date.today().strftime('%Y-%m-%d'))
                license.save()

        return render(request, 'process/mml-success.html', {'context': context})



#deal all the ajax
class ProcessAjax(View):
    def get(self, request):
        pass

    def post(self, request):
        data = {'is_valid': False}
        if request.POST.get('type') == 'mgw':
            form = MML_DATA(request.POST, request.FILES)
            if form.is_valid():
                info = MgwProcessor().process_mml(request.FILES['file'])
                data = { 'is_valid': True, 'info': info }

        response = JsonResponse(data)
        return response


#process Citrix
class ProcessCitrix(LoginRequiredMixin, View):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'
    def get(self, request):
        context = {'title': 'CITRIX Processor: Ericsson cell data'}
        return render(request, 'process/citrix.html', {'form': CitrixData, 'context': context})
        pass
    def post(self, request):

        form = CitrixData(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['type'] == 'bsc':
                filename = "Ericsson_Gcell_" + "{:%d_%m_%Y}".format(timezone.now())
                data = CitrixProcessor().process_citrix_output_bsc(request.FILES['file'])
            else:
                filename = "Ericsson_Ucell_" + "{:%d_%m_%Y}".format(timezone.now())
                data = CitrixProcessor().process_citrix_output_rnc(request.FILES['file'])

            return excel.make_response_from_array(data, 'xlsx', file_name=filename)
        else:
            return HttpResponseRedirect(reverse('process:citrix-process'))