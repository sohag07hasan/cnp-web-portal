from django.conf.urls import url
from .views import ProcessMML, ProcessAjax, ProcessCitrix

app_name = 'process'
urlpatterns = [
    url(r'^mml/$', ProcessMML.as_view(), name='mml-process'),
    #url(r'mml/submit/', ProcessMML.as_view(), name='mml-process-submit'),
    url(r'^citrix/$', ProcessCitrix.as_view(), name='citrix-process'),
    url(r'^ajax/$', ProcessAjax.as_view(), name='ajax-process'),
]