jQuery(document).ready(function ($) {


     function extracted_info(ajax_data, input_data){
        if (ajax_data.is_valid == true){
            alert(ajax_data.filename);
        }
     }

     //function to handle ajax
     function process_ajax(input_data, success_function){
        var csrftoken = Cookies.get('csrftoken');
        
        $.ajax({
            beforeSend: function () {
                $('#submit_button').val('Processing..');
                $('#submit_button').addClass('processing');
                $('#upload_progress').removeClass('display_none');
                $('#ajax_processing_message').removeClass('display_none');
            },

            url: AjaxProces.ajax_url,
            method: 'POST',
            dataType: 'json',
            cache: false,
            async: true,
            processData: false,
            contentType: false,
            data: input_data,
            headers:{'X-CSRFToken': csrftoken},
            success: function (ajax_data) {
                success_function(ajax_data, input_data);
            },
            error: function () {
                alert('Error: Please check excel file again and try!');
            },
            complete: function () {
                $('#submit_button').val('Start Parsing');
                $('#submit_button').removeClass('processing');
                $('#upload_progress').addClass('display_none');
                $('#ajax_processing_message').addClass('display_none');
                $('#ioform').trigger("reset");

            }
        });
     }


      $('#submit_button').click(function () {
          var files
          $('input[type=file]').on('change', prepareUpload);
          function prepareUpload(event){
             files = event.target.files;
          }
          var data = new FormData($('form#ioform')[0]);
          $.each(files, function (key, value) {
              data.append(key, value);
          });
          data.append('type', $('#id_type').val());
          process_ajax(data, extracted_info);

      });


     /** Making the parsed table sortable **/



});
