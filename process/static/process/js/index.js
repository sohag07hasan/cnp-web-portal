jQuery(document).ready(function ($) {


     function extracted_info(ajax_data, input_data){
        if (ajax_data.is_valid == true){
            tt = $('#overshoot_donwload');
            tt.html(''); //forcing populated result to be zero
            data = ajax_data.info;
            for (var i=0; i<data.length; i++){
                if( data[i][1] > 10 ){
                    cl =  "danger";
                }
                else{
                    cl = ""
                }
                row_count = i + 1;
                line = "<tr class='"+cl+"'><th scope='row'>" + row_count + "</th><td><input type='hidden' name='ne_name' value='"+data[i][0]+"'/>" +  data[i][0] + "</td><td><input type='hidden' name='overshoot' value='"+data[i][1]+"'/>" + data[i][1] + "</td></tr>";
                tt.append(line);
            }

            $('.result_shown_form').removeClass('display_none');
        }
     }

     //function to handle ajax
     function process_ajax(input_data, success_function){
        var csrftoken = Cookies.get('csrftoken');
        
        $.ajax({
            beforeSend: function () {
                $('#submit_button').val('Processing..');
                $('#submit_button').addClass('processing');
                $('#upload_progress').removeClass('display_none');
                $('#ajax_processing_message').removeClass('display_none');
            },

            url: AjaxProces.ajax_url,
            method: 'POST',
            dataType: 'json',
            cache: false,
            async: true,
            processData: false,
            contentType: false,
            data: input_data,
            headers:{'X-CSRFToken': csrftoken},
            success: function (ajax_data) {
                success_function(ajax_data, input_data);
            },
            error: function () {
                alert('Error: Please check excel file again and try!');
            },
            complete: function () {
                $('#submit_button').val('Start Parsing');
                $('#submit_button').removeClass('processing');
                $('#upload_progress').addClass('display_none');
                $('#ajax_processing_message').addClass('display_none');
                $('#ioform').trigger("reset");
                $('#save_button').removeClass('display_none');
            }
        });
     }


      $('#submit_button').click(function () {
          var files
          $('input[type=file]').on('change', prepareUpload);
          function prepareUpload(event){
             files = event.target.files;
          }
          var data = new FormData($('form#ioform')[0]);
          $.each(files, function (key, value) {
              data.append(key, value);
          });
          data.append('type', $('#id_type').val());
          process_ajax(data, extracted_info);

      });

      /** Saving the results **/
      $('#save_button').click(function () {
         return $('#overshoot_save_form').submit();
      });

});
