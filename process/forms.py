from django import forms

#hanling import and export

class MML_DATA(forms.Form):
    TYPES = ( ('mgw', 'MGW License'), )
    file = forms.FileField()
    type = forms.ChoiceField(choices=TYPES, required=True)


#Citrix data

class CitrixData(forms.Form):
    TYPES = (
        ('bsc', 'CITRIX Raw Data for BSC'),
        ('rnc', 'CITRIX Raw Data for RNC'),
    )
    file = forms.FileField()
    type = forms.ChoiceField(choices=TYPES, required=True)


#final data saving
class SaveMMLDATA(forms.Form):
    ne_name = forms.CharField(widget=forms.HiddenInput())
    overshoot = forms.CharField(widget=forms.HiddenInput())
