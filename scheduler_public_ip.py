import sys, requests, psycopg2, os
from pypac import PACSession # it is a library that uses system proxy for each request
from lxml import html
from datetime import date

sys.path.append(os.path.join(os.path.dirname(__file__), 'cnp')) # adding cnp folder/project
os.environ["DJANGO_SETTINGS_MODULE"] = "cnp.settings" # cnp settings

import django
from django.conf import settings
django.setup()
from igw.models import PublicIP


class PublicIPInfo:

    #constructor functions
    def __init__(self, proxy = None):
        if proxy != 'n':
            self.set_proxy()
        self.set_looking_glass()

    #GP Office lan needs proxy to surf in internet
    #this is why proxy is being added
    def set_proxy(self):
        os.environ["HTTPS_PROXY"] = "10.10.20.49:3828"
        os.environ["HTTP_PROXY"] = "10.10.20.49:3828"

    def set_looking_glass(self):
        self.url = 'https://www.as13030.net/looking-glass.php'
        self.post = 'https://www.as13030.net/looking-glass.php'
        self.routerid = '10' # 10 means RR Zurich
        self.requestid = '40' # 20 means ssh ip bgp_path
        self.argument = '' #this is userinput ip/mask
        self.xpath = '//*[@id="main-content"]/div/div[2]/div[3]/div/div/div/div/div/pre'


    def start_updating(self):
        public_ips = PublicIP.objects.all()

        try:
            s = PACSession()
            login = s.get(self.url)
            login_html = html.fromstring(login.text)
            hidden_inputs = login_html.xpath(r'//form//input[@type="hidden"]')
            form = {x.attrib["name"]: x.attrib["value"] for x in hidden_inputs}
            form['routerid'] = self.routerid
            form['requestid'] = self.requestid

            for public_ip in public_ips:
                form['argument'] = "{0}/{1}".format(public_ip.ip, public_ip.subnet)
                try:
                    response = s.post( self.post, data = form)
                    if response.status_code == 200:
                        info = self.extract_bgp_info(response.content)
                        if '24389' in info['bgp_path']:
                            public_ip.bgp_path = info['bgp_path']
                            public_ip.bgp_meta = info['bgp_meta']
                            public_ip.modified_date = date.today()
                            public_ip.save()
                    else:
                        print('Error found')
                except:
                    print("Unexpected error_2:", sys.exc_info()[0])
        except:
            print("Unexpected error_1:", sys.exc_info()[0])

    #extract required html to get bgp information
    def extract_bgp_info(self, content):
        return_content = {'bgp_path': '', 'bgp_meta': ''}
        response_html = html.fromstring(content)
        bgp_info = response_html.xpath(r'//pre[@class="border"]/text()') #getting xlookup value

        if len(bgp_info) > 0:
            return_content['bgp_meta'] = bgp_info[0]
            info = bgp_info[0].split('\r\n')
            if '24389' in info[3]:
                return_content['bgp_path'] = info[3].strip()

        return return_content


if __name__ == "__main__":
    try:
        proxy = sys.argv[1]
    except:
        proxy = None

    PIP = PublicIPInfo(proxy)
    PIP.start_updating()
