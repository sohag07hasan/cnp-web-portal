from django.db import models
from django.utils import timezone
from datetime import date, timedelta


#hourly traffic model
class HourlyTraffic( models.Model ):
    datetime =  models.DateTimeField(default=timezone.now, blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    traffic = models.FloatField(default=0)

    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


#summary traffic model
class SummaryTraffic( models.Model ):
    day = models.DateField(blank=True)
    hlr_subscriber = models.IntegerField(default=0)
    vlr_registered = models.IntegerField(default=0)
    vlr_active = models.IntegerField(default=0)
    bh_traffic_erl = models.FloatField(default=0)
    bh_call_attempt = models.IntegerField(default=0)
    hw_capacity_erl = models.FloatField(default=0)
    sw_capacity_erl = models.FloatField(default=0)
    forecast_erl = models.FloatField(default=0)


#GGSN Volumne traffic
class GiTraffic(models. Model):
    datetime = models.DateTimeField(default=timezone.now, blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    ne_name = models.CharField(max_length=500)
    uwg_function = models.CharField(max_length=500)
    gi_traffic = models.FloatField(default=0)       #TB
    gi_throughput = models.FloatField(default=0)    #MB/s ( megabyte per seconds )
    sgi_traffic = models.FloatField(default=0)      #TB
    sgi_throughput = models.FloatField(default=0)  # MB/s ( megabyte per seconds )

    def __str__(self):
        return self.ne_name


#Overshoot of Core Nodes
class Overshoot(models.Model):
    NODE_TYPE = (('mss', 'MSS'), ('mgw', 'MGW'))
    node_name = models.CharField(max_length=500, unique=True)
    node_type = models.CharField(max_length=500, choices=NODE_TYPE, default='mss')
    overshoot_count = models.IntegerField(default=0)
    day = models.DateField(blank=True)
    def __str__(self):
        return self.node_name


#Meta information and corresponding values
class SiteMeta(models.Model):
    meta_key = models.CharField(max_length=500, default=None, blank=True, null=True)
    meta_value = models.CharField(max_length=500, default=None, blank=True, null=True)
    remarks = models.CharField(max_length=500, default=None, blank=True, null=True)