from django.contrib import admin
from .models import HourlyTraffic, SummaryTraffic, GiTraffic, Overshoot, SiteMeta


## Hourly Traffic auto parsed from CSM_SOC portal
class HourlyTrafficAdmin( admin.ModelAdmin ):
    list_display = ('datetime', 'day', 'hour', 'min', 'traffic')
    search_fields = ['datetime', 'day']


#Summary Traffic
class SummaryTrafficAdmin( admin.ModelAdmin ):
    list_display = ( 'day', 'hlr_subscriber', 'vlr_registered', 'vlr_active', 'bh_traffic_erl', 'hw_capacity_erl', 'sw_capacity_erl', 'forecast_erl', 'bh_call_attempt' )
    search_fields = ['day']


#GGSN Volumen & Throughput
class GiTrafficAdmin( admin.ModelAdmin ):
    list_display = ('datetime', 'day', 'hour', 'min', 'ne_name', 'uwg_function', 'gi_traffic', 'gi_throughput', 'sgi_traffic', 'sgi_throughput')
    search_fields = ['day', 'ne_name', 'uwg_function']


#Overshoot of core Nodes
class OvershootAdmin( admin.ModelAdmin ):
    list_display = ('node_name', 'node_type', 'overshoot_count', 'day')
    search_fields = ['node_name', 'node_type', 'overshoot_count', 'day']

#Site Meta Amdin page
class SiteMetaAdmin( admin.ModelAdmin ):
    list_display = ('meta_key', 'meta_value', 'remarks')
    search_fields = ['meta_key', 'meta_value', 'remarks']


admin.site.register(HourlyTraffic, HourlyTrafficAdmin)
admin.site.register(SummaryTraffic, SummaryTrafficAdmin)
admin.site.register(GiTraffic, GiTrafficAdmin)
admin.site.register(Overshoot, OvershootAdmin)
admin.site.register(SiteMeta, SiteMetaAdmin)
