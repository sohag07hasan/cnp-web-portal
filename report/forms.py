from django import forms

#handle data import to database
TYPES = (
    ('summarytraffic', 'Summary Traffic'),
    ('gitraffic', 'Gi Traffic'),
    ('15_min_traffic', '15 Minutes Netwok Traffic'),
)
class DataIO( forms.Form ):
    file = forms.FileField()
    type = forms.ChoiceField(choices=TYPES, required=True)
