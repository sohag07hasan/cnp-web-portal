# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-09-10 10:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0009_auto_20170718_1709'),
    ]

    operations = [
        migrations.CreateModel(
            name='Overshoot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('node_name', models.CharField(max_length=500, unique=True)),
                ('node_type', models.CharField(choices=[('MSS', 'mss'), ('MGW', 'mgw')], default='mss', max_length=500)),
                ('overshoot_count', models.IntegerField(default=0)),
                ('day', models.DateField(blank=True)),
            ],
        ),
    ]
