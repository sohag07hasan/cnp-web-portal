# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-05-22 07:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0004_auto_20170522_1341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hourlytraffic',
            name='day',
            field=models.DateField(blank=True),
        ),
    ]
