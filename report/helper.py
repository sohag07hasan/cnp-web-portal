from datetime import date, timedelta, datetime
from django.utils import timezone
from .models import HourlyTraffic, SummaryTraffic, GiTraffic, Overshoot
from django.db.models import Max, Sum, Count, Avg, F
import sys, time, dateparser
#from datetime import datetime

class QueryTraffic:
    def get_hourly_traffic(self, day=None):
        if day is None:
            day = timezone.now() - timedelta(days=1, hours=-6)
            day = day.strftime("%Y-%m-%d")

        try:
            results = HourlyTraffic.objects.raw("SELECT avg(traffic) as id, hour from report_hourlytraffic where day = %s group by 2  order by 2", [day])
            #results = HourlyTraffic.objects.raw("SELECT avg(traffic) as id, DATE_TRUNC('hour', datetime) as hour from report_hourlytraffic where datetime >= %s AND datetime <= %s group by 2  order by 2", [day1, day2])
            traffic = []
            hour = []
            for r in results:
                traffic.append(round(r.id/1000, 3))
                hour.append(str(r.hour) + ':00')

            data = {
                'hour': hour,
                'traffic': traffic,
                'day': day,
            }

        except:
            data = {}

        return data

    def get_recent_days(self, limit=15):
        all_days = []
        try:
            days = HourlyTraffic.objects.values('day').distinct('day').order_by('-day')
            for d in days:
                if timezone.now().strftime("%Y-%m-%d") == d.get('day').strftime("%Y-%m-%d"):
                    continue #removing today's date

                dd = {
                    'date': d.get('day').strftime("%Y-%m-%d"),
                    'day': d.get('day').strftime("%B %d, %Y")
                }
                all_days.append(dd)
        except:
            pass

        return all_days




#heps to query traffic
class TrafficHelper:
    traffic_columns = ['Row Labels', 'Max of Period (min)', 'Sum of Seizure Traffic (Erl)', 'Sum of Seizure Traffic (Erl)2']

    def get_hourly_traffic(self, day=None):
        if day is None:
            day = timezone.now() - timedelta(days=1, hours=-6)
            day = day.strftime("%Y-%m-%d")

        try:
            results = HourlyTraffic.objects.raw("SELECT avg(traffic) as id, hour from report_hourlytraffic where day = %s group by 2  order by 2", [day])
            traffic = []
            hour = []
            for r in results:
                traffic.append(round(r.id/1000, 3))
                hour.append(str(r.hour) + ':00')

            data = {
                'hour': hour,
                'traffic': traffic,
                'day': day,
            }
        except:
            data = {}

        return data


    #get daily traffic
    def get_daily_traffic(self, day1=None, day2=None):
        data = {}
        day, hour, traffic = [], [], []
        try:
            results = HourlyTraffic.objects.values('day').filter(day__range=(day1, day2)).order_by('day').distinct('day')
            for r in results:
                try:
                    info = HourlyTraffic.objects.values('hour').filter(day=r.get('day')).annotate(avg_traffic=Avg('traffic')).order_by('-avg_traffic')[:1]
                    day.append(r.get('day').strftime("%b %d"))
                    hour.append(info[0].get("hour"))
                    traffic.append(round(float(info[0].get('avg_traffic')) / 1000, 2))
                except:
                    pass
            data['hour'] = hour
            data['throughput'] = traffic
            data['day'] = day
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data




    #calling minutes
    def get_calling_minutes(self, type=None, day1=None, day2=None):
        final_data = {'day': [], 'sum_min': [], 'max_min': [], 'bh_contribution': [], 'from': day1, 'to': day2}

        try:
            #results = GiTraffic.objects.values('day').filter(day__range=(day1, day2)).annotate(sum_gi=Sum('gi_traffic')).order_by('day')
            results = HourlyTraffic.objects.values('day').filter(day__range=(day1, day2)).annotate( sum_min=Sum('traffic'), sum_count=Count('traffic') ).order_by('day')
            for r in results:

                #data is available in database 15 minutes interval. So per hour record qty is 4
                # So per day total records =  24*4 = 96
                if r['sum_count'] >= 96:
                    final_data['day'].append(r['day'].strftime("%b %d"))
                    sum_min = float(r['sum_min']) * 15 / 1000000 # converting into Millions and 15 = 60/4
                    final_data['sum_min'].append(round( sum_min, 2 ))
                    max_min = HourlyTraffic.objects.values('hour').filter(day=r['day']).annotate( sum_min=Sum('traffic')).order_by('-sum_min' )[:1]
                    final_max_min = float(max_min[0]['sum_min']) * 15 / 1000000 # converting inot millions and 15 = 60/4
                    final_data['max_min'].append(round(final_max_min, 2))
                    final_data['bh_contribution'].append(round( (final_max_min / sum_min)*100, 2))
        except:
            pass
        return final_data

    #saving houly traffic
    def save_15_mins_traffic( self, filehandler ):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)  # removing spaces
        created = 0
        failure = 0
        updated = 0
        index = 1
        failed_rows = []
        is_valid = True


        for row in sheet:
            if index == 1:
                col = 0
                for col_name in self.traffic_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if column is found valid, then it will delte all the existing records
                if is_valid == False:
                    break
            else:
                #due_date = datetime.strptime(row[0], '%Y-%m-%d').strftime('%Y-%m-%d')
                try:
                    if int(row[1]) == 15:
                        due_datetime = dateparser.parse(row[0])
                        defaults = {
                            'datetime': due_datetime.strftime('%Y-%m-%d %H:%M:%S'),
                            'day': due_datetime.strftime('%Y-%m-%d'),
                            'hour': due_datetime.strftime('%H'),
                            'min': due_datetime.strftime('%M'),
                            'traffic': float(row[2]) + float(row[3])
                        }
                        try:
                            obj = HourlyTraffic.objects.get( day = defaults['day'], hour = defaults['hour'], min = defaults['min'] )
                            obj.gi_traffic = defaults['traffic']
                            obj.save()
                            updated += 1
                        except HourlyTraffic.DoesNotExist:
                            obj = HourlyTraffic( **defaults )
                            obj.save()
                            created += 1
                        except:
                            failure += 1
                            failed_rows.append(index)
                    else:
                        failure += 1
                        failed_rows.append(index)
                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    raise

            index += 1 #index increasing

        return { 'created': created, 'updated': updated, 'failure': failure, 'failed_rows': failed_rows }

    # cleaning unwanted characters from sheet
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v



#helps views to save and query data from Summary Traffic Model
class SummaryTrafficHelper:

    summary_columns = ["Date", "HLR Subscriber", "VLR Registered Subscriber", "VLR Active Subscriber", "Busy Hour A-Interface Traffic (Er)", "Busy Hour Call Attempt", "HW Capacity", "SW Capacity", "Forecast"]

    def save_traffic_data(self, filehandler ):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func) #removing spaces
        created = 0
        failure = 0
        updated = 0
        index = 1
        failed_rows = []
        is_valid = True

        for row in sheet:
            if index == 1:
                col = 0
                for col_name in self.summary_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if colum is found valid, then it will delte all the existing records
                if is_valid == False:
                    break

            elif (len(row[0]) > 4):
                try:
                    #due_date = datetime.strptime(row[0], '%Y-%m-%d').strftime('%Y-%m-%d')
                    due_date = dateparser.parse(row[0]).strftime('%Y-%m-%d')
                    defaults = {
                        'hlr_subscriber': int(row[1]),
                        'vlr_registered': int(row[2]),
                        'vlr_active': int(row[3]),
                        'bh_traffic_erl': float(row[4]),
                        'bh_call_attempt': int(row[5]),
                        'hw_capacity_erl': float(row[6]),
                        'sw_capacity_erl': float(row[7]),
                        'forecast_erl': float(row[8])
                    }
                    try:
                        obj = SummaryTraffic.objects.get(day = due_date)
                        for key, value in defaults.items():
                            setattr(obj, key, value)
                        obj.save()
                        updated += 1
                    except SummaryTraffic.DoesNotExist:
                        new_values = {'day': due_date}
                        new_values.update(defaults)
                        obj = SummaryTraffic( **new_values )
                        obj.save()
                        created += 1
                    except:
                        failure += 1
                        failed_rows.append(index)

                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    raise
                    failure += 1
                    failed_rows.append(index)
            index += 1

        return { 'created': created, 'updated': updated, 'failure': failure, 'failed_rows': failed_rows }

    #cleaning unwanted characters from sheet
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v

    #return summary data
    #@type is the main key by which it will return data
    def get_summary_data(self, type=None, start_day=None, end_day=None):
        return self.generate_summary_data(type, start_day, end_day)


    #this will generate summary data based on query parameters
    def generate_summary_data(self, type=None, day1=None, day2=None ):

        if type == "traffic_and_cap_trend":
            final_data = {'day': [], 'bh_traffic': [], 'hw_capacity': [], 'sw_capacity': [], 'forecast': []}
            results = SummaryTraffic.objects.values( 'day', 'bh_traffic_erl', 'hw_capacity_erl', 'sw_capacity_erl', 'forecast_erl' ).filter(day__range=( day1, day2 )).order_by('day')
            if len( results ) > 0:
                for r in results:
                    final_data['day'].append( r['day'].strftime("%b %d") )
                    final_data['bh_traffic'].append( round ( float(r['bh_traffic_erl']) / 1000, 3 ) ) #Erl to kErl
                    final_data['hw_capacity'].append( round( float(r['hw_capacity_erl']) / 1000, 3 ) ) #Erl to kErl
                    final_data['sw_capacity'].append( round(  float(r['sw_capacity_erl']) / 1000, 3) ) #Erl to kErl
                    final_data['forecast'].append( round( float(r['forecast_erl']) /1000,3 ) ) #Erl to kErl

        elif type == 'subscriber_trend':
            final_data = {'day': [], 'vlr_registered': [], 'vlr_active': []}
            results = SummaryTraffic.objects.values( 'day', 'vlr_registered', 'vlr_active' ).filter(day__range=( day1, day2 )).order_by('day')
            if len( results ) > 0:
                for r in results:
                    final_data['day'].append( r['day'].strftime("%b %d") )
                    final_data['vlr_registered'].append( r['vlr_registered'] / 1000000) #converting to millions
                    final_data['vlr_active'].append( r['vlr_active'] / 1000000 ) #converting to millions

        elif type == 'mhts':
            final_data = {'day': [], 'mhts': []}
            results = SummaryTraffic.objects.values('day', 'bh_call_attempt', 'bh_traffic_erl').filter(day__range=(day1, day2)).order_by('day')
            if len( results ) > 0:
                for r in results:
                    final_data['day'].append(r['day'].strftime("%b %d"))
                    final_data['mhts'].append( round( float(r['bh_traffic_erl']) / int(r['bh_call_attempt']) * 3600, 3)  )

        elif type == 'merl_and_bhca_per_sub':
            final_data = { 'day': [], 'merl_sub': [], 'bhca_sub': [] }
            results = results = SummaryTraffic.objects.values('day', 'vlr_active', 'bh_traffic_erl', 'bh_call_attempt').filter(day__range=(day1, day2)).order_by('day')
            if len( results ) > 0:
                for r in results:
                    final_data['day'].append(r['day'].strftime("%b %d"))
                    final_data['merl_sub'].append( round( float(r['bh_traffic_erl']) / int(r['vlr_active']) * 1000, 3 ) )
                    final_data['bhca_sub'].append( round( float(r['bh_call_attempt']) / int(r['vlr_active']), 3 ) )

        else:
            final_data = {}
        return final_data


#GGSN Throughput helper
class GGSNThroughputHelper:

    ggsn_columns = ["Start Time", "Period (min)", "NE Name", "UGW Function", "Gi traffic in MB (MB)", "Gi peak throughput in MB/s (MB/s)", "SGi downlink user traffic in KB (kB)", "SGi uplink user traffic in KB (kB)", "SGi downlink user traffic peak throughput in KB/s (kB/s)", "SGi uplink user traffic peak throughput in KB/s (kB/s)"]

    #saving U2000 dump at database
    def save_gi_traffic(self, filehandler):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)  # removing spaces
        created = 0
        failure = 0
        updated = 0
        index = 1
        failed_rows = []

        is_valid = True

        for row in sheet:
            if index == 1:
                col = 0
                for col_name in self.ggsn_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        #print(row[col].strip().upper() + " " + col_name.upper())
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if column is found valid, then it will delte all the existing records
                if is_valid == False:
                    break
            else:
                try:
                    #due_date = datetime.strptime(row[0], '%Y-%m-%d').strftime('%Y-%m-%d')
                    try:
                        #if int(row[1]) != 60: break #counter might need to be 60 minutes
                        #From August 2018 all counters are allowed

                        due_datetime = dateparser.parse(row[0])
                        defaults = {
                            'datetime': due_datetime.strftime('%Y-%m-%d %H:%M:%S'),
                            'day': due_datetime.strftime('%Y-%m-%d'),
                            'hour': due_datetime.strftime('%H'),
                            'min': due_datetime.strftime('%M'),
                            'ne_name': row[2],
                            'uwg_function': row[3],
                            'gi_traffic': float(row[4])/1000000, #savng in TB
                            'gi_throughput': float(row[5]), #saving in MB/s
                            'sgi_traffic': ( float(row[6]) + float(row[7]) )/1000000000, #savng in TB
                            'sgi_throughput': ( float(row[8]) + float(row[9]) )/1000 #saving in MB/s
                        }
                    except:
                        continue

                    try:
                        obj = GiTraffic.objects.get( day = defaults['day'], hour = defaults['hour'], min = defaults['min'], ne_name = defaults['ne_name'], uwg_function=defaults['uwg_function'] )
                        obj.gi_traffic = defaults['gi_traffic']
                        obj.gi_throughput = defaults['gi_throughput']
                        obj.sgi_traffic = defaults['sgi_traffic']
                        obj.sgi_throughput = defaults['sgi_throughput']
                        obj.save()
                        updated += 1
                    except GiTraffic.DoesNotExist:
                        obj = GiTraffic( **defaults )
                        obj.save()
                        created += 1
                    except:
                        failure += 1
                        failed_rows.append(index)

                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    raise
                    failure += 1
                    failed_rows.append(index)
            index += 1

        return { 'created': created, 'updated': updated, 'failure': failure, 'failed_rows': failed_rows }


    #return data as ajax response
    #now both Gi and SGi traffic
    def get_data_volume(self, type, day1=None, day2=None):
        final_data = {'day': [], 'sum_gi': [], 'max_gi': [], 'bh_contribution':[], 'from': day1, 'to': day2}

        try:
            if type == 'gi_traffic_sgi': #4G traffic
                results = GiTraffic.objects.values('day').filter(day__range=(day1, day2)).annotate(volume=Sum('sgi_traffic'), count_gi=Count('sgi_traffic')).order_by('day')
                #print(len(results))
            elif type == 'gi_traffic_gi': #2G+4G traffic
                results = GiTraffic.objects.values('day').filter(day__range=(day1, day2)).annotate(volume=Sum('gi_traffic'), count_gi=Count('gi_traffic')).order_by('day')
                #print(len(results))
            else: #2G+3G+4G
                results = GiTraffic.objects.values('day').filter(day__range=(day1, day2)).annotate(sum_gi=Sum('gi_traffic'), sum_sgi=Sum('sgi_traffic'), count_gi=Count('gi_traffic')).annotate(volume=F('sum_gi') + F('sum_sgi')).order_by('day')
                #print(len(results))

            for r in results:
                # total node 2 so per hour there will be 2 records. So for 24 hours records will be 48
                # If any has less then 48  records this will filter it
                if r['count_gi'] >= 48:
                    final_data['day'].append(r['day'].strftime("%b %d"))
                    final_data['sum_gi'].append(round( r['volume'], 2 ))

                    if type == 'gi_traffic_sgi':
                        max_gi = GiTraffic.objects.values('hour').filter(day=r['day']).annotate(volume=Sum('sgi_traffic')).order_by('-volume')[:1]
                    elif type == 'gi_traffic_gi':
                        max_gi = GiTraffic.objects.values('hour').filter(day=r['day']).annotate(volume=Sum('gi_traffic')).order_by('-volume')[:1]
                    else:
                        max_gi = GiTraffic.objects.values('hour').filter(day = r['day']).annotate(sum_gi=Sum('gi_traffic'), sum_sgi=Sum('sgi_traffic')).annotate(volume=F('sum_gi')+F('sum_sgi')).order_by('-volume')[:1]

                    final_max_gi = max_gi[0]['volume']
                    final_data['max_gi'].append(round( final_max_gi, 2 ))
                    final_data['bh_contribution'].append( round( float(final_max_gi)/float(r['volume']) * 100, 2 ) )
        except:
            pass
        return final_data

    #returns hourly throughput for one day
    #This fucntion will work for any count( 5 minutes, 30 minutes, 60 minutes)
    #this fucntion returns both gi & sgi throughput
    def get_hourly_throughput(self, day=None):
        if day is None:
            day = timezone.now() - timedelta(days=1, hours=-6)
            day = day.strftime("%Y-%m-%d")

        data = {}
        hour, throughput = [], []
        initial_data = {}
        try:
            results = GiTraffic.objects.values('hour', 'ne_name').filter(day=day).annotate(gi=Max('gi_throughput'), sgi=Max('sgi_throughput')).annotate(th=F('gi')+F('sgi')).order_by('hour')
            for r in results:
                # hour.append( r.get("hour") )
                h = int(r.get("hour"))
                if h not in initial_data.keys():
                    initial_data[h] = 0
                initial_data[h] += float(r.get('th'))

            for key in sorted(initial_data):
                hour.append(str(key) + ':00')
                throughput.append(round(initial_data[key]* 8/1000, 2))

            data['hour'] = hour
            data['throughput'] = throughput
            data['day'] = day
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data


    #sgi means 4G throughput
    def get_hourly_sgi_throughput(self, day=None):
        if day is None:
            day = timezone.now() - timedelta(days=1, hours=-6)
            day = day.strftime("%Y-%m-%d")

        data = {}
        hour, throughput = [], []
        initial_data = {}
        try:
            results = GiTraffic.objects.values('hour', 'ne_name').filter(day = day).annotate(sgi=Max('sgi_throughput')).order_by('hour')
            for r in results:
                h = r.get("hour")
                if h not in initial_data.keys():
                    initial_data[h] = 0
                initial_data[h] += float(r.get('sgi'))

            for key in sorted(initial_data):
                hour.append(str(key) + ':00')
                throughput.append(round(initial_data[key]* 8/1000, 2))

            data['hour'] = hour
            data['throughput'] = throughput
            data['day'] = day
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data

    #return daily peak throughput for a date range
    #Logic: SUM of ( hourly max gi for Each GGSN + hourly max sgi for Each GGSN )
    def get_daily_peak_throughput(self, day1=None, day2=None):

        data = {}
        day, hour, throughput = [], [], []

        try:
            results = GiTraffic.objects.values('day').filter(day__range=(day1, day2)).order_by('day').distinct('day')
            for r in results:
                try:
                    info = GiTraffic.objects.values('hour', 'ne_name').filter(day=r.get('day')).annotate(gi=Max('gi_throughput'), sgi=Max('sgi_throughput')).annotate(th=F('gi')+F('sgi')).order_by('hour')
                    initial_data = {}
                    for rr in info:
                        # hour.append( r.get("hour") )
                        h = int(rr.get("hour"))
                        if h not in initial_data.keys():
                            initial_data[h] = 0
                        initial_data[h] += float(rr.get('th'))

                    items = [ (v, k) for k, v in initial_data.items() ]
                    items.sort()
                    items.reverse()
                    #print(items[0])
                    day.append(r.get('day').strftime("%b %d"))
                    hour.append(items[0][1])
                    throughput.append(round( items[0][0] * 8 / 1000, 2))
                except:
                    pass
            data['hour'] = hour
            data['throughput'] = throughput
            data['day'] = day
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data


    # sgi interface is for LTE
    # retruns daily peak SGI traffic for a date range
    def get_daiy_peak_sgi_throughput(self, day1=None, day2=None):

        data = {}
        day, hour, throughput = [], [], []
        try:
            results = GiTraffic.objects.values('day').filter(day__range=(day1, day2)).order_by('day').distinct('day')
            for r in results:
                try:
                    info = GiTraffic.objects.values('hour', 'ne_name').filter(day=r.get('day')).annotate( sgi=Max('sgi_throughput')).order_by('hour')
                    initial_data = {}
                    for rr in info:
                        # hour.append( r.get("hour") )
                        h = int(rr.get("hour"))
                        if h not in initial_data.keys():
                            initial_data[h] = 0
                        initial_data[h] += float(rr.get('sgi'))

                    #sorting
                    items = [ (v, k) for k, v in initial_data.items() ]
                    items.sort()
                    items.reverse()

                    day.append(r.get('day').strftime("%b %d"))
                    hour.append(items[0][1])
                    throughput.append(round( items[0][0] * 8 / 1000, 2))
                except:
                    pass
            data['hour'] = hour
            data['throughput'] = throughput
            data['day'] = day
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data


    # cleaning unwanted characters from sheet
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v


#overshoot calcuation
class NodeOvershootHelper:
    def get_overshoots(self, type):
        final_data = {'nodes':[], 'overshoots': [], 'overshoot_limit': []}
        try:
            data = Overshoot.objects.filter(node_type = type).order_by('node_name')
            day = Overshoot.objects.values('day').order_by('-day')[:1]
            final_data['day'] = day[0].get('day').strftime('%Y-%m-%d')
            if len(data) > 0:
                for d in data:
                    final_data['nodes'].append(d.node_name)
                    final_data['overshoots'].append(d.overshoot_count)
                    final_data['overshoot_limit'].append(30)

        except:
            pass
        return final_data