from django.shortcuts import render
from django.views import View
from .helper import QueryTraffic, SummaryTrafficHelper, TrafficHelper, GGSNThroughputHelper, NodeOvershootHelper
import json, sys
from django.core.serializers.json import DjangoJSONEncoder
from datetime import date, timedelta, datetime
from django.utils import timezone
from .forms import DataIO
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse


# Create your views here.

#Home page of traffic ( ../report/ )
class Traffic( View, QueryTraffic):

    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):

        if (request.is_ajax()):
            if request.GET.get('type', None) == 'hourly_traffic':
                day = request.GET.get('day', None)
                data = TrafficHelper().get_hourly_traffic(day)
            else:
                pass

            return JsonResponse(data, safe=False)

        # if not ajax then it will proceed
        try:
            day = request.GET.get('day')
            day = day.strip()
            traffic = self.get_hourly_traffic(day)
        except:
            day = ''
            traffic = self.get_hourly_traffic()

        context = {
            'title': 'CNP Report: Hourly Traffic'
        }
        return render(request, 'report/traffic.html', {'context': context})


class HrTraffic( View, QueryTraffic ):

    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        try:
            if request.GET.get('day') is None:
                day = timezone.now() - timedelta(days=1, hours=-6)
                day = day.strftime("%Y-%m-%d")
            else:
                day = request.GET.get('day').strip()

            traffic = self.get_hourly_traffic(day)
            gi_throuput = GGSNThroughputHelper().get_hourly_throughput(day) #2G+3G+4G
            sgi_throughput = GGSNThroughputHelper().get_hourly_sgi_throughput(day) #only 4G
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

        context = {
            'title': 'CNP Report: Hourly Traffic',
            'day1': json.dumps(traffic, cls=DjangoJSONEncoder),
            'throughput': json.dumps(gi_throuput, cls=DjangoJSONEncoder),
            'sgi_throughput': json.dumps(sgi_throughput, cls=DjangoJSONEncoder),
            #'recent_days': self.get_recent_days(),
            'selected_day': day
        }
        return render(request, 'report/hourly_traffic.html', {'context': context})

#summary core traffic
class SummaryTraffic( View, SummaryTrafficHelper ):

    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        try:
            startDate = datetime.strptime( request.GET.get('from', ''), '%Y-%m-%d' )
            endDate = datetime.strptime( request.GET.get('to', ''), '%Y-%m-%d')
        except:
            endDate = date.today()
            startDate = endDate - timedelta(30)

        if(request.is_ajax()):
            data = self.get_summary_data(request.GET.get('type', None), startDate, endDate)
            return JsonResponse(data, safe=False)
        else:
            context = {
                'title': 'CNP Report: Summary Traffic',
                'startDate': startDate.strftime('%Y-%m-%d'),
                'endDate': endDate.strftime('%Y-%m-%d'),
            }
            return render( request, 'report/summary_traffic.html', {'context': context} )


#import & export data for rport
class ImportExport( LoginRequiredMixin, View ):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    #Inital page to import and export
    def get( self, request ):
        context = {
            'title': 'CNP Report: IO',
            'form': DataIO
        }
        return render( request, 'report/io.html', {'context': context} )


    #This function is called uring ajax requests while uploading raw data
    def post( self, request ):
        form = DataIO( request.POST, request.FILES )
        data = {}
        if form.is_valid():
            if form.cleaned_data['type'] == 'summarytraffic':
                data = SummaryTrafficHelper().save_traffic_data( request.FILES['file'] )
                data['is_valid'] = True
            elif form.cleaned_data['type'] == 'gitraffic':
                data = GGSNThroughputHelper().save_gi_traffic( request.FILES['file'] )
            elif form.cleaned_data['type'] == '15_min_traffic':
                data = TrafficHelper().save_15_mins_traffic( request.FILES['file'] )
            else:
                data['is_valid'] = False
        else:
            data['is_valid'] = False

        return JsonResponse(data)



#GGSN Throughput
class ThroughputVolume( View ):

    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        try:
            startDate = datetime.strptime(request.GET.get('from', ''), '%Y-%m-%d')
            endDate = datetime.strptime(request.GET.get('to', ''), '%Y-%m-%d')
        except:
            endDate = date.today() - timedelta(1)
            startDate = endDate - timedelta(30)

        if request.is_ajax():
            type = request.GET.get('type', None)
            if type == 'calling_minutes':
                data = TrafficHelper().get_calling_minutes(type, startDate, endDate)
            else:
                data = GGSNThroughputHelper().get_data_volume(type, startDate, endDate)  # 2G+3G, 4G or both have been segregated throgh type name

            data['startDate'] = startDate.strftime('%Y-%m-%d')
            data['endDate'] = endDate.strftime('%Y-%m-%d')
            return JsonResponse(data, safe=False)


        context = {
            'title': 'CNP Report: Data Volume & Calling Minutes',
            'startDate': startDate.strftime('%Y-%m-%d'),
            'endDate': endDate.strftime('%Y-%m-%d')
        }
        return render(request, 'report/volume_traffic.html', {'context': context})



#Overshoot calculations
class NodeOvershoot( View ):

    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):

        if (request.is_ajax()):
            data = NodeOvershootHelper().get_overshoots(request.GET.get('type', None))
            return JsonResponse(data, safe=False)

        context = {
            'title': 'CNP Report: Overshoots of Core Nodes'
        }
        return render(request, 'report/overshoot.html', {'context': context})


#throughput
class Throughput( View ):

    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        try:
            startDate = datetime.strptime(request.GET.get('from', ''), '%Y-%m-%d')
            endDate = datetime.strptime(request.GET.get('to', ''), '%Y-%m-%d')
        except:
            endDate = date.today() - timedelta(1)
            startDate = endDate - timedelta(30)

        if request.is_ajax():
            type = request.GET.get('type', None)
            if type == 'gi_throughput':
                data = GGSNThroughputHelper().get_daily_peak_throughput(startDate, endDate) #2G+3G+4G
            elif type == 'sgi_throughput':
                data = GGSNThroughputHelper().get_daiy_peak_sgi_throughput(startDate, endDate) #only 4G
            else:
                data = TrafficHelper().get_daily_traffic(startDate, endDate)

            data['startDate'] = startDate.strftime('%Y-%m-%d')
            data['endDate'] = endDate.strftime('%Y-%m-%d')
            return JsonResponse(data, safe=False)

        context = {
            'title': 'CNP Report: Daily Traffic and Throughput pattern',
            'startDate': startDate.strftime('%Y-%m-%d'),
            'endDate': endDate.strftime('%Y-%m-%d')
        }
        return render(request, 'report/throughput.html', {'context': context})
