from django.conf.urls import url
from .views import HrTraffic, SummaryTraffic, ImportExport, Traffic, ThroughputVolume, NodeOvershoot, Throughput

app_name = 'report'
urlpatterns = [
    url(r'^hourlytraffic$', HrTraffic.as_view(), name='hourlytraffic'),
    url(r'^$', Traffic.as_view(), name='traffic'),
    url(r'^summarytraffic$', SummaryTraffic.as_view(), name='summarytraffic'),
    url(r'^io$', ImportExport.as_view(), name='io'),
    url(r'^volumetraffic$', ThroughputVolume.as_view(), name='volumetraffic'),
    url(r'^throughput$', Throughput.as_view(), name='throughput'),
    url(r'^overshoot$', NodeOvershoot.as_view(), name='overshoot'),
]