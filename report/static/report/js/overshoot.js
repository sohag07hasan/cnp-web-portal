jQuery(document).ready(function ($) {

    /**
    * Add a URL parameter (or modify if already exists)
    * @param {url}   string  url
    * @param {param} string  the key to set
    * @param {value} string  value
    */
	var addParam = function(url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1"+str);
        if (q === a.search) {
          a.search += (a.search ? "&" : "") + str;
        } else {
          a.search = q;
        }
        return a.href;
	};

    function create_bar_chart(data){
        Highcharts.chart(data.container, {
            chart: {
                /* type: 'bar' */
            },
            title: {
                text: data.title
            },
            subtitle: {
                text: 'Date: ' + data.day
            },
            xAxis: {
                categories: data.category,
                title: {
                    text: data.x
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Overshoots',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                /* valueSuffix: ' times' */
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                x: -100,
                y: 30,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [
                {
                    type: 'bar',
                    name: 'Overshoots',
                    data: data.overshoots
                },

                {
                    type: 'line',
                    name: 'Overshoots Limit',
                    data: data.overshoot_limit
                }

            ]
        });
    }


    map_names = [

        {
            'name': "mss",
            'title': "MSS License Overshoots",
            'container': 'container_1',
            'x': 'MSS',
            'y': 'Overshoot Counts',
            'y_max': 30
        },

        {
            'name': "mgw",
            'title': "MGW License Overshoots",
            'container': 'container_2',
            'x': 'MGW',
            'y': 'Overshoot Counts',
            'y_max': 30
        }

    ];


    $.each(map_names, function (i, map_name) {
        var ajax_url = addParam(window.location.href, 'type', map_name.name.toLowerCase());

        $.getJSON(ajax_url, function (data) {
            map_name.category = data.nodes;
            map_name.overshoots = data.overshoots;
            map_name.day = data.day;
            map_name.overshoot_limit = data.overshoot_limit;
            create_bar_chart(map_name);
        });
    });



});