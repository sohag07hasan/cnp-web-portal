jQuery(document).ready(function ($) {

    /**
    * Add a URL parameter (or modify if already exists)
    * @param {url}   string  url
    * @param {param} string  the key to set
    * @param {value} string  value
    */
	var addParam = function(url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1"+str);
        if (q === a.search) {
          a.search += (a.search ? "&" : "") + str;
        } else {
          a.search = q;
        }
        return a.href;
	};


    var seriesOptions = [], categoriesOptions = [], mapTitle = '', mapSubtitle = '', x = '', y = '', y_max = '';
    var legend_y = 40, legend_x = 120;

    map_names = [

        {
            name: 'traffic_and_cap_trend',
            title: 'Traffic and CAP Trend',
            container: 'container_1',
            x: 'Day',
            y: 'kErl',
            y_max: 2050,
        },
        {
            name: 'subscriber_trend',
            title: 'Subscriber Trend',
            container: 'container_2',
            x: 'Day',
            y: 'Subscriber ( Mn )',
            y_max: 60,
        },
        {
            name: 'mhts',
            title: 'Mean Holding Time',
            container: 'container_3',
            x: 'Day',
            y: 'Time ( seconds )',
            y_max: 85,
        }

    ];

    dual_map_names = [
        {
            name: 'merl_and_bhca_per_sub',
            title: 'mErl/Sub & BHCA/Sub',
            container: 'container_4',
            x: 'Day',
            y1: {
                title: 'mErl/Sub',
                max: 55
            },
            y2: {
                title: 'BHCA/Sub',
                max: 10
            },
        }
    ];

    /**
     * create the chart when all data loaded
     * @returns {undefined}
     * */
    function createChart( id_name ) {
        Highcharts.chart(id_name, {
            chart: {
                type: 'spline'
            },
            title: {
                text: mapTitle
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: mapSubtitle
            },
            xAxis: {
                 title:{
                    text: x
                },
                categories: categoriesOptions
            },
            yAxis: {
                title: {
                    text: y
                },
                max: y_max
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                },

                series: {
                    cursor: 'pointer',
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'left',
                x: legend_x,
                verticalAlign: 'top',
                y: legend_y,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: seriesOptions

        });
    }

    /**
     * This function will create map with dual Y axis one oposite
     * */
    function createDualAxisChart( id_name ){

        Highcharts.chart( id_name, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'mErL/Sub and BHCA/Sub'
            },
             credits: {
                enabled: false
            },
            subtitle: {
                text: mapSubtitle
            },
            xAxis: [{
                categories: categoriesOptions,
                crosshair: true,
                title:{
                    text: 'day'
                },
            }],
            yAxis: [
                { // Primary yAxis
                    labels: {
                        //format: '{value}°C',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    title: {
                        text: 'mErl/Sub',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                }, { // Secondary yAxis
                    title: {
                        text: 'BHCA/Sub',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    labels: {
                        //format: '{value} mm',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true
                }
            ],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'horizontal',
                align: 'left',
                x: legend_x,
                verticalAlign: 'top',
                y: legend_y,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [
                {
                    name: 'mErl/Sub',
                    type: 'spline',

                    data: merl_sub,
                    tooltip: {
                        //valueSuffix: ' mm'
                    }
                }, {
                    name: 'BHCA/Sub',
                    type: 'spline',
                    yAxis: 1,
                    data: bhca_sub,
                    tooltip: {
                        //valueSuffix: '°C'
                    }
                }
            ]
        });

    }



    $.each(map_names, function (i, map_name) {
        var ajax_url = addParam(window.location.href, 'type', map_name.name.toLowerCase());
        $.getJSON(ajax_url, function (data) {
             var j = 0;
            $.each(data, function ( key, value ) {
                if ( key == 'day'){
                    categoriesOptions = value; // assinging day at x axis
                }
                else{
                    seriesOptions[j] = {
                        name: key,
                        data: value
                    };
                    j++;
                }
            });
            if(j > 0) {
                mapTitle = map_name.title;
                x = map_name.x;
                y = map_name.y;
                y_max = map_name.y_max;
                mapSubtitle = Report.range_title;
                createChart( map_name.container );
                seriesOptions = []; categoriesOptions = [];
            }
        });
    });

     $.each(dual_map_names, function (i, map_name) {
         var ajax_url = addParam(window.location.href, 'type', map_name.name.toLowerCase());
        $.getJSON(ajax_url, function (data) {
            merl_sub = data.merl_sub;
            bhca_sub = data.bhca_sub;
            categoriesOptions = data.day;
            mapSubtitle = Report.range_title;
            createDualAxisChart( map_name.container );
        });
     });
     
     
     /**
      * Filter date range
      * */
     $('#generate_report').click(function (evt) {
         d1 = $('input[name="startDate"]').val().trim();
         d2 = $('input[name="endDate"]').val().trim();
         day1 = new Date(d1);
         day2 = new Date(d2);
         if(day2 > day1){
            var redirect_url = Report.summarytraffic + '?from=' + d1 + '&to=' + d2;
            window.location.href = redirect_url;
         }
         else{
             alert('End date might be greater than Start Day');
         }
     });



});


/**
 * Adding date  picker at summary traffic
 * */
jQuery(document).ready(function ($) {
    var date_start = $('input[name="startDate"]');
    var date_end = $('input[name="endDate"]');
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '+0d',
        startDate: '2017-01-01'
    };
    date_start.datepicker(options);
    date_end.datepicker(options);
});