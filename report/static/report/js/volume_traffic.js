/**
 * Created by mahibul.hasan on 7/18/2017.
 */
jQuery(document).ready(function($) {

    /**
     * Add a URL parameter (or modify if already exists)
     * @param {url}   string  url
     * @param {param} string  the key to set
     * @param {value} string  value
     */
    var addParam = function (url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1" + str);
        if (q === a.search) {
            a.search += (a.search ? "&" : "") + str;
        } else {
            a.search = q;
        }
        return a.href;
    };


    /* Function to ploat a chart
     * */
    function draw_three_series_chart(data) {

        Highcharts.chart(data.container, {
            chart: {
                zoomType: 'xy'
            },
            credits: {
                enabled: false
            },
            title: {
                text: data.title
            },
            subtitle: {
                text: data.subtitle
            },
            xAxis: [{
                categories: data.x_axis.categories,
                crosshair: true
            }],
            yAxis: [
                { // Primary yAxis
                    labels: {
                        format: data.y_axis.p_label,
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    title: {
                        text: data.y_axis.p_title,
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                },
                { // Secondary yAxis
                    title: {
                        text: data.y_axis.s_title,
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        },
                    },
                    labels: {
                        format: data.y_axis.s_label,
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true
                }
            ],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'horizontal',
                align: 'left',
                x: data.legends.x,
                verticalAlign: 'top',
                y: data.legends.y,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [
                {
                    name: data.series.series_1.name,
                    type: 'column',
                    color: Highcharts.getOptions().colors[0],
                    yAxis: 0,
                    data: data.series.series_1.data,
                    tooltip: {
                        valueSuffix: data.series.series_1.tooltip
                    }
                },
                {
                    name: data.series.series_2.name,
                    type: 'column',
                    color: Highcharts.getOptions().colors[2],
                    yAxis: 0,
                    data: data.series.series_2.data,
                    tooltip: {
                        valueSuffix: data.series.series_2.tooltip
                    }
                },
                {
                    name: data.series.series_3.name,
                    type: 'spline',
                    color: Highcharts.getOptions().colors[1],
                    yAxis: 1,
                    data: data.series.series_3.data,
                    tooltip: {
                        valueSuffix: data.series.series_3.tooltip
                    }
                }
            ]
        });
    }

    /** Maps definations */
    maps = [
        {
            type: 'gi_traffic',
            container: 'container_1',
            title: 'Data Volume ( Network )',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} TB',
                p_title: 'Volume ( TB )',
                s_label: '{value} %',
                s_title: 'BH Contribution'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Total Volume',
                    data: [],
                    tooltip: ' TB'
                },
                series_2: {
                    name: 'BH Volume',
                    data: [],
                    tooltip: ' TB'
                },
                series_3: {
                    name: 'BH Contribution',
                    data: [],
                    tooltip: ' %s'
                }
            }
        },

        {
            type: 'gi_traffic_gi',
            container: 'container_2',
            title: 'Data Volume ( 2G+3G )',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} TB',
                p_title: 'Volume ( TB )',
                s_label: '{value} %',
                s_title: 'BH Contribution'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Total Volume',
                    data: [],
                    tooltip: ' TB'
                },
                series_2: {
                    name: 'BH Volume',
                    data: [],
                    tooltip: ' TB'
                },
                series_3: {
                    name: 'BH Contribution',
                    data: [],
                    tooltip: ' %s'
                }
            }
        },

        {
            type: 'gi_traffic_sgi',
            container: 'container_3',
            title: 'Data Volume ( 4G )',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} TB',
                p_title: 'Volume ( TB )',
                s_label: '{value} %',
                s_title: 'BH Contribution'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Total Volume',
                    data: [],
                    tooltip: ' TB'
                },
                series_2: {
                    name: 'BH Volume',
                    data: [],
                    tooltip: ' TB'
                },
                series_3: {
                    name: 'BH Contribution',
                    data: [],
                    tooltip: ' %s'
                }
            }
        },

        {
            type: 'calling_minutes',
            title: 'Calling Minutes',
            container: 'container_4',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} Mn',
                p_title: 'Minutes ( Mn )',
                s_label: '{value} %',
                s_title: 'BH Contribution'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Total Minutes',
                    data: [],
                    tooltip: ' Mn'
                },
                series_2: {
                    name: 'BH Minutes',
                    data: [],
                    tooltip: ' Mn'
                },
                series_3: {
                    name: 'BH Contribution',
                    data: [],
                    tooltip: ' %s'
                }
            }
        },

    ]

    $.each(maps, function (i, map) {
        var ajax_url = addParam(window.location.href, 'type', map.type);
        $.getJSON(ajax_url, function (data) {
            map.x_axis.categories = data.day;
            map.subtitle = 'Date: ' + data.startDate + ' to ' + data.endDate;
            if (map.type == 'calling_minutes') { //calling_minutes
                map.series.series_1.data = data.sum_min;
                map.series.series_2.data = data.max_min;
                map.series.series_3.data = data.bh_contribution;
            }
            else {
                map.series.series_1.data = data.sum_gi;
                map.series.series_2.data = data.max_gi;
                map.series.series_3.data = data.bh_contribution;
            }
            draw_three_series_chart(map);
        });
    });

});

jQuery(document).ready(function ($) {
    /**
    * Filter date range
    * */
    $('#generate_report').click(function (evt) {
        d1 = $('input[name="startDate"]').val().trim();
        d2 = $('input[name="endDate"]').val().trim();
        day1 = new Date(d1);
        day2 = new Date(d2);
        if(day2 > day1){
            var redirect_url = Report.volumetraffic + '?from=' + d1 + '&to=' + d2;
            window.location.href = redirect_url;
        }
        else{
            alert('End date might be greater than Start Day');
        }
    });


    /*** Attaching date picker ***/
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '-1d',
        startDate: '2017-01-01'
    };

    $('#start_date').datepicker(options);
    $('#end_date').datepicker(options);

    /*** End of Date Picker ***/

});