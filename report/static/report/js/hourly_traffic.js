jQuery(document).ready(function ($) {


    $('#hourly_traffic_date').change(function (evt) {
        var day = $(this).val();
        var newUrl = Report.hourlytraffic + '?day='+day;
        window.location.href = newUrl;
    });


    //date picker function

    var date_start = $('#start_date');
     var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '+0d',
        startDate: '2017-01-01'
    };
    date_start.datepicker(options);


    //binding click function
    $('#generate_report').click(function () {
        var day = $('#start_date').val();
        var newUrl = Report.hourlytraffic + '?day='+day;
        window.location.href = newUrl;
    });

});