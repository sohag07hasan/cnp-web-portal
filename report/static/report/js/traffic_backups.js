jQuery(document).ready(function ($) {

    /*** Redirect url ***/
    $('#hourly_traffic_date').change(function (evt) {
        var day = $(this).val();
        var newUrl = Report.traffic + '?day='+day;
        window.location.href = newUrl;
    });

    /*** Attaching date picker ***/
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '+0d',
        startDate: '2017-01-01'
    };

    var traffic_date = $('input[name="TrafficDate"]');
    traffic_date.datepicker(options);
    /*** End of Date Picker ***/


    /**
     * adding paramters in urls
     * **/
      /**
    * Add a URL parameter (or modify if already exists)
    * @param {url}   string  url
    * @param {param} string  the key to set
    * @param {value} string  value
    */
	var addParam = function(url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1"+str);
        if (q === a.search) {
          a.search += (a.search ? "&" : "") + str;
        } else {
          a.search = q;
        }
        return a.href;
	};


    /**
     * generate line chart
     * */
    function generate_line_chart(container, data){

        Highcharts.chart(container, {
            chart: {
                type: 'line'
            },
            credits: {
                enabled: false,
            },
            title: {
                text: data.title.title
            },
            subtitle: {
                text: data.title.sub_title
            },
            xAxis: {
                title:{
                    text: data.xAxis.title
                },
                categories: data.xAxis.categories
            },
            yAxis: {
                title: {
                    text: data.yAxis.title
                },
                max: 2500
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        rotation: data.PlotOptions.rotation,
                        format: data.PlotOptions.format,
                        y: data.PlotOptions.y
                    },
                    enableMouseTracking: true,

                }
            },
            series: data.series,
             exporting: {
                enabled: true
            }
        });
    }









            var ajax_url = Report.traffic;
            ajax_url = addParam(ajax_url, 'day', '2017-07-16');
            ajax_url = addParam(ajax_url, 'type', 'hourly_traffic');
            $.getJSON(ajax_url, function (data) {
                var map_data = {
                    title: {
                        title: 'Hourly Network Traffic',
                        subtitle: '2017-06-12'
                    },
                    xAxis: {
                        title: 'Hour',
                        categories: data.hour
                    },
                    yAxis: {
                        title: 'kErl',
                        max: 2500
                    },
                    PlotOptions: {
                        rotation: -90,
                        format: '{point.y:.1f}',
                        y: -30
                    },
                    series:[
                        {
                            showInLegend: false,
                            name: data.day,
                            data: data.traffic
                        }
                    ]
                };
                generate_line_chart('container_1', map_data);
            });





















    //click event to redraw maps
    $('.generate_report').click(function () {
        var type = $(this).attr('type');
        var ajax_url = addParam(Report.traffic, 'type', type.toLowerCase());

        if (type == 'hourly_traffic'){
            ajax_url = addParam(ajax_url, 'day', $('#traffic_date').val());
            $.getJSON(ajax_url, function (data) {
                var map_data = {
                    title: {
                        title: 'Hourly Network Traffic',
                        subtitle: '2017-06-12'
                    },
                    xAxis: {
                        title: 'Hour',
                        categories: data.hour
                    },
                    yAxis: {
                        title: 'kErl',
                        max: 2500
                    },
                    plotOptions: {
                        rotation: 90,
                        format: '{point.y:.1f}',
                        y: -30
                    },
                    series:[
                        {
                            showInLegend: false,
                            name: data.day,
                            data: data.traffic
                        }
                    ]
                };

                generate_line_chart('container_2', map_data);
            });


        }
    });



});