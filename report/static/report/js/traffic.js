jQuery(document).ready(function ($) {

    /*** Redirect url ***/
    $('#hourly_traffic_date').change(function (evt) {
        var day = $(this).val();
        var newUrl = Report.traffic + '?day='+day;
        window.location.href = newUrl;
    });

    /*** Attaching date picker ***/
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '+0d',
        startDate: '2017-01-01'
    };

    var traffic_date = $('#traffic_date');
    traffic_date.datepicker(options);
    /*** End of Date Picker ***/

    /*
    * Add a URL parameter (or modify if already exists)
    * @param {url}   string  url
    * @param {param} string  the key to set
    * @param {value} string  value
    */
	var addParam = function(url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1"+str);
        if (q === a.search) {
          a.search += (a.search ? "&" : "") + str;
        } else {
          a.search = q;
        }
        return a.href;
	};


	/*
     * Houlry Traffic in linek chart
     */
    function draw_hourly_line_chart() {
        var ajax_url = Report.traffic;
        ajax_url = addParam(ajax_url, 'type', 'hourly_traffic');
        $.getJSON(ajax_url, function (data) {

            var chart = Highcharts.chart('container_1', {
                chart: {
                    type: 'line'
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "Hourly Network Traffic"
                },
                subtitle: {
                    text: 'Date: ' + data.day
                },
                xAxis: {
                    title: {
                        text: 'Hour'
                    },
                    categories: data.hour
                },
                yAxis: {
                    title: {
                        text: "Traffic ( kErl )"
                    },
                    max: 2500
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true,
                            allowOverlap: true,
                            rotation: -90,
                            format: '{point.y:.1f}',
                            y: -30
                        },
                        enableMouseTracking: true,

                    }
                },
                series: [
                    {
                        showInLegend: false,
                        name: data.day,
                        data: data.traffic,
                    }
                ],
                exporting: {
                    enabled: true
                }
            });

            $('.generate_report').click(function () {
                day = $('#traffic_date').val();
                ajax_url = addParam(ajax_url, 'day', day);
                 $.getJSON(ajax_url, function (data) {
                     chart.update({
                         subtitle: {
                            text: 'Date:' + data.day
                         },
                         xAxis:{
                            categories: data.hour
                         },
                         series: [
                            {
                                showInLegend: false,
                                name: data.day,
                                data: data.traffic,
                            }
                        ]
                     });
                 });

            });
        });

    }



    /*
    * Adding comparison of multiple days
    * */
    function comparision_map(){

    }




    /** initialize hourly traffic */
    draw_hourly_line_chart();

});