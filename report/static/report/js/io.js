/**
 * Created by mahibul.hasan on 19/04/2017.
 */

jQuery(document).ready(function ($) {
    var files;

    $('input[type=file]').on('change', prepareUpload);
    function prepareUpload(event){
      files = event.target.files;
    }

    $('form#ioform').on( 'submit', upLoadFiles );
    function upLoadFiles(event) {
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening

        var type = $('#id_type').val();
        if (type == 'summarytraffic') {
            var message = 'Summary Traffic';
        }
        else if(type == '15_min_traffic'){
            var message = '15 Minutes Hourly Network Traffic';
        }
        else{
            var message = 'GGSN Volume & Throughput';
        }


        if(confirm( "You are going to import " + message )) {
            var data = new FormData($('form#ioform')[0]);
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            data.append('type', type); // including excel type
            process_ajax(data, submitForm); // file uploading starting
        }
        else{
            return false;
        }
    }

   //function to handle ajax
     function process_ajax(input_data, success_function){
        var csrftoken = Cookies.get('csrftoken');
        $.ajax({
            beforeSend: function () {
                $('#submit_button').val('Uploading..');
                $('#submit_button').addClass('processing');
                $('#upload_progress').removeClass('display_none');
                $('#ajax_processing_message').removeClass('display_none');
            },
            url: ReportIO.ajax_url,
            method: 'POST',
            dataType: 'json',
            cache: false,
            async: true,
            processData: false,
            contentType: false,
            data: input_data,
            headers:{'X-CSRFToken': csrftoken},
            success: function (ajax_data) {
                success_function(ajax_data, input_data);
            },
            error: function () {
                alert('Error: Please check excel file again and try!');
            },
            complete: function () {
                $('#submit_button').val('Start Uploading');
                $('#submit_button').removeClass('processing');
                $('#upload_progress').addClass('display_none');
                $('#ajax_processing_message').addClass('display_none');
                $('#ioform').trigger("reset");

            }
        });
     }

     //runs it when file upload done
     function submitForm( ajax_data, input_data ){
         $('#submit_button').val('Start Uploading');
         var message = ' Updated: ' + ajax_data.updated +', New Imported: ' + ajax_data.created + ' & Failed: ' + ajax_data.failure;
         if ( ajax_data.failed_rows.length > 0 ){
             message += " Failed Rows: " + ajax_data.failed_rows.join();
         }

         $('#success_message').html(message);

         if(input_data.get('type') == 'summarytraffic'){
             var title = 'Summary Traffic';
         }


         $('.alert-success').find('strong').html(title);
         $('.alert-success').removeClass('display_none');
     }
})