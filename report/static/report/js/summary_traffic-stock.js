jQuery(document).ready(function ($) {

    var seriesOptions = [],
    seriesCounter = 0,
    names = ['HW_Capacity', 'SW_Capacity', 'BH_Traffic', 'Forecast'];
    summary_traffic_url = Report.summarytraffic;

    /**
     * create the chart when all data loaded
     * @returns {undefined}
     * */
    function createChart(){
        Highcharts.stockChart('container', {

            rangeSelector: {
                selected: 4
            },

            yAxis: {
                labels: {
                    formatter: function () {
                        return (this.value > 0 ? ' + ' : '') + this.value + '%';
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },

            plotOptions: {
                series: {
                    compare: '',
                    showInNavigator: true
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2,
                split: true
            },

            series: seriesOptions
        });

    }

    $.each(names, function (i, name) {
        var ajax_url = summary_traffic_url + '?type=' + name.toLowerCase();
        $.getJSON(ajax_url, function (data) {

            //alert(data);

            seriesOptions[i] = {
                name: name,
                data: data
            };

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
            }
        });
    })

});