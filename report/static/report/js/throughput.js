/**
 * Created by mahibul.hasan on 7/18/2017.
 */
jQuery(document).ready(function($) {

    /**
     * Add a URL parameter (or modify if already exists)
     * @param {url}   string  url
     * @param {param} string  the key to set
     * @param {value} string  value
     */
    var addParam = function (url, param, value) {
        param = encodeURIComponent(param);
        var r = "([&?]|&amp;)" + param + "\\b(?:=(?:[^&#]*))*";
        var a = document.createElement('a');
        var regex = new RegExp(r);
        var str = param + (value ? "=" + encodeURIComponent(value) : "");
        a.href = url;
        var q = a.search.replace(regex, "$1" + str);
        if (q === a.search) {
            a.search += (a.search ? "&" : "") + str;
        } else {
            a.search = q;
        }
        return a.href;
    };


    /* Function to ploat a chart
     * */
    function draw_three_series_chart(data) {

        Highcharts.chart(data.container, {
            chart: {
                zoomType: 'xy'
            },
            credits: {
                enabled: false
            },
            title: {
                text: data.title
            },
            subtitle: {
                text: data.subtitle
            },
            xAxis: [{
                categories: data.x_axis.categories,
                crosshair: true
            }],
            yAxis: [
                { // Primary yAxis
                    labels: {
                        format: data.y_axis.p_label,
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    title: {
                        text: data.y_axis.p_title,
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                },
                { // Secondary yAxis
                    title: {
                        text: data.y_axis.s_title,
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        },
                    },
                    labels: {
                        format: data.y_axis.s_label,
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true,
                    max: 24
                }
            ],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'horizontal',
                align: 'left',
                x: data.legends.x,
                verticalAlign: 'top',
                y: data.legends.y,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [
                {
                    name: data.series.series_1.name,
                    type: 'column',
                    color: Highcharts.getOptions().colors[0],
                    yAxis: 0,
                    data: data.series.series_1.data,
                    tooltip: {
                        valueSuffix: data.series.series_1.tooltip
                    }
                },
                {
                    name: data.series.series_2.name,
                    type: 'spline',
                    color: Highcharts.getOptions().colors[1],
                    yAxis: 1,
                    data: data.series.series_2.data,
                    tooltip: {
                        valueSuffix: data.series.series_2.tooltip
                    }
                }
            ]
        });
    }

    /** Maps definations */
    maps = [
        {
            type: 'gi_throughput',
            container: 'container_1',
            title: 'Daily Data Peak Throughput',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} Gbps',
                p_title: 'Throughput ( Gbps )',
                s_label: '{value}:00',
                s_title: 'Hour'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Throughput',
                    data: [],
                    tooltip: ' Gbps'
                },
                series_2: {
                    name: 'Hour',
                    data: [],
                    tooltip: ':00'
                },

            }
        },

        {
            type: 'sgi_throughput',
            container: 'container_2',
            title: 'Daily LTE Peak Throughput',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} Gbps',
                p_title: 'Throughput ( Gbps )',
                s_label: '{value}:00',
                s_title: 'Hour'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Throughput',
                    data: [],
                    tooltip: ' Gbps'
                },
                series_2: {
                    name: 'Hour',
                    data: [],
                    tooltip: ':00'
                },

            }
        },

        {
            type: 'daily_traffic',
            title: 'Daily Voice Peak Traffic',
            container: 'container_3',
            subtitle: '',
            x_axis: {
                categories: []
            },
            y_axis: {
                p_label: '{value} kErl',
                p_title: 'Traffic ( kErl )',
                s_label: '{value}:00',
                s_title: 'Hour'
            },
            legends: {
                x: 120,
                y: 50
            },
            series: {
                series_1: {
                    name: 'Traffic',
                    data: [],
                    tooltip: ' kErl'
                },
                series_2: {
                    name: 'Hour',
                    data: [],
                    tooltip: ':00'
                }
            }
        }

    ]

    $.each(maps, function (i, map) {
        var ajax_url = addParam(window.location.href, 'type', map.type);
        $.getJSON(ajax_url, function (data) {
            map.x_axis.categories = data.day;
            map.subtitle = 'Date: ' + data.startDate + ' to ' + data.endDate;
            map.series.series_1.data = data.throughput;
            map.series.series_2.data = data.hour;
            draw_three_series_chart(map);
        });
    });

});

jQuery(document).ready(function ($) {
    /**
    * Filter date range
    * */
    $('#generate_report').click(function (evt) {
        d1 = $('input[name="startDate"]').val().trim();
        d2 = $('input[name="endDate"]').val().trim();
        day1 = new Date(d1);
        day2 = new Date(d2);
        if(day2 > day1){
            var redirect_url = Report.throughput + '?from=' + d1 + '&to=' + d2;
            window.location.href = redirect_url;
        }
        else{
            alert('End date might be greater than Start Day');
        }
    });


    /*** Attaching date picker ***/
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '-1d',
        startDate: '2017-01-01'
    };

    $('#start_date').datepicker(options);
    $('#end_date').datepicker(options);

    /*** End of Date Picker ***/

});