from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from .helper import DashbordHelper, IIGDashboardHelper, IoHelper, IPHelper
from datetime import date, timedelta, datetime
import django_excel as excel
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

class Dashboard(View):
    def get(self, request):
        context = {'title': 'Internet Dashboard'}
        return render(request, 'igw/internet-dashboard.html', {'context': context})


#handles all ajax
class Ajax(View):
    def get(self, request):
        today = date.today()
        toDay = today - timedelta(1)
        fromDay = toDay - timedelta(50)

        type = request.GET.get('type', None) #get the type ( i.g iig, cdn .. )
        page = request.GET.get('page', None) # get the page ( i.g dashboard, atl .. )
        vendor = request.GET.get('name', None) # get the page ( i.g dashboard, atl .. )
        unit = request.GET.get('unit', None) # get unit gbps/mbps

        if page == 'dashboard' and type == 'iig':
            data = DashbordHelper().get_iig_data(fromDay, toDay)
        elif page == 'dashboard' and type == 'cdn':
            data = DashbordHelper().get_cdn_data(fromDay, toDay)
        elif page == 'dashboard' and type == 'iigsubscription':
            data = DashbordHelper().get_iig_subscription(fromDay, toDay)
        elif page == 'iig-dashboard' and type == 'iig':
            data = IIGDashboardHelper().get_specific_iig_data(fromDay, toDay, vendor, unit)

        return JsonResponse(data, safe=False)

#Handling IIG
class IIG(View):
    def get(self, request, name=None):
        context = {'title': 'iig | atl | summit | fiber@home | btcl'}
        return render(request, 'igw/iig-dashboard.html', {'context': context})


#Handling Nix
class IX(View):
    def get(self, request, name=None):
        context = {'title': 'iig | novonix | bdix'}
        return render(request, 'igw/ix-dashboard.html', {'context': context})



#Handling CDN
class CDN(View):
    def get(self, request, name=None):
        context = {'title': 'cdn | fna | ggc | akamai'}
        return render(request, 'igw/cdn-dashboard.html', {'context': context})


#Handling Import & Export
class IO(LoginRequiredMixin, View):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request, name=None):
        context = {'title': 'io | import | export'}
        return render(request, 'igw/io-dashboard.html', {'context': context})

    def post(self, request):
        startDay = request.POST.get('startDate')
        endDay = request.POST.get('endDate')
        data = IoHelper().export_by_range(startDay, endDay)
        attachment_name = "IIG_Data_" + "{:%Y-%m-%d}".format(datetime.now())
        return excel.make_response_from_array(data, 'xls', file_name=attachment_name)


#This class will show Public public_ips
class IP(LoginRequiredMixin, View):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        ips = IPHelper().get_ips()
        context = {'ip': ips, 'title': 'public ip address'}
        return render(request, 'igw/ip-dashboard.html', {'context': context})
