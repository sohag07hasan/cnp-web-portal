jQuery(document).ready(function ($) {

    var series_option = [];

    function draw_iig_chart(chart_data){
        Highcharts.chart(chart_data.line_container, {
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: chart_data.title
            },
            subtitle: {
                text: chart_data.subtitle
            },
            xAxis: {
                categories: chart_data.categories,
                title:{
                    text: chart_data.x.title
                }
            },
            yAxis: [{ //primary axis
                title: {
                    text: chart_data.y.title,
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },

                labels: {
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

                //max: chart_data.y.max
            },{ //secondary axis
                 title: {
                    text: chart_data.y.title2,
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },

                labels: {
                    format: '{value} %',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                opposite: true
            }],

            legend: {
                layout: 'horizontal',
                align: 'left',
                verticalAlign: 'top',
                x: 100,
                y: 40,
                floating: true
            },
             plotOptions: {
                column: {
                    stacking: 'normal',

                    dataLabels: {
                        //enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true,
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>'
            },
            series: [
                {
                    type: 'column',
                    name: 'Savar',
                    data: chart_data.sav_in,
                    color: Highcharts.getOptions().colors[2],
                    yAxis: 0,
                }, {
                    type: 'column',
                    name: 'Gazipur',
                    data: chart_data.gp_in,
                    color: '#81913d'
                    //color: Highcharts.getOptions().colors[3]
                }, {
                    type: 'column',
                    name: 'Jashore',
                    data: chart_data.js_in,
                    color: Highcharts.getOptions().colors[3],
                    //yAxis: 0,
                },


                /*{
                    type: 'spline',
                    name: 'Total',
                    data: chart_data.total_in,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                }, */ {
                    type: 'spline',
                    name: 'Subscription',
                    data: chart_data.sub,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[4],
                        fillColor: 'white'
                    },
                    yAxis: 0,
                },{
                    type: 'spline',
                    name: 'Utilization',
                    data: chart_data.util,
                    yAxis: 1,
                    tooltip: {
                        valueSuffix: ' %'
                    },
                    color: Highcharts.getOptions().colors[1]
                },
            ]
        });

    }


    iig_charts = [
        {
            'name': 'atl',
            'type': 'iig',
            'page': 'iig-dashboard',
            'line_container': 'iig_container_1',
            'series': [],
            'x': {'title': 'Day'},
            'y': {'title': 'Gbps', 'max': 45, 'title2': 'Utilization'},
            'title': 'ATL',
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in':[],
            'sub': [],
            'subtitle': 'Source: ATL Cacti'
        },
        {
            'name': 'levelthree',
            'type': 'iig',
            'page': 'iig-dashboard',
            'line_container': 'iig_container_2',
            'series': [],
            'x': {'title': 'Day'},
            'y': {'title': 'Gbps', 'max': 45, 'title2': 'Utilization'},
            'title': 'Level Three',
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in':[],
            'sub': [],
            'subtitle': 'Source: Level three Cacti'
        },
        {
            'name': 'fiberathome',
            'type': 'iig',
            'page': 'iig-dashboard',
            'line_container': 'iig_container_3',
            'series': [],
            'x': {'title': 'Day'},
            'y': {'title': 'Gbps', 'max': 45, 'title2': 'Utilization'},
            'title': 'Fiber@Home',
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in':[],
            'sub': [],
            'subtitle': 'Source: Fiber@Home Cacti'
        },
        {
            'name': 'summit',
            'type': 'iig',
            'page': 'iig-dashboard',
            'line_container': 'iig_container_4',
            'series': [],
            'x': {'title': 'Day'},
            'y': {'title': 'Gbps', 'max': 45, 'title2': 'Utilization'},
            'title': 'Summit Communications',
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in':[],
            'sub': [],
            'subtitle': 'Source: Summit Cacti'
        },
        {
            'name': 'btcl',
            'type': 'iig',
            'page': 'iig-dashboard',
            'line_container': 'iig_container_5',
            'series': [],
            'x': {'title': 'Day'},
            'y': {'title': 'Gbps', 'max': 45, 'title2': 'Utilization'},
            'title': 'BTCL',
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in':[],
            'sub': [],
            'subtitle': 'Source: BTCL Cacti',
            'util': [],
        },
        {
            'name': 'mangotel',
            'type': 'iig',
            'page': 'iig-dashboard',
            'line_container': 'iig_container_6',
            'series': [],
            'x': {'title': 'Day'},
            'y': {'title': 'Gbps', 'max': 45, 'title2': 'Utilization'},
            'title': 'Mango Tel',
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in':[],
            'sub': [],
            'subtitle': 'Source: MangoTel Cacti',
            'util': [],
        }

    ];

    $.each(iig_charts, function(i, chart){

        var ajax_url = IGW.ajax_url + '?name=' + chart.name + '&page=' + chart.page + '&type=' + chart.type;

        $.getJSON(ajax_url, function(data){

            //alert(chart.name);

            chart.categories = data.days;
            chart.sav_in = data.sav_in;
            chart.gp_in = data.gp_in;
            chart.js_in = data.js_in;
            chart.total_in = data.total_in;
            chart.sub = data.subscriptions;
            chart.util = data.utilizations;

            draw_iig_chart(chart);
            //draw_line_chart();

        });


    });

    //draw_pie_chart(false);
    //draw_line_chart(false);

});