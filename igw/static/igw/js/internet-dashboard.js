jQuery(document).ready(function ($) {

    function draw_line_chart( chart ){

        Highcharts.chart(chart.container, {
            chart: {
                type: 'spline'
            },
            credits: {
                enabled: false
            },
            title: {
                text: chart.title
            },
            subtitle: {
                text: 'Source: ' + chart.subtitle
            },
            xAxis: {
                categories: chart.categories,
                title:{
                    text: chart.x.title
                }
            },
            yAxis: {
                title: {
                    text: chart.y.title
                },
                labels: {
                    formatter: function () {
                        return this.value + '';
                    }
                },
                //max: chart.y.max

            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            legend: {
                layout: 'horizontal',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 40,
                floating: true,
                borderWidth: 1,
                shadow: true,
                floating: true,
                //backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: chart.series
        });
    }

    charts = [
        {
            'name': 'iig',
            'sub_names': ['atl', 'levelthree', 'fiberathome', 'summit', 'btcl'],
            'page': 'dashboard',
            'container': 'iig-bandwidth-container',
            'x': {'title': 'Day'},
            'y': {'title': 'gbps', 'max': 55},
            'title': 'IIG Throughput',
            'subtitle': 'IIG portals',
            'series': [],
            'categories': []
        },
         {
            'name': 'iigsubscription',
            'sub_names': ['atl', 'levelthree', 'fiberathome', 'summit', 'btcl'],
            'page': 'dashboard',
            'container': 'iig-bandwidth-capacity',
            'x': {'title': 'Day'},
            'y': {'title': 'gbps', 'max': 55},
            'title': 'IIG Subscription',
            'subtitle': 'Planning Database',
            'series': [],
            'categories': []
        },
        {
            'name': 'cdn',
            'sub_names': ['fna', 'ggc', 'akamai'],
            'page': 'dashboard',
            'container': 'cdn-bandwidth-container',
            'x': {'title': 'Day'},
            'y': {'title': 'gbps', 'max': 55},
            'title': 'CDN Throughput',
            'subtitle': 'CDN portals',
            'series': [],
            'categories': []
        }

    ];

    function sleep(delay) {
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay);
    }

    // Set the global configs to synchronous
    $.ajaxSetup({
        async: false
    });

    $.each(charts, function(i, chart){

        var ajax_url = IGW.ajax_url + '?name=' + chart.name + '&page=' + chart.page + '&type=' + chart.name;
        var j = 0
        $.getJSON(ajax_url, function(data){
             $.each(data, function ( key, value ) {
                if ( key == 'days'){
                     chart.categories = data.days;
                }
                else{
                    chart.series[j] = {
                        name: key,
                        data: value
                    };
                    j++;
                }
            });
        });

        if (j > 0) { //ensuring proper data load
            draw_line_chart(chart);
        }
    });

});