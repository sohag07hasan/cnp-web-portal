jQuery(document).ready(function ($) {

    /** starting datepicker     * */

    var date_start = $('input[name="startDate"]');
    var date_end = $('input[name="endDate"]');
    var options = {
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        endDate: '+0d',
        startDate: '2018-01-01'
    };
    date_start.datepicker(options);
    date_end.datepicker(options);

    /** end of date picker */


    /* Staring Export functionality **/
    $('#submit_button').click(function () {
         d1 = $('input[name="startDate"]').val().trim();
         d2 = $('input[name="endDate"]').val().trim();
         day1 = new Date(d1);
         day2 = new Date(d2);
         if(day2 > day1){
            $('form#iig-export-form').submit();
         }
         else{
             alert('End date might be greater than Start Day');
             return false;
         }
    });

});