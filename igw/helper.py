from .models import ATL, FiberAtHome, BtCl, BdIx, LevelThree, Summit, Capacity, DailyPeakThroughput, PublicIP, IIGInformation
from django.db.models import Max, Sum, Count, Avg, F, Value
import sys

#dashboard helper
class DashbordHelper:
    #landing function
    iig_vendors = ['atl', 'levelthree', 'fiberathome', 'summit', 'btcl']
    cdn_vendors = ['fna', 'ggc', 'akamai']

    #returns iig data
    #@fromDay is start day & @toDay is for end day
    def get_iig_data(self, fromDay, toDay):
        final_data = {'days': [], 'atl': [], 'levelthree': [], 'fiberathome': [], 'summit': [], 'btcl': [] }
        try:
            results = DailyPeakThroughput.objects.values('day').filter(day__range=(fromDay, toDay)).distinct('day').order_by('day')
            for r in results:
                day = r.get('day').strftime("%Y-%m-%d")
                final_data['days'].append(day)
                for vendor in self.iig_vendors:
                    try:
                        data = DailyPeakThroughput.objects.values('total_in').filter(day=day, vendor=vendor)
                        traffic = data[0].get('total_in')
                    except:
                        traffic = 0
                    final_data[vendor].append( round( float(traffic)/1000, 2 ) ) #converting into gbps

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    #subscription data for iig
    def get_iig_subscription(self, fromDay, toDay):
        final_data = {'days': [], 'atl': [], 'levelthree': [], 'fiberathome': [], 'summit': [], 'btcl': []}
        try:
            results = Capacity.objects.values('day', 'atl', 'levelthree', 'fiberathome', 'summit','btcl').filter(day__range=(fromDay, toDay)).distinct('day').order_by('day')
            for r in results:
                day = r.get('day').strftime("%Y-%m-%d")
                final_data['days'].append(day)
                final_data['atl'].append( float(r.get('atl')))
                final_data['summit'].append( float(r.get('summit')) )
                final_data['levelthree'].append( float(r.get('levelthree')) )
                final_data['fiberathome'].append( float(r.get('fiberathome')) )
                final_data['btcl'].append( float(r.get('btcl')) )
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    #get cdn data
    def get_cdn_data(self, fromDay, toDay):
        final_data = {'days': [], 'fna': [], 'ggc': [], 'akamai': []}
        return final_data


## handles IIG data in details
class IIGDashboardHelper:
    def get_specific_iig_data(self, fromDay, toDay, vendor = None, unit= None):
        final_data = {'days': [], 'subscriptions': [], 'sav_in': [], 'gp_in': [], 'js_in':[], 'total_in': [], 'utilizations': []}
        try:
            results = DailyPeakThroughput.objects.values('day', 'gp_in', 'sav_in', 'js_in', 'total_in').filter(day__range=(fromDay, toDay), vendor=vendor).distinct('day').order_by('day')
            for r in results:
                day = r.get('day').strftime("%Y-%m-%d")
                sub = CapacityHelper().get_capacity(vendor, day)

                final_data['days'].append(day)
                if unit == 'mbps':
                    final_data['gp_in'].append(round(r.get('gp_in'), 2))
                    final_data['sav_in'].append(round(r.get('sav_in'), 2))
                    final_data['js_in'].append(round(r.get('js_in'), 2))
                    final_data['total_in'].append(round(r.get('total_in'), 2))
                    final_data['subscriptions'].append(sub*1000)
                    final_data['utilizations'].append(round(r.get('total_in') / (sub * 10), 2))
                else:
                    final_data['gp_in'].append( round(r.get('gp_in')/1000, 2) )
                    final_data['sav_in'].append( round(r.get('sav_in')/1000, 2) )
                    final_data['js_in'].append( round(r.get('js_in')/1000, 2) )
                    final_data['total_in'].append( round(r.get('total_in')/1000, 2) )
                    final_data['subscriptions'].append( sub )
                    final_data['utilizations'].append( round(r.get('total_in')/(sub*10), 2) )

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data


class DailyPeakThroughputHelper:
    def get_pick_traffic(self, vendor, fromDay, toDay):
        iigs = [ 'atl', 'summit', 'btcl', 'fiberathome', 'levelthree' ]
        #iig_subs = [ 'atl', 'summit', 'btcl', 'fiberathome', 'levelthree' ]
        final_data = {
            'day': [],
            'sav_in': [],
            'gp_in': [],
            'js_in': [],
            'total_in': [],
            'subscription': [],
        }
        try:
            days = []
            results = DailyPeakThroughput.objects.values('day').filter(day__range=(fromDay, toDay)).distinct('day').order_by('day')
            for r in results:
                day = r.get('day').strftime("%Y-%m-%d")
                final_data['day'].append(day)
                subscription = CapacityHelper().get_capacity(vendor, day)
                tt = self.get_peak_traffic_by_day( day, vendor )

                final_data['sav_in'].append(round(tt.get('sav_in') / 1000, 2))
                final_data['gp_in'].append(round(tt.get('gp_in') / 1000, 2))
                final_data['js_in'].append(round(tt.get('js_in') / 1000, 2))
                final_data['total_in'].append(round(tt.get('total_in') / 1000, 2))
                final_data['subscription'].append(round(subscription, 2))

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

        return final_data

    def get_peak_traffic_by_day(self, day, vendor):
        data = {}
        try:
            results = DailyPeakThroughput.objects.values('gp_in', 'sav_in', 'js_in', 'total_in').filter(day = day, vendor = vendor)
            #results = DailyPeakThroughput.objects.values('day', 'total_in', 'total_out').filter(day = day, vendor = vendor)
            data = {
                'total_in': results[0].get('total_in'),
                'sav_in': results[0].get('sav_in'),
                'gp_in': results[0].get('gp_in'),
                'js_in': results[0].get('js_in'),
            }
        except:
            data = {
                'total_in': 0,
                'sav_in': 0,
                'gp_in': 0,
                'js_in': 0,
            }

        return data


# query capacity table
class CapacityHelper:
    #get capacity
    def get_capacity(self, vendor, day=None):
        try:
            results = Capacity.objects.values(vendor).filter(day=day)
            return results[0][vendor]

        except:
            return 0


#Import & Export healper
class IoHelper:
    def export_by_range(self, fromDay=None, toDay=None):
        header = ['day', 'vendor', 'sav_in', 'sav_out', 'gp_in', 'gp_out', 'total_in', 'total_out', 'subscription']
        final_data = [ header]
        try:
            results = DailyPeakThroughput.objects.values('day', 'vendor', 'sav_in', 'sav_out', 'gp_in', 'gp_out', 'total_in', 'total_out').filter(day__range=(fromDay, toDay)).order_by('vendor', '-day')
            #print(results)
            for r in results:
                day = r.get('day').strftime("%Y-%m-%d")
                vendor = r.get('vendor')
                gp_in = round(r.get('gp_in') / 1000, 2)
                gp_out = round(r.get('gp_out') / 1000, 2)
                sav_in = round(r.get('sav_in') / 1000, 2)
                sav_out = round(r.get('sav_out') / 1000, 2)
                total_in = round(r.get('total_in') /1000, 2)
                total_out = round(r.get('total_out') /1000, 2)
                sub = CapacityHelper().get_capacity(vendor, day)
                #sub = 0
                final_data.append( [day, vendor, sav_in, sav_out, gp_in, gp_out, total_in, total_out, sub] )

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data


#Public IP IoHelper
class IPHelper:
    def get_ips(self):
        final_data = []
        try:
            ips = PublicIP.objects.values('ip', 'subnet', 'site', 'iig', 'bgp_path', 'bgp_meta', 'modified_date').order_by('-site', 'iig')
            for i, ip in enumerate(ips, 1):
                data = {
                    'index': i,
                    'ip':       "{0}/{1}".format(ip.get('ip'), ip.get('subnet')),
                    #'subnet':   ip.get('subnet'),
                    'site':     ip.get('site'),
                    'iig':      ip.get('iig'), #planned IIG
                    'bgp_path': " << ".join(ip.get('bgp_path').split(" ")),
                    'bgp_meta': ip.get('bgp_meta'),
                    'date':     ip.get('modified_date')
                }
                final_data.append(data)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    #this function will return a dictionary
    #{'as_number': 'IIG Name', ...}
    def get_asnumbers(self):
        data = {}
        try:
            as_numbers = IIGInformation.objects.values('as_number', 'name')
            for as_number in as_numbers:
                data[as_number.get('as_number')] = as_number.get('name')
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data
