from django.db import models
from datetime import date, timedelta


'''

site1: Savar
site2: Gazipur

'''


#summit communication Ltd
class Summit( models.Model ):
    datetime =  models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0) #mbps
    site_1_out = models.FloatField(default=0) #mbps
    site_2_in = models.FloatField(default=0) #mbps
    site_2_out = models.FloatField(default=0) #mbps
    site_3_in = models.FloatField(default=0) #mbps
    site_3_out = models.FloatField(default=0) #mbps



    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


#ATL
class ATL( models.Model ):
    datetime =  models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps


    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')



#Level Three data
class LevelThree( models.Model ):
    datetime =  models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps


    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


# Level Three data
class FiberAtHome(models.Model):
    datetime = models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps


    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


# Level of BDIx
class BdIx(models.Model):
    datetime = models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps


    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


#data of Novonix
class NovoNix(models.Model):
    datetime = models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps


    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


# data of BTCL
class BtCl(models.Model):
    datetime = models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps


    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')

# data of Mango
class MangoTel(models.Model):
    datetime = models.DateTimeField(blank=True)
    day = models.DateField(blank=True)
    hour = models.IntegerField(default=0)
    min = models.IntegerField(default=0)
    sec = models.IntegerField(default=0)
    site_1_in = models.FloatField(default=0)  # mbps
    site_1_out = models.FloatField(default=0)  # mbps
    site_2_in = models.FloatField(default=0)  # mbps
    site_2_out = models.FloatField(default=0)  # mbps
    site_3_in = models.FloatField(default=0)  # mbps
    site_3_out = models.FloatField(default=0)  # mbps

    def __str__(self):
        return self.datetime.strftime('%Y-%m-%d %H:%M:%S')


# holds capacity information
class Capacity(models.Model):
    day = models.DateField(blank=True)
    atl = models.FloatField(default=0)
    levelthree = models.FloatField(default=0)
    summit = models.FloatField(default=0)
    fiberathome = models.FloatField(default=0)
    btcl = models.FloatField(default=0)
    bdix = models.FloatField(default=0)
    novonix = models.FloatField(default=0)
    ggc = models.FloatField(default=0)
    fna = models.FloatField(default=0)
    akamai = models.FloatField(default=0)
    mangotel = models.FloatField(default=0)

    def __str__(self):
        return self.day.strftime('%Y-%m-%d')


#holds daily pick throughput of IIG
class DailyPeakThroughput(models.Model):
    day = models.DateField(blank=True)
    vendor = models.CharField(max_length=500, default=None, blank=True, null=True)
    gp_in = models.FloatField(default=0)  # mbps
    gp_out = models.FloatField(default=0)  # mbps
    sav_in = models.FloatField(default=0)  # mbps
    sav_out = models.FloatField(default=0)  # mbps
    js_in = models.FloatField(default=0)  # mbps
    js_out = models.FloatField(default=0)  # mbps
    total_in = models.FloatField(default=0)  # mbps
    total_out = models.FloatField(default=0)  # mbps

    def __str__(self):
        return self.day.strftime('%Y-%m-%d')

#holds public IP info
class PublicIP(models.Model):
    ip = models.GenericIPAddressField(protocol='both', default=None)
    subnet = models.IntegerField(default=24)
    site = models.CharField(max_length=500, default=None, blank=True, null=True)
    iig = models.CharField(max_length=500, default=None, blank=True, null=True)
    bgp_path = models.CharField(max_length=500, default=None, blank=True, null=True)
    bgp_meta = models.TextField(default=None, blank=True, null=True)
    modified_date = models.DateField(blank=True, default=date.today)

    def __str__(self):
        return "{0}/{1}".format(self.ip, self.subnet)

#IIG information
class IIGInformation(models.Model):
    name = models.CharField(max_length=500, default=None)
    as_number = models.IntegerField()
    remarks = models.TextField(default=None, blank=True, null=True)

    def __str__(self):
        return self.name


#Meta information and corresponding values
class SiteMeta(models.Model):
    meta_key = models.CharField(max_length=500, default=None, blank=True, null=True)
    meta_value = models.CharField(max_length=500, default=None, blank=True, null=True)
    remarks = models.CharField(max_length=500, default=None, blank=True, null=True)
