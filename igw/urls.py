from django.conf.urls import url
from .views import Dashboard, Ajax, IIG, IX, CDN, IO, IP


app_name = 'igw'
urlpatterns = [
    url(r'^$', Dashboard.as_view(), name='dashboard'),
    url(r'^ajax/$', Ajax.as_view(), name='ajax'),
    url(r'^iig/$', IIG.as_view(), name='iig'),
    url(r'^ix/$', IX.as_view(), name='ix'),
    url(r'^cdn/$', CDN.as_view(), name='cdn'),
    url(r'^io/$', IO.as_view(), name='io'),
    url(r'^ip/$', IP.as_view(), name='ip')
]
