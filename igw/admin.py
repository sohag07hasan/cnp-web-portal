from django.contrib import admin
from .models import Summit, ATL, LevelThree, FiberAtHome, NovoNix, BdIx, BtCl, MangoTel, Capacity, DailyPeakThroughput, PublicIP,  IIGInformation

#manageing ATL
class ATLAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']

#Managing Summit
class SummitAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out' )
    search_fields = ['day', 'hour']

#managing Level Three
class LevelThreeAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']

#Managing Fiber At Home
class FiberAtHomeAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']

#Managing BTCL
class BtClAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']

#Managing MangoTel
class MangoTelAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']

#Manging BDIX
class BdIxAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']

#Manging BDIX
class NovoNixAdmin( admin.ModelAdmin ):
    list_display = ('day', 'hour', 'min', 'sec', 'site_1_in', 'site_1_out', 'site_2_in', 'site_2_out', 'site_3_in', 'site_3_out')
    search_fields = ['day', 'hour']


#managing capacity
class CapacityAdmin(admin.ModelAdmin):
    list_display = ('day', 'atl', 'levelthree', 'summit', 'fiberathome', 'btcl', 'mangotel', 'bdix', 'novonix', 'ggc', 'fna', 'akamai')
    search_fields = ['day']


#showing Daily throughput report
class DailyPeakThroughputAdmin(admin.ModelAdmin):
    list_display = ('day', 'vendor', 'gp_in', 'gp_out', 'sav_in', 'sav_out', 'js_in', 'js_out', 'total_in', 'total_out')
    search_fields  = ['day', 'vendor']

#showing public IP GenericIPAddressField
class PublicIPAdmin(admin.ModelAdmin):
    list_display = ('ip', 'subnet', 'site', 'iig', 'bgp_path', 'modified_date')
    search_fields  = ['ip', 'bgp_path', 'site', 'iig']

class IIGInformationAdmin(admin.ModelAdmin):
    list_display = ('name', 'as_number', 'remarks')
    search_fields = ['name', 'as_number', 'remarks']


#registering Admin classes
admin.site.register( ATL, ATLAdmin )
admin.site.register( Summit, SummitAdmin )
admin.site.register( LevelThree, LevelThreeAdmin )
admin.site.register( FiberAtHome, FiberAtHomeAdmin )
admin.site.register( BtCl, BtClAdmin )
admin.site.register( MangoTel, MangoTelAdmin )
admin.site.register( NovoNix, NovoNixAdmin )
admin.site.register( BdIx, BdIxAdmin )
admin.site.register( Capacity, CapacityAdmin )
admin.site.register( DailyPeakThroughput, DailyPeakThroughputAdmin )
admin.site.register( PublicIP, PublicIPAdmin )
admin.site.register( IIGInformation, IIGInformationAdmin )
