from django.apps import AppConfig


class IgwConfig(AppConfig):
    name = 'igw'
