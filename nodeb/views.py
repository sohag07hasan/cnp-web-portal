from django.shortcuts import render
from django.views import View
from .forms import DataIO, LatLonFileUploadForm, FinalPlanDonwloadForm, SearchSites
from .helper import DataIOHelper, NodebPlanHelper, NodebAjaxHelper, SiteMetaHelper, LatLonDatabase, AboutHelper, SearchHelper, DownloadHelper
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
import django_excel as excel
from django.utils import timezone
from .models import HwSPU, NodebDatabase
from django.urls import reverse
import sys


#handles lat long file & allow downloading
class NodebPlan( LoginRequiredMixin, View ):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'
    context = {'title': 'CNP Plan: New 3G'}

    def get(self, request):
        form = LatLonFileUploadForm(initial={'plan_uploaded':'Y'})
        return render( request, 'nodeb/index.html', {'context': self.context, 'form': form} )


    #handle plan file upload
    def post(self, request):
        if request.POST.get('plan_uploaded') == 'Y' and request.FILES['file']:
            form = LatLonFileUploadForm(request.POST, request.FILES)
            if form.is_valid():
                data = NodebPlanHelper().process_latlong_file(request.FILES['file'])
            else:
                data = []
        else:

            try:
                PlanHelper = NodebPlanHelper() # instantiating Plan Helper

                header = ['WR Name', 'Site Name', 'RNC', 'LAC', 'RAC', 'PLMN Tag LAC Min', 'PLMN Tag LAC Max', 'PLMN Tag RAC Min', 'PLMN Tag RAC Max', 'URA ID', 'Subrack, Slot', 'RNC/BSC FG2C (Main) IP Address', 'Subnet mask RNC', 'BSC/RNC Gateway', 'FG2C PN', 'SPU SRN', 'SPU SN', 'SCTP LNKN 1', 'SCTP LNKN 2']
                d = [header]

                sites = request.POST.getlist('site')
                ipwave_wrs = request.POST.getlist('ipwave_wr')
                cnp_wrs = request.POST.getlist('cnp_wr')
                rncs = request.POST.getlist('rnc')
                lacs = request.POST.getlist('lac')
                racs = request.POST.getlist('rac')
                plmns = request.POST.getlist('plmn')
                uras = request.POST.getlist('ura')
                lats = request.POST.getlist('lat')
                lons = request.POST.getlist('lon')
                vendors = request.POST.getlist('vendor')

                for i in range(len(sites)):

                    rnc_info = PlanHelper.get_rnc_info(rncs[i], vendors[i])

                    if vendors[i].upper() == 'H':
                        plmn_lac_min = int(plmns[i])
                        plmn_lac_max = plmn_lac_min + 15
                        plmn_rac_min = plmn_lac_min + 16
                        plmn_rac_max = plmn_lac_min + 31

                        spu_info = PlanHelper.get_spu(rncs[i], rnc_info['vendor'])
                    else:
                        plmn_lac_min = ''
                        plmn_lac_max = ''
                        plmn_rac_min = ''
                        plmn_rac_max = ''
                        spu_info = { 'spu_srn': '', 'spu_sn': '', 'sctp_link1': '', 'sctp_link2': '' }


                    full_info = [ ipwave_wrs[i], sites[i], rnc_info['rncname'], lacs[i], racs[i], plmn_lac_min, plmn_lac_max, plmn_rac_min,
                             plmn_rac_max, uras[i], rnc_info['subrack_slot'], rnc_info['main_address'], rnc_info['subnet_mask'],
                             rnc_info['gateway_address'], rnc_info['port'], spu_info['spu_srn'], spu_info['spu_sn'], spu_info['sctp_link1'], spu_info['sctp_link2'] ]

                    d.append(full_info)

                    #updating spu info to adjust further
                    if vendors[i].upper() == 'H':
                        PlanHelper.update_spu_info(sites[i], spu_info)
                        PlanHelper.update_rnc_tracker(sites[i], rnc_info)

                    plan_date = "{:%Y-%m-%d}".format(timezone.now())
                    PlanHelper.save_at_nodeb_database(full_info, plan_date, request.user.username) #solving database
                    PlanHelper.save_latlong(sites[i], lats[i], lons[i]) #saving lat long file

                #naming attachments
                try:
                    attachment_name = "WR_" + cnp_wrs[0]
                except:
                    today_date = "{:%Y-%m-%d}".format(timezone.now())
                    attachment_name = "WR_" + today_date

                return excel.make_response_from_array(d, 'xls', file_name=attachment_name)
            except:
                print("Unexpected error:", sys.exc_info()[0])
                raise


        return render(request, 'nodeb/map.html', {'data': data, 'form': FinalPlanDonwloadForm, 'context': self.context})



#handle data e.g ucell data uploading
class ImportExport( LoginRequiredMixin, View ):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    #only page query
    def get(self, request):
        form = DataIO
        context = {'title': 'CNP 3G Plan: IO', 'form': form}
        return render(request, 'nodeb/io.html', {'context': context})

    #handle post requests ( ajax ) requests
    def post(self, request):
        form = DataIO(request.POST, request.FILES)
        data = {}
        if form.is_valid():
            if form.cleaned_data['type'] == 'ucell':
                data = DataIOHelper().save_ucell( request.FILES['file'], request )
                data['is_valid'] = True
                self.keep_log('ucell', 'mode: Ucell', request) #keeping log

            elif form.cleaned_data['type'] == 'spu':
                data = DataIOHelper().save_spu(request.FILES['file'], request, 'update')
                data['is_valid'] = True
                self.keep_log('spu', 'mode: HwSPU', request) #keeping log

            elif form.cleaned_data['type'] == 'newspu':
                data = DataIOHelper().save_spu(request.FILES['file'], request, 'new')
                data['is_valid'] = True
                self.keep_log('spu', 'mode: HwSPU', request) #keeping log

            elif form.cleaned_data['type'] == 'newucell':
                data = DataIOHelper().save_new_ucell( request.FILES['file'], request )
                data['is_valid'] = True
                self.keep_log('ucell', 'mode: Ucell', request)  # keeping log
            else:
                data['is_valid'] = False
        else:
            data['is_valid'] = False

        return JsonResponse(data)

    #keep track at site meta database
    def keep_log(self, type, comment, request = None):
        data = {
            'save_time': "{:%d-%m-%Y}".format(timezone.now()),
            'save_by': request.user.username
        }
        for key, value in data.items():
            SiteMetaHelper().save_log(type + '_' + key, value, comment)


#Handling ajax at plan page
class NodebAjax(View):
    def get(self, request):
        self.request = request
        if request.GET.get('hook') == 'nearestsites':
            limit = 100
            lat = request.GET.get('lat', None)
            lon = request.GET.get('lon', None)
            vendor = request.GET.get('vendor', None)
            data = NodebAjaxHelper().get_nearest_sites( lat, lon, vendor, limit);
        elif request.GET.get('hook') == 'recentplans':
            limit = 100
            data = NodebAjaxHelper().get_recent_plans(limit);
        else:
            data = []
        return JsonResponse(data, safe=False)  # default function to handle json form django 1.8


#About page
class About(View):
    def get(self, request):
        context = {
            'title': 'CNP 3G Plan: About',
            'ucell': AboutHelper().get_ucell_info(),
            'spu': AboutHelper().get_spu_info(),
        }
        return render(request, 'nodeb/about.html', {'context': context})


#Search page
class Search(View):
    def get(self, request):

        if request.is_ajax():
            data = []
            if request.GET.get('hook', None) == 'search':
                form = SearchSites(request.GET)
                if form.is_valid():
                    limit = 350
                    data = SearchHelper().get_nearest_sites( form.cleaned_data['s'].strip(), limit )

            return JsonResponse(data, safe=False)

        else:
            context = {
                'title': 'CNP 3G Plan: Search',
                'form': SearchSites
            }
            return render(request, 'nodeb/search.html', {'context': context})


# Download links
class Download(View):
    def get(self, request, type=None):
        download_date = "{:%Y-%m-%d}".format(timezone.now())
        if type == 'spu':
            #data = DownloadHelper().get_whole_spu()
            data = DownloadHelper().get_whole_old_spu()
            return excel.make_response_from_array(data, 'xlsx', file_name='SPU_DB_' + download_date)
        elif type == 'latlon':
            data = DownloadHelper().get_whole_latlon()
            return excel.make_response_from_array(data, 'xlsx', file_name='LatLon_DB_' + download_date)
        elif type == 'ucell':
            data = DownloadHelper().get_whole_ucell()
            return excel.make_response_from_array(data, 'xlsx', file_name='UCELL_' + download_date)
        else:
            return HttpResponseRedirect(reverse('nodeb:io'))
