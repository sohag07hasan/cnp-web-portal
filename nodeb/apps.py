from django.apps import AppConfig


class NodebConfig(AppConfig):
    name = 'nodeb'
