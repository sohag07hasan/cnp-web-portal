from django.db import models

class Ucell( models.Model ):
    sitename = models.CharField(max_length=500)
    rncname = models.CharField(max_length=500)
    lac = models.IntegerField(default=0)
    rac = models.IntegerField(default=0)
    plmn = models.CharField(max_length=500, default='')
    ura = models.IntegerField(default=0)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)

    def __str__(self):
        return self.sitename


# meta information
# e.g who saves Gcell and When it is saved etc
class SiteMeta(models.Model):
    meta_key = models.CharField(max_length=500, default=None, blank=True, null=True)
    meta_value = models.CharField(max_length=500, default=None, blank=True, null=True)
    remarks = models.CharField(max_length=500, default=None, blank=True, null=True)


#RNC info
class HwRNC(models.Model):
    rncname = models.CharField(max_length=500)
    vendor = models.CharField(max_length=500)
    subrack_slot = models.CharField(max_length=500)
    board = models.CharField(max_length=500)
    port_number = models.IntegerField(default=0)
    main_address = models.CharField(max_length=500, default=None)
    subnet_mask = models.CharField(max_length=500, default=None)
    gateway_address = models.CharField(max_length=500, default=None)

    def __str__(self):
        return self.rncname


#Ericsson RNC information
class ERNC(models.Model):
    rncname = models.CharField(max_length=500)
    vendor = models.CharField(max_length=500)
    main_address = models.CharField(max_length=500, default=None)
    subnet_mask = models.CharField(max_length=500, default=None)
    gateway_address = models.CharField(max_length=500, default=None)

    def __str__(self):
        return self.rncname


#SPU information
class HwSPU(models.Model):
    rncname = models.CharField(max_length=500)
    vendor = models.CharField(max_length=500, default=None)
    spu_srn = models.IntegerField(default=55)
    spu_sn = models.IntegerField(default=55)
    sctp_link1 = models.IntegerField(default=1000)
    sctp_link2 = models.IntegerField(default=1001)

    def __str__(self):
        return self.rncname


#separate SPU informaiton for RNC6900
#This deb contains information of SPU info for all RNC6900
#6900 spu_srn, spu_sn, stcpt_link1 & sctp2 must be unique
#for 6901, sctp_link1 & sctp_line2 must be unique. we put a default value of 55 for both spu_srn & spu_sn
class OldHwSPU(models.Model):
    sitename = models.CharField(max_length=500)
    rncname = models.CharField(max_length=500)
    vendor = models.CharField(max_length=500, default=None)
    spu_srn = models.IntegerField(default=55)
    spu_sn = models.IntegerField(default=55)
    sctp_link1 = models.IntegerField(default=1000)
    sctp_link2 = models.IntegerField(default=1001)

    def __str__(self):
        return self.sitename



#saves initial site info and it will be shown later at about page
class NodebDatabase(models.Model):
    sitename = models.CharField(max_length=500)
    rncname = models.CharField(max_length=500)
    lac = models.IntegerField(default=0)
    rac = models.IntegerField(default=0)
    plmn_lac_min = models.CharField(max_length=500, default='')
    plmn_lac_max = models.CharField(max_length=500, default='')
    plmn_rac_min = models.CharField(max_length=500, default='')
    plmn_rac_max = models.CharField(max_length=500, default='')
    ura = models.IntegerField(default=0)
    subrack_slot = models.CharField(max_length=500)
    rnc_main_address = models.CharField(max_length=500, default=None)
    rnc_gateway_address = models.CharField(max_length=500, default=None)
    rnc_subnet_mask = models.CharField(max_length=500, default=None)
    board_port = models.CharField(max_length=500, default='')
    spu_srn = models.CharField(max_length=500, default='')
    spu_sn = models.CharField(max_length=500, default='')
    sctp_link1 = models.CharField(max_length=500, default='')
    sctp_link2 = models.CharField(max_length=500, default='')
    plan_date = models.DateField()
    username = models.CharField(default='NA', max_length=500)

    def __str__(self):
        return self.sitename


#latitude & longitude database
class LatLonDatabase(models.Model):
    sitename = models.CharField(max_length=500)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)

    def __str__(self):
        return self.sitename


#RNC SCTP Link Tracker
class RNCTracker(models.Model):
    sitename = models.CharField(max_length=500)
    rncname = models.CharField(max_length=500)
    rnc_ip = models.CharField(max_length=500)

    def __str__(self):
        return self.rncname
