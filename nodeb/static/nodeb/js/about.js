/**
 * Created by mahibul.hasan on 24/04/2017.
 */
jQuery(document).ready(function ($) {

    /** showing tab wise information **/
    $('.secondary_navigation').find('li').click(function (evt) {
        var div_id = $(this).attr('tab');
        $('.nav_details').addClass('display_none');
        $('.secondary_navigation').find('li').removeClass('active');
        $(this).addClass('active');
        $('#'+div_id).removeClass('display_none');
        return false;
    });


    //function to handle ajax
    function process_ajax(method, input_data, success_function){
        $.ajax({
            url: NewSitePlan.ajax_url,
            method: method,
            dataType: 'json',
            async: input_data.async,
            data: input_data,
            success: function (ajax_data) {
                success_function( ajax_data, input_data );
            },
            error: function () {
                alert('Error: Please check excel file again and try!');
            }
        });
    }


    /** Process recent plans  * */
    function process_recent_plans( ajax_data, input_data ){
        if (ajax_data.length >= 0){
            plan_table_body = $('#recently_planned_body');
            for( var i=0; i<=ajax_data.length; i++ ){
                plan_row = "<tr>";
                plan_row += "<td>" + ajax_data[i].site + "</td>";
                plan_row += "<td>" + ajax_data[i].coordinate + "</td>";
                plan_row += "<td>" + ajax_data[i].rnc + "</td>";
                plan_row += "<td>" + ajax_data[i].lac + "</td>";
                plan_row += "<td>" + ajax_data[i].rac + "</td>";
                plan_row += "<td>" + ajax_data[i].ura + "</td>";
                plan_row += "<td>" + ajax_data[i].rnc_ip + "</td>";
                plan_row += "<td>" + ajax_data[i].plan_date + "</td>";
                plan_row += "<td>" + ajax_data[i].username + "</td>";
                plan_row += "</tr>";
                plan_table_body.append(plan_row);
            }
        }
    }

    /** using ajax to show recent plans **/
    var data = {
        'hook': 'recentplans'
    };
    process_ajax('get', data, process_recent_plans)

});
