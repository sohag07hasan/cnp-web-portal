 jQuery(document).ready(function ($) {

        var map;
        function initMap(input_data, data){

            if( input_data.s.match(/,/) ){
                latlon = input_data.s.split(',');
                lat = latlon[0];
                lon = latlon[1];
                title = input_data.s;
            }
            else{
                lat = data[0].lat;
                lon = data[0].lon;
                title = data[0].site;
            }

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: new google.maps.LatLng(lat, lon),
                mapTypeId: 'terrain'
            });

            //creating marker of new site
            var marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: Search.static_url + 'images/google_icons/blue-dot.png',
                map: map,
                title: title
            });


            //color block
            var colors = ['blue', 'red', 'yellow', 'green', 'purple', 'oceanblue', 'pink', 'maron', 'grey', 'deepgreen', 'parrot', 'navyblue', 'oceanblue'];
            var color_index = 0;
            lac_list=[];
            color_list=[];


            for (var i=0; i<data.length; i++){
                if(!lac_list.includes(data[i].lac)){
                    lac_list.push(data[i].lac);
                    color_list.push(colors[color_index]);
                    color_index ++;
                    if(color_index == colors.length) color_index = 0; // forcely making color index zero
                }

                var latLng = new google.maps.LatLng(data[i].lat,data[i].lon);
                var marker_title = data[i].site + ' ' + data[i].rnc + ' ' + data[i].lac;

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: marker_title,
                    icon: Search.static_url + 'images/google_icons/' + color_list[lac_list.indexOf(data[i].lac)] + '.png'
                });


               // var info_content = '<h4>'+data[i].site+ ' - '+data[i].bsc+' - '+data[i].lac+'</h4>';
                var info_content = '<h4>'+data[i].site+ ' - '+data[i].rnc+'</h4>';
                info_content += generate_info_window_content(data[i]);

                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(marker, 'click', (function(marker, info_content, map) {
                    return function () {
                        infowindow.setContent(info_content);
                        infowindow.open(map, marker);
                    }

                })(marker, info_content, map));

            }

        }

        function generate_info_window_content(data) {
            var content = '<table class="table table-sm">';
            content += '<thead><tr><th>LAC</th><th>RAC</th><th>PLMN</th><th>URA</th></tr></thead>';
            content += '<tbody><tr>' + '<td>' + data.lac + '</td>' + '<td>' + data.rac + '</td>' + '<td>' + data.plmn + '</td>' + '<td>' + data.ura + '</td>';
            content += '<tbody></table';
            return content;
        }


        //function to handle ajax
         function process_ajax(method, input_data, success_function){
            $.ajax({
                url: Search.ajax_url,
                method: method,
                dataType: 'json',
                async: input_data.async,
                data: input_data,
                success: function (ajax_data) {
                    success_function(ajax_data, input_data);
                },
                error: function () {
                    alert('Error! Please consult with the site admin');
                }
            });
         }

         //process ajax and initiate map
         function process_nearest_sites(ajax_data, input_data){
             if(ajax_data.length > 0) {
                 initMap(input_data, ajax_data);
                 $('div#map').css({'border': '1px solid blue'});
             }
             else{
                 alert("Nothing matched with the search")
             }
         }

        //trigger the whole thing when an user clicks here
        $("#search_sites").click(function () {
            var search_text = $("input[name='s']").val();
            search_text = search_text.replace(/\s/g, "");
            if( search_text.length >= 7 ) {
                var data = {
                    'hook': 'search',
                    's': search_text,
                    'async': true
                }
                process_ajax('GET', data, process_nearest_sites);
            }
            else{
                alert('Site name must be 7 character long');
            }
            return false;
        });

        //hooking enter with the search button
        $('input[name=s]').keypress(function (e) {
             var key = e.which;
             if(key == 13)  // the enter key code
              {
                  $("#search_sites").click();
                  return false;
              }
         });

});


