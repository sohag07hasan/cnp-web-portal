
jQuery(document).ready(function ($) {
    var files;

    $('input[type=file]').on('change', prepareUpload);
    function prepareUpload(event){
      files = event.target.files;
    }

    $('form#ioform').on( 'submit', upLoadFiles );
    function upLoadFiles(event) {
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening

        var type = $('#id_type').val();

        if ( type == 'ucell' ){
            var message = 'Ucell ( Update )';
        }
        else if( type == "newucell" ){
            var message = "Ucell ( New )";
        }
        else if( type == 'spu' ){
            var message = 'SPU ( Update )';
        }
        else if( type == 'newspu' ){
            var message = 'SPU ( New )';
        }
        else{
            var message = "Not Supported file type";
        }

        if(confirm( "You are going to import " + message )) {
            var data = new FormData($('form#ioform')[0]);
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            data.append('type', type); // including excel type
            process_ajax(data, submitForm); // file uploading starting
        }
        else{
            return false;
        }
    }

   //function to handle ajax
     function process_ajax(input_data, success_function){
        var csrftoken = Cookies.get('csrftoken');

        $.ajax({
            beforeSend: function () {
                $('#submit_button').val('Uploading..');
                $('#submit_button').addClass('processing');
                $('#upload_progress').removeClass('display_none');
                $('#ajax_processing_message').removeClass('display_none');
            },
            url: NodebPlanIO.ajax_url,
            method: 'POST',
            dataType: 'json',
            cache: false,
            async: true,
            processData: false,
            contentType: false,
            data: input_data,
            headers:{'X-CSRFToken': csrftoken},
            success: function (ajax_data) {
                success_function(ajax_data, input_data);
            },
            error: function () {
                alert('Error: Please check excel file again and try!');
            },
            complete: function () {
                $('#submit_button').val('Start Uploading');
                $('#submit_button').removeClass('processing');
                $('#upload_progress').addClass('display_none');
                $('#ajax_processing_message').addClass('display_none');
                $('#ioform').trigger("reset");

            }
        });
     }

     //runs it when file uplad done
     function submitForm( ajax_data, input_data ){
         $('#submit_button').val('Start Uploading');

         if(input_data.get('type') == 'spu'){
             var title = 'SPU';
             var message = ' Newly created: ' + ajax_data.created +', Updated: ' + ajax_data.updated + ' & Failed: ' + ajax_data.failure
         }
         else if(input_data.get('type') == 'ucell'){
              var title = 'Ucell';
              var message = ' Newly Created: ' + ajax_data.created +', Updated: ' + ajax_data.updated + ' & Failed: ' + ajax_data.failure
         }
         else{
              var title = 'Ucell';
              var message = ' Deleted: ' + ajax_data.deleted + ' Newly Created: ' + ajax_data.created + ' & Failed: ' + ajax_data.failure
         }


         $('#success_message').html(message);

         $('.alert-success').find('strong').html(title);
         $('.alert-success').removeClass('display_none');
     }
})