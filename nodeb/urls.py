from django.conf.urls import url
from .views import NodebPlan, ImportExport, NodebAjax, About, Search, Download

app_name = 'nodeb'
urlpatterns = [
    url(r'^$', NodebPlan.as_view(), name='nodeb-plan'),
    url(r'^nodebajax/$', NodebAjax.as_view(), name='nodebajax'),
    url(r'^io/$', ImportExport.as_view(), name='io'),
    url(r'^about/$', About.as_view(), name='about'),
    url(r'^search/$', Search.as_view(), name='search'),
    url(r'^download/(?P<type>[a-z]+)/$', Download.as_view(), name='download')
]