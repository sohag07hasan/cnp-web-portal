from django.contrib import admin
from .models import Ucell, HwRNC, ERNC, LatLonDatabase, NodebDatabase, RNCTracker, HwSPU, OldHwSPU

#managing ucell from admin
class UcellAdmin( admin.ModelAdmin ):
    list_display = ( 'sitename', 'rncname', 'lac', 'rac', 'plmn', 'ura', 'latitude', 'longitude' )
    search_fields = ( 'sitename', 'rncname', 'lac' )


#manageing IP data of HW RNC from admin panel
class HwRNCAdmin( admin.ModelAdmin ):
    list_display = ( 'rncname', 'vendor', 'subrack_slot', 'board', 'port_number', 'main_address', 'subnet_mask', 'gateway_address' )
    search_fields = ( 'rncname', 'vendor', 'board', 'main_address' )


#managing ericsson rnc info from admin panel
class ERNCAdmin( admin.ModelAdmin ):
    list_display = ( 'rncname', 'vendor', 'main_address', 'subnet_mask', 'gateway_address' )
    search_fields = ('rncname', 'vendor', 'main_address')


#manaigng SPU information from applicaiton admin panel
class HwSPUAdmin(admin.ModelAdmin):
    list_display = ( 'rncname', 'vendor', 'spu_srn', 'spu_sn', 'sctp_link1', 'sctp_link2' )
    search_fields = ( 'rncname', 'vendor', 'sctp_link1', 'sctp_link2')


#manaigng SPU information from applicaiton admin panel
class OldHwSPUAdmin(admin.ModelAdmin):
    list_display = ( 'sitename', 'rncname', 'vendor', 'spu_srn', 'spu_sn', 'sctp_link1', 'sctp_link2' )
    search_fields = ( 'sitename', 'rncname', 'vendor', 'sctp_link1', 'sctp_link2')


#latitude Longitude database
class LatLonDatabaseAdmin(admin.ModelAdmin):
    list_display = ('sitename', 'latitude', 'longitude')
    search_fields = ('sitename',)


#NodeB Database
class NodebDatabaseAdmin(admin.ModelAdmin):
    list_display = ('sitename', 'rncname', 'lac', 'rac', 'ura', 'rnc_main_address', 'rnc_gateway_address', 'rnc_subnet_mask', 'board_port', 'spu_srn', 'spu_sn', 'sctp_link1', 'sctp_link2', 'plan_date', 'username')
    search_fields = ('sitename', 'rncname', 'lac', 'plan_date', 'username')


#SCTP Link Tracker
class RNCTrackerAdmin(admin.ModelAdmin):
    list_display = ('sitename', 'rncname', 'rnc_ip')
    search_fields = ('sitename', 'rncname', 'rnc_ip')




admin.site.register( Ucell, UcellAdmin )
admin.site.register( HwRNC, HwRNCAdmin )
admin.site.register( ERNC, ERNCAdmin )
admin.site.register( HwSPU, HwSPUAdmin )
admin.site.register( OldHwSPU, OldHwSPUAdmin )
admin.site.register( LatLonDatabase, LatLonDatabaseAdmin )
admin.site.register( NodebDatabase, NodebDatabaseAdmin )
admin.site.register( RNCTracker, RNCTrackerAdmin )