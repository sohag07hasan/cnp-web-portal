from .models import Ucell, SiteMeta, ERNC, HwRNC, RNCTracker, HwSPU, NodebDatabase, LatLonDatabase, OldHwSPU
from django.utils import timezone
from django.db.models import Func, Value, Count, F, CharField, Max, Min
import sys


#class that will save DATA IO
class DataIOHelper:

    ucell_columns = ["NodeB Name", "RNC Name", "LAC", "RAC", "PLMN", "URA ID", "LAT", "LON"]
    #spu_columns = ["RNC Name", "Vendor", "Subrack", "Slot", "SCTP Link1", "SCTP Link2"]
    spu_columns = ['NodeB', 'RNC', 'Vendor', 'Subrack_No', 'Slot_No', 'SCTP_LINK1', 'SCTP_LINK2']

    def save_ucell( self, filehandler, request = None ):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)  # removing spaces
        created = 0
        failure = 0
        updated = 0
        is_valid = True

        index = 1

        for row in sheet:

            if index == 1:
                col = 0
                for col_name in self.ucell_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if colum is found valid, then it will delte all the existing records
                if is_valid == False:
                    break

            else:
                try:
                    u = Ucell.objects.get(sitename = row[0])
                    u.rncname = row[1]
                    u.lac = self.formatting(row[2], 'int')
                    u.rac = self.formatting(row[3], 'int')
                    u.plmn = row[4]
                    u.ura = self.formatting(row[5], 'int')
                    u.latitude = self.formatting(row[6], 'float')
                    u.longitude = self.formatting(row[7], 'float')
                    u.save()
                    updated += 1
                except Ucell.DoesNotExist:
                    u = Ucell(
                        sitename=row[0], rncname=row[1],
                        lac=self.formatting(row[2], 'int'),
                        rac=self.formatting(row[3], 'int'),
                        plmn=row[4],
                        ura=self.formatting(row[5], 'int'),
                        latitude=self.formatting(row[6], 'float'),
                        longitude=self.formatting(row[7], 'float')
                    )
                    u.save()
                    created += 1
                except:
                    failure += 1
                    print("Unexpected error:", sys.exc_info()[0])
                    raise

            index += 1

        data = {
            'updated': updated,
            'created': created,
            'failure': failure,
            'is_valid': is_valid
        }
        return data

    #create new ucell, It will delete previous cell data
    def save_new_ucell( self, filehandler, request = None ):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)  # removing spaces
        created = 0
        failure = 0
        deleted_ucells = 0
        is_valid = True

        index = 1

        for row in sheet:

            if index == 1:
                col = 0
                for col_name in self.ucell_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if colum is found valid, then it will delte all the existing records
                if is_valid == True:
                    deleted = Ucell.objects.all().delete()  # deleting ucells before importing new cells
                    deleted_ucells = deleted[0]
                else:
                    break

            else:
                try:
                    u = Ucell(
                        sitename = row[0], rncname = row[1],
                        lac = self.formatting(row[2], 'int'),
                        rac = self.formatting(row[3], 'int'),
                        plmn = row[4],
                        ura = self.formatting(row[5], 'int'),
                        latitude = self.formatting(row[6], 'float'),
                        longitude = self.formatting(row[7], 'float')
                    )
                    u.save()
                    created += 1

                except:
                    failure += 1
                    print("Unexpected error:", sys.exc_info()[0])
                    raise

            index += 1

        data = {
            'created': created,
            'failure': failure,
            'deleted': deleted_ucells,
            'is_valid': is_valid
        }
        return data

    #formatting
    def formatting(self, data, type=None):
        if type == 'int':
            try:
                data = int(data)
            except:
                data = 0
        elif type == 'float':
            try:
                data = float(data)
            except:
                data = 0.00

        return data


    #saving spu info
    #previous function save_spu
    def save_spu_backup(self, filehandler, request = None, type = None):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)  # removing spaces

        created = 0
        failure = 0
        index = 1
        updated = 0

        is_valid = True

        for row in sheet:

            if index == 1:
                col = 0
                for col_name in self.spu_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if colum is found valid, then it will delte all the existing records
                if is_valid == False:
                    break

            else:
                try:
                    vendor = row[2].upper()
                    if vendor == 'HW_6900':
                        try:
                            spu = HwSPU.objects.get( rncname = row[1], vendor = vendor, spu_srn = int(row[3]), spu_sn = int(row[4]), sctp_link1 = int(row[5]), sctp_link2 = int(row[6]) )
                            spu.sctp_link1 = int(row[4])
                            spu.sctp_link2 = int(row[5])
                            spu.save()
                            updated += 1
                        except HwSPU.DoesNotExist:
                            spu = HwSPU(
                                rncname=row[0], vendor=vendor, spu_srn=int(row[2]), spu_sn=int(row[3]),
                                sctp_link1=int(row[4]), sctp_link2=int(row[5])
                            )
                            spu.save()
                            created += 1
                        except:
                            print("Unexpected error:", sys.exc_info()[0])
                            raise
                    else:
                        try:
                            spu = HwSPU.objects.get(rncname=row[0], vendor=vendor)
                            spu.sctp_link1 = int(row[4])
                            spu.sctp_link2 = int(row[5])
                            spu.save()
                            updated += 1
                        except HwSPU.DoesNotExist:
                            spu = HwSPU(
                                rncname=row[0], vendor=vendor, spu_srn=55, spu_sn=55,
                                sctp_link1=int(row[4]), sctp_link2=int(row[5])
                            )
                            spu.save()
                            created += 1
                        except:
                            print("Unexpected error:", sys.exc_info()[0])
                            raise

                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    raise
                    failure += 1
            index += 1

        data = {
            'created': created,
            'updated': updated,
            'failure': failure,
            'is_valid': is_valid
        }

        return data

    #saving Spu informaiton
    def save_spu(self, filehandler, request=None, type=None):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)  # removing spaces

        created = 0
        failure = 0
        index = 1
        updated = 0

        is_valid = True

        for row in sheet:
            if index == 1:
                col = 0
                for col_name in self.spu_columns:
                    if (col_name.upper() != row[col].strip().upper()):
                        is_valid = False
                        break
                    col += 1

                # if any column is invalid, it will break the loop
                # also if colum is found valid, then it will delte all the existing records
                if is_valid == False:
                    break

            else:
                vendor = row[2].upper()
                try:
                    spu = OldHwSPU(sitename=row[0], rncname=row[1], vendor=vendor, spu_srn=int(row[3]), spu_sn=int(row[4]), sctp_link1=int(row[5]), sctp_link2=int(row[6]))
                    spu.save()
                    created += 1
                except:
                    failure += 1
            index += 1

        data = {
            'created': created,
            'updated': updated,
            'failure': failure,
            'is_valid': is_valid
        }

        return data


    # cleaning unwanted characters from sheet
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v



#this class will keep track/log of site meta information
class SiteMetaHelper:
    #save key value pair at database
    def save_log(self, key, value, comments):
        try:
            obj = SiteMeta.objects.get(meta_key=key)
            obj.meta_value = value
            obj.remarks = comments
            obj.save()
        except SiteMeta.DoesNotExist:
            obj = SiteMeta(meta_key=key, meta_value=value, remarks=comments)
            obj.save()

    #get meta log
    def get_site_meta(self, meta_key):
        try:
            obj = SiteMeta.objects.get(meta_key=meta_key)
            meta_value = obj.meta_value
        except:
            meta_value = 'Not Found'
        return meta_value




#this class will help to process latlong file uploader
class NodebPlanHelper:
    def get_nearest_site(self, lat, lon, vendor=None):
        d = {}
        try:
            if vendor.lower() == 'h':
                result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell where rncname not like %s order by 7 asc limit 1", [lon, lat, '%ERNC%'])
            elif vendor.lower() == 'e':
                result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell where rncname like %s order by 7 asc limit 1", [lon, lat, '%ERNC%'])
            else:
                result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell order by 7 asc limit 1", [lon, lat])
                #result = None
            for x in result:
                d = {
                    'site': x.sitename,
                    'rnc': x.rncname,
                    'lac': x.lac,
                    'lat': x.latitude,
                    'lon': x.longitude,
                    'rac': x.rac,
                    'plmn': x.plmn,
                    'ura': x.ura,
                    'distance': round(x.distance_miles, 2)
                }
        except:
            d = {}
        return d


    def process_latlong_file(self, filehandler=None):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)
        index = 0
        data = []
        for row in sheet:
            if index > 0:
                neighbor = self.get_nearest_site(row[3], row[4], row[5]) #latitude & longitude & vendor
                if 'site' in neighbor.keys():
                    d = {
                        'cnpwr': row[0],
                        'ipwavewr': row[1],
                        'site': row[2],
                        'lat': row[3],
                        'lon': row[4],
                        'nearest_site': neighbor.get('site'),
                        'proposed_rnc': neighbor.get('rnc'),
                        'proposed_lac': neighbor.get('lac'),
                        'proposed_rac': neighbor.get('rac'),
                        'proposed_plmn': neighbor.get('plmn'),
                        'proposed_ura': neighbor.get('ura'),
                        'distance': neighbor.get('distance'),
                        'vendor': row[5]
                    }
                    data.append(d)
            index += 1
        return data

    # cell cleaning of excel files
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v


    #get rence subrack & slot
    def get_rnc_info(self, rncname, vendor=None):
        rncname = rncname.upper()
        data = {}
        try:

            if vendor.upper() == 'H':
                rnc_ip = self.get_proper_rnc_ip(rncname)
                info = HwRNC.objects.get(main_address=rnc_ip, rncname=rncname)
                data['subrack_slot'] = info.subrack_slot
                data['port'] = info.port_number
            else:
                info = ERNC.objects.get(rncname=rncname)
                data['port'] = ''
                data['subrack_slot'] = ''

            data['main_address'] = info.main_address
            data['subnet_mask'] = info.subnet_mask
            data['gateway_address'] = info.gateway_address
            data['vendor'] = info.vendor
            data['rncname'] = info.rncname
        except:
            #print("Unexpected error:", sys.exc_info()[0])
            data['rncname'] = 'Not Found!'
            data['subrack_slot'] = 'NA'
            data['port'] = 'NA'
            data['main_address'] = 'NA'
            data['subnet_mask'] = 'NA'
            data['gateway_address'] = 'NA'
            data['vendor'] = 'NA'

        return data


    #getting proper RNC info based on rnc tracker
    def get_proper_rnc_ip(self, rncname):
        selected_ip = None
        try:
            tracker = {}
            rnc_ip = {}

            tracker_info = RNCTracker.objects.raw("SELECT COUNT(id) as id, rnc_ip FROM nodeb_rnctracker WHERE rncname = %s group by rnc_ip order by id ASC", [rncname])
            for t in tracker_info:
                tracker[t.rnc_ip] = t.id

            rnc_ip_info = HwRNC.objects.filter(rncname=rncname).values_list('main_address').distinct()
            for r in rnc_ip_info:
                rnc_ip[r[0]] = 0

            rnc_ip.update(tracker)
            ip_counts = sorted(list(rnc_ip.values()))
            for ip, cnt in rnc_ip.items():
                if int( cnt ) == int ( ip_counts[0] ):
                    selected_ip = ip
                    break

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

        print(selected_ip)
        return selected_ip

    #retun spu info
    def get_spu_info(self, rncname):
        data = {}
        try:
            spu_with_minimum_sctp = HwSPU.objects.filter(rncname = rncname).order_by('sctp_link1')[:1]
            data['vendor'] = spu_with_minimum_sctp[0].vendor #assgning vendor
            data['rncname'] = spu_with_minimum_sctp[0].rncname #assgning vendor
            data['sctp_link1'] = int(spu_with_minimum_sctp[0].sctp_link1) + 2
            data['sctp_link2'] = int(spu_with_minimum_sctp[0].sctp_link1) + 3

            if spu_with_minimum_sctp[0].vendor == 'HW_6900':
                data['spu_srn'] = spu_with_minimum_sctp[0].spu_srn
                data['spu_sn'] = spu_with_minimum_sctp[0].spu_sn
            else:
                data['spu_srn'] = ''
                data['spu_sn'] = ''
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return data


    #return spu informaiton based on new functionalilty
    def get_old_spu_info(self, rncname, vendor):
        data = {}
        try:
            spu_info = OldHwSPU.objects.annotate(ss = Func( F('spu_srn'), Value('_'), F('spu_sn'), function='CONCAT', output_field=CharField() )).filter(rncname = rncname).values('ss').annotate(site_count = Count('sitename')).order_by("site_count")[:1]

            subrack_slot = spu_info[0].get('ss')
            ss = subrack_slot.split("_")

            #sctp_info = OldHwSPU.objects.filter(rncname = rncname, spu_srn = int(ss[0]), spu_sn = int(ss[1])).aggregate(sctp_max = Max('sctp_link1'), sctp_min = Min('sctp_link1'))
            info = OldHwSPU.objects.filter(rncname=rncname, spu_srn=int(ss[0]), spu_sn=int(ss[1])).values("sctp_link1", "vendor")
            sctp_links = []
            for sctp in info:
                sctp_links.append(sctp.get('sctp_link1'))

            sctp_links = list(set(sctp_links)) #making uniqe list

            #min_sctp, max_sctp = min(sctp_links), max(sctp_links)
            all_sctp_links = range(0, 1198, 2) # maxium sctp number can be 1198 for HW_6900

            if vendor.upper() == "HW_6910":
                all_sctp_links = range(0, 5000, 2)

            selected_sctp_link = 0
            for i in all_sctp_links:
                if i not in sctp_links:
                    selected_sctp_link = i
                    break

            data['vendor'] = info[0].get('vendor')
            data['rncname'] = rncname
            data['sctp_link1'] = selected_sctp_link
            data['sctp_link2'] = selected_sctp_link + 1
            if info[0].get('vendor') == 'HW_6900':
                data['spu_srn'] = ss[0]
                data['spu_sn'] = ss[1]
            else:
                data['spu_srn'] = ''
                data['spu_sn'] = ''
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

        return data

    # updating different information after a new site plan.It will be reflected immediately
    def update_spu_info(self, sitename, info):
        try:

            info['sctp_link1'] = int(info['sctp_link1'])
            info['sctp_link2'] = int(info['sctp_link2'])
            if info['vendor'] == 'HW_6900':
                info['spu_srn'] = int(info['spu_srn'])
                info['spu_sn'] = int(info['spu_sn'])
            else:
                info['spu_srn'] = 55
                info['spu_sn'] = 55

            spu = OldHwSPU(sitename=sitename, rncname=info['rncname'], vendor=info['vendor'], spu_srn=info['spu_srn'], spu_sn=info['spu_sn'], sctp_link1=info['sctp_link1'], sctp_link2=info['sctp_link2'])
            spu.save()

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    # updating RNC Tracker
    def update_rnc_tracker(self, sitename, rnc_info):
        try:
            trackers = RNCTracker.objects.filter(sitename = sitename)[:1]
            if len(trackers) >= 1:
                for tracker in trackers:
                    tracker.rncname = rnc_info['rncname']
                    tracker.rnc_ip = rnc_info['main_address']
                    tracker.save()
            else:
                tracker = RNCTracker(sitename = sitename, rncname = rnc_info['rncname'], rnc_ip = rnc_info['main_address'])
                tracker.save()
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    #saving whole node b info at NodebDatabase
    def save_at_nodeb_database(self, info, plan_date, username):
        try:
            new_nodeb = NodebDatabase(
                sitename = info[1], rncname = info[2], lac = int(info[3]), rac = int(info[4]), plmn_lac_min = info[5],
                plmn_lac_max = info[6], plmn_rac_min = info[7], plmn_rac_max = info[8], ura = int(info[9]),
                subrack_slot = info[10], rnc_main_address = info[11], rnc_subnet_mask = info[12], rnc_gateway_address = info[13],
                board_port = info[14], spu_srn = info[15], spu_sn = info[16], sctp_link1 = info[17],
                sctp_link2 = info[18], plan_date = plan_date, username = username
            )
            new_nodeb.save()
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise


    #saving lat long file
    def save_latlong(self, sitename, latitude, longitude):
        try:
            site = LatLonDatabase.objects.get(sitename = sitename)
            site.latitude = float(latitude)
            site.longitude = float(longitude)
            site.save()
        except:
            site = LatLonDatabase(sitename = sitename, latitude = latitude, longitude = longitude)
            site.save()


#ajax helper
class NodebAjaxHelper:
    # returnlist of nearest sites based on distance
    def get_nearest_sites(self, lat, lon, vendor=None, query_limit=100):
        try:
            if vendor.strip().lower() == 'h':
                result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell where rncname not like %s order by 7 asc limit %s", [lon, lat, '%ERNC%', query_limit])
            elif vendor.strip().lower() == 'e':
                result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell where rncname like %s order by 7 asc limit %s", [lon, lat, '%ERNC%', query_limit])
            else:
                result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell order by 7 asc limit %s", [lon, lat, query_limit])

            data = []
            for x in result:
                d = {
                    'site': x.sitename,
                    'rnc': x.rncname,
                    'lac': x.lac,
                    'rac': x.rac,
                    'plmn': x.plmn,
                    'ura': x.ura,
                    'lat': x.latitude,
                    'lon': x.longitude,
                    'distance': round(x.distance_miles, 2)
                }
                data.append(d)
        except:
            data = []
        return data;

    #return recent plans to show at about page
    # @limit: limiting site count
    def get_recent_plans(self, limit=100):
        data = []
        try:
            plans = NodebDatabase.objects.order_by('-plan_date', 'sitename')[:limit]
            for plan in plans:
                coordinate = self.get_coordinate(plan.sitename)
                d = { 'site': plan.sitename, 'coordinate': coordinate, 'rnc': plan.rncname, 'lac': plan.lac, 'rac': plan.rac, 'ura': plan.ura, 'rnc_ip': plan.rnc_main_address, 'plan_date': plan.plan_date, 'username': plan.username }
                data.append(d)
        except:
            pass

        return data

    # return lat, long formatted
    def get_coordinate(self, site=None):
        try:
            info = LatLonDatabase.objects.get(sitename=site)
            ss = str(round(info.latitude, 4)) + ', ' + str(round(info.longitude, 4))
        except:
            ss = None
        return ss



#about helper
class AboutHelper:

    #return gcell info for about page
    def get_ucell_info(self):
        try:
            ucell_info = Ucell.objects.raw("select count(distinct(rncname)) as id, count(distinct(sitename)) as s from nodeb_ucell")
            ucell = {
                'last_updated': SiteMetaHelper().get_site_meta('ucell_save_time'),
                'updated_by': SiteMetaHelper().get_site_meta('ucell_save_by'),
                'rnc_count': ucell_info[0].id,
                'site_count': ucell_info[0].s
            }
        except:
            ucell = {
                'last_updated': 'Data Not found',
                'ucell_update_by': 'Data Not found',
                'rnc_count': 'Data Not found',
                'site_count': 'Data Not found'
            }
        return ucell

    #get spu information for spu page
    def get_spu_info(self):
        try:
            spu_info = HwSPU.objects.raw("SELECT count(distinct(rncname)) as id from nodeb_hwspu")
            spu = {
                'last_updated': SiteMetaHelper().get_site_meta('spu_save_time'),
                'updated_by': SiteMetaHelper().get_site_meta('spu_save_by'),
                'rnc_count': spu_info[0].id
            }
        except:
            spu = {
                'last_updated': 'Data Not found',
                'updated_by': 'Data not found',
                'rnc_count': 'Data Not found'
            }
        return spu


#Search Helper class
class SearchHelper:
    def get_nearest_sites(self, s, query_limit = 400):
        data = []
        lat, lon = 0, 0
        if ',' in s:
            lat, lon = s.split(',')
        else:
            ucell = Ucell.objects.filter(sitename=s.upper()).distinct('sitename')
            if ucell.count() > 0:
                lat = ucell[0].latitude
                lon = ucell[0].longitude

        if float(lat) > 20 and float(lon) > 85:
            result = Ucell.objects.raw("select distinct on(distance_miles, sitename) id, rncname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles, rac, plmn, ura from nodeb_ucell order by 7 asc limit %s", [lon, lat, query_limit])
            for x in result:
                d = {
                    'site': x.sitename,
                    'rnc': x.rncname,
                    'lac': x.lac,
                    'rac': x.rac,
                    'plmn': x.plmn,
                    'ura': x.ura,
                    'lat': x.latitude,
                    'lon': x.longitude,
                    'distance': round(x.distance_miles, 2)
                }
                data.append(d)
        return data



#Download helper
class DownloadHelper:
    #return whole SPU
    def get_whole_spu(self):
        final_data = [['RNC Name', 'Vendor', 'Subrack', 'Slot', 'SCTP Link1', 'SCTP Link2']]
        try:
            result = HwSPU.objects.all().order_by('vendor', 'rncname', 'spu_srn', 'spu_sn', 'sctp_link1')
            for d in result:
                final_data.append( [ d.rncname, d.vendor, d.spu_srn, d.spu_sn, d.sctp_link1, d.sctp_link2 ] )

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    def get_whole_old_spu(self):
        final_data = [['NodeB', 'RNC', 'Vendor', 'Subrack_No', 'Slot_No', 'SCTP_LINK1', 'SCTP_LINK2']]
        try:
            result = OldHwSPU.objects.all().order_by('vendor', 'rncname', 'spu_srn', 'spu_sn', 'sctp_link1')
            for d in result:
                #final_data.append([d.rncname, d.vendor, d.spu_srn, d.spu_sn, d.sctp_link1, d.sctp_link2])
                final_data.append([d.sitename, d.rncname, d.vendor, d.spu_srn, d.spu_sn, d.sctp_link1, d.sctp_link2])

        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    #def return whole latlong db
    def get_whole_latlon(self):
        final_data = [['Site', 'Latitude', 'Longitude']]
        try:
            result = LatLonDatabase.objects.all().order_by('sitename')
            for d in result:
                final_data.append( [ d.sitename, d.latitude, d.longitude ] )
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    #return whole ucell
    def get_whole_ucell(self):
        final_data = [['NodeB Name', 'RNC Name', 'LAC', 'RAC', 'PLMN', 'URA ID', 'LAT', 'LON']]
        try:
            result = Ucell.objects.all().order_by('rncname', 'sitename')
            for d in result:
                final_data.append( [d.sitename, d.rncname, d.lac, d.rac, d.plmn, d.ura, d.latitude, d.longitude] )
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data