from django import forms

#hanling import and export of different files of nodeb
TYPES = (
    ('newucell', 'New Ucell'),
    ('ucell', 'Update Ucell'),
    ('spu', 'Update SPU'),
    #('newspu', 'New SPU')
)
class DataIO(forms.Form):
    file = forms.FileField()
    type = forms.ChoiceField(choices=TYPES, required=True)


#plan/lat-lon file uploader
class LatLonFileUploadForm(forms.Form):
    file = forms.FileField()
    plan_uploaded = forms.CharField(widget=forms.HiddenInput())

#Handle final plan download
class FinalPlanDonwloadForm(forms.Form):
    cnp_wr = forms.CharField(widget=forms.HiddenInput())
    ipwave_wr = forms.CharField(widget=forms.HiddenInput())
    site = forms.CharField(widget=forms.HiddenInput())
    rnc = forms.CharField(widget=forms.HiddenInput())
    lac = forms.CharField(widget=forms.HiddenInput())
    rac = forms.CharField(widget=forms.HiddenInput())
    plmn = forms.CharField(widget=forms.HiddenInput())
    ura = forms.CharField(widget=forms.HiddenInput())
    lat = forms.CharField(widget=forms.HiddenInput())
    lon = forms.CharField(widget=forms.HiddenInput())
    vendor = forms.CharField(widget=forms.HiddenInput())


#Search fileds in search page
class SearchSites(forms.Form):
    s = forms.CharField()



