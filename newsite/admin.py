from django.contrib import admin
from .models import Gcell, BscDatabase, AoipDatabase, EricssonBscDatabase, BscSiteDatabase, Coordinates

class GcellAdmin(admin.ModelAdmin):
    #fields = ['cellname', 'sitename', 'bscname', 'lac', 'trx', 'traffic']
    list_display = ('cellname', 'sitename', 'bscname', 'lac', 'latitude', 'longitude', 'trx', 'traffic')
    search_fields = ['sitename', 'cellname', 'bscname']


class BscDatabaseAdmin(admin.ModelAdmin):
    list_display = ('bscname', 'vendor', 'subrack_slot', 'main_address', 'subnet_mask', 'gateway_address', 'network_address')
    search_fields = ['bscname', 'vendor', 'main_address', 'network_address']


class EricssonBscDatabaseAdmin(admin.ModelAdmin):
    list_display = ('bscname', 'vendor', 'subrack_slot', 'main_address', 'subnet_mask', 'gateway_address', 'network_address')
    search_fields = ['bscname', 'vendor']


class AoipDatabaseAdmin(admin.ModelAdmin):
    list_display = ('cnp_wr', 'ipwave_wr', 'sitename', 'bscname', 'lac', 'subrack_slot', 'bsc_ip', 'username', 'plan_date')
    search_fields = ['sitename', 'bscname', 'lac', 'cnp_wr', 'ipwave_wr', 'username', 'plan_date']



class BscSiteDatabaseAdmin(admin.ModelAdmin):
    list_display = ('bscname', 'btscomtype', 'bts', 'btsip', 'bscip')
    search_fields = ['bscname', 'bscip', 'btsip']


class CoordinatesAdmin(admin.ModelAdmin):
    list_display = ('sitename', 'latitude', 'longitude')
    search_fields = ['sitename']


admin.site.register(Gcell, GcellAdmin)
admin.site.register(BscDatabase, BscDatabaseAdmin)
admin.site.register(EricssonBscDatabase, EricssonBscDatabaseAdmin)
admin.site.register(AoipDatabase, AoipDatabaseAdmin)
admin.site.register(BscSiteDatabase, BscSiteDatabaseAdmin)
admin.site.register(Coordinates, CoordinatesAdmin)