from django import forms

#handle first page excel file upload
class UploadFileForm(forms.Form):
    file = forms.FileField()
    plan_uploaded = forms.CharField(widget=forms.HiddenInput())


#Handle final plan download
class FinalPlanDonwload(forms.Form):
    cnp_wr = forms.CharField(widget=forms.HiddenInput())
    ipwave_wr = forms.CharField(widget=forms.HiddenInput())
    site = forms.CharField(widget=forms.HiddenInput())
    bsc = forms.CharField(widget=forms.HiddenInput())
    lac = forms.CharField(widget=forms.HiddenInput())
    lat = forms.CharField(widget=forms.HiddenInput())
    lon = forms.CharField(widget=forms.HiddenInput())


#hanling import and export
TYPES = (
    ('gcell', 'Gcell'),
    ('btsipdata', 'BTS IP Data'),
)
class DataIO(forms.Form):
    file = forms.FileField()
    type = forms.ChoiceField(choices=TYPES, required=True)



#Search fileds in search page
class SearchSites(forms.Form):
    s = forms.CharField()


#Rehoming form
class Rehoming(forms.Form):
    from_bsc = forms.CharField()
    to_bsc = forms.CharField()