from .models import Gcell, EricssonBscDatabase, BscDatabase, BscSiteDatabase, AoipDatabase, SiteMeta, Coordinates
import sys, random
from django.utils import timezone


class QueryGcell:
    def get_nearest_sites_by_latlon(self, lat, lon, limit):
        pass

    def get_nearest_sites_by_name(self, sitename):
        pass

    def get_sites_by(self, type, value, limit=None):
        pass



#it helps NewSitePlan View to optimize codes
class NewSitePlanHelper:
    # cell cleaning of excel files
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v

    def process_latlongfile(self, filehandler):
        data = []
        try:
            sheet = filehandler.get_sheet()
            sheet.map(self.cleanse_func)
            index = 0
            for row in sheet:
                if index > 0:
                    neighbor = self.get_nearest_site(row[3], row[4], row[1], row[2])
                    if 'site' in neighbor.keys():
                        d = {
                            'cnpwr': row[0],
                            'ipwavewr': row[1],
                            'site': row[2],
                            'lat': row[3],
                            'lon': row[4],
                            'nearest_site': neighbor.get('site'),
                            'proposed_bsc': neighbor.get('bsc'),
                            'proposed_lac': neighbor.get('lac'),
                            'distance': neighbor.get('distance')
                        }
                        data.append(d)
                index += 1

        except:
            pass
        return data



    # it returns nearest site based on lat lon
    #for pico it only returns nearest Huawei site
    def get_nearest_site(self, lat, lon, wr=None, site=None):
        d = {}
        pico = None

        try:
            if str(site[-1]).upper() == 'P':
                pico = 'pico'
        except:
            pico = None

        try:
            if pico == 'pico':
                result = Gcell.objects.raw("select distinct on(distance_miles, sitename) id, bscname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles from newsite_gcell where bscname like %s order by 7 asc limit 1", [lon, lat, '%HBS%'])
            else:
                result = Gcell.objects.raw("select distinct on(distance_miles, sitename) id, bscname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles from newsite_gcell order by 7 asc limit 1", [lon, lat])

            for x in result:
                d = {
                    'site': x.sitename,
                    'bsc': x.bscname,
                    'lac': x.lac,
                    'lat': x.latitude,
                    'lon': x.longitude,
                    'distance': round(x.distance_miles, 2)
                }
        except:
            d = {}

        return d


    # return bsc subrack and slot
    # it also consider load balance during choosing subrack and slot
    def get_bsc_subrack_and_ip(self, bsc_name):
        e_bss = EricssonBscDatabase.objects.values_list('bscname', flat=True).distinct()
        result = []
        if bsc_name in e_bss:
            info = EricssonBscDatabase.objects.get(bscname=bsc_name)
            if len(info.bscname) > 0:
                result = [info.bscname, info.main_address, info.network_address, info.subnet_mask,
                          info.gateway_address]

        else:
            sql = "SELECT COUNT(id) as id, bscip FROM newsite_bscsitedatabase WHERE btscomtype like '{}' AND bscname like '{}' group by bscip order by 1 limit 1".format(
                'PORTIP', bsc_name)
            try:
                subrack_ip = BscSiteDatabase.objects.raw(sql)
                info = BscDatabase.objects.filter(main_address=subrack_ip[0].bscip)
                if info.exists():
                    result = [ info[0].bscname, info[0].subrack_slot, info[0].main_address, info[0].gateway_address, info[0].subnet_mask ]

            except:
                result = self.get_subrack_slot_ip_of_new_bsc(bsc_name)

        return result

    #get subrack & slot from new bsc ip
    def get_subrack_slot_ip_of_new_bsc(self, bsc_name):
        result = []
        try:
            info = BscDatabase.objects.filter(bscname = bsc_name)
            random_num = random.randint(0, len(info)-1)
            bss = info[random_num]
            result = [ bss.bscname, bss.subrack_slot, bss.main_address, bss.gateway_address, bss.subnet_mask ]
        except:
            result = [bsc_name + ' not found', 'NA', 'NA', 'NA', 'NA']
        return result


    # saving record in database
    def save_log(self, site, bss, ipwave_wr, cnp_wr_no, lac, username=None):
        try:
            # keeping a record to make reflection on next plan on a same bsc
            e_bss = EricssonBscDatabase.objects.values_list('bscname', flat=True).distinct()
            if bss[0] not in e_bss:
                default_fields = {'btsip': "255.255.255.255", 'btscomtype': 'PORTIP'}
                BscSiteDatabase.objects.get_or_create(bscname=bss[0], bts=site, bscip=bss[2], defaults=default_fields)

            # saving at aoip database
            year = "{:%y}".format(timezone.now())
            cnp_wr = str(cnp_wr_no) + "-" + year
            remark = '2G: CNP Plan'
            cnp_wr_no = int(cnp_wr_no)

            AoipDatabase.objects.create(
                sitename=site, bscname=bss[0], lac=lac, subrack_slot=bss[1], bsc_ip=bss[2],
                bsc_mask=bss[3],  bsc_gateway=bss[4], cnp_wr=cnp_wr, cnp_wr_no=cnp_wr_no,
                ipwave_wr=ipwave_wr, username=username, remark=remark
            )
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    #save co-ordinates of the file
    def save_coordinates(self, site, lat, lon):
        defaults = {
            'latitude': float( lat ),
            'longitude': float ( lon )
        }
        try:
            obj = Coordinates.objects.get( sitename = site )
            for key, value in defaults.items():
                setattr(obj, key, value)
            obj.save()
        except Coordinates.DoesNotExist:
            new_values = {'sitename': site}
            new_values.update( defaults )
            obj = Coordinates(**new_values)
            obj.save()


#handling import and Export
class ImportExportHelper():
    gcell_meta_key = 'gcell_import_date'
    btsip_meta_key = 'btsip_import_date'

    gcell_columns = ['BSC_SITE', 'DIST', 'SITENAME', 'CELLNAME', 'CI', 'BSCNAME', 'LAC', 'ACTSTATUS', 'LAT', 'LON', 'TRX', 'TRAFFIC']
    ipdata_columns = ['BSCName', 'BTS', 'BTSCOMTYPE', 'BTSIP', 'BSCIP']

    #saving gcell from ip page
    def save_gcell(self, filehandler):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)
        success = 0
        failure = 0
        deleted_numbers = 0
        index = 1
        is_valid = True
        message = ''

        for row in sheet:
            if index == 1:
                col = 0
                for col_name in self.gcell_columns:
                    if ( col_name.upper() != row[col].strip().upper() ):
                        is_valid = False
                        message += "Error: Column Name Error!"
                        break
                    col += 1

                #if any column is invalid, it will break the loop
                #also if colum is found valid, then it will delte all the existing records
                if is_valid == True:
                    deleted = Gcell.objects.all().delete() #deleting Gcells
                    deleted_numbers = deleted[0]
                    self.keep_log(self.gcell_meta_key, "{:%d-%m-%Y}".format(timezone.now()), 'Model: Gcell')#saving log
                else:
                    break

            else:
                try:
                    #this default checking is actually forcing non number values to be skip
                    default_floats = { 'latitude': row[8], 'longitude': row[9], 'traffic': row[11] }
                    default_ints = { 'trx': row[10] }

                    for k, v in default_floats.items():
                        try:
                            default_floats[k] = float(v)
                        except:
                            default_floats[k] = 0

                    for k, v in default_ints.items():
                        try:
                            default_ints[k] = int(v)
                        except:
                            default_ints[k] = 0


                    g = Gcell(
                        bsc_site=row[0], dist=row[1], sitename=row[2], cellname=row[3], ci=int(row[4]), bscname=row[5],
                        lac=int(row[6]), activestatus=row[7], latitude=default_floats['latitude'],
                        longitude=default_floats['longitude'], trx=default_ints['trx'], traffic=default_floats['traffic']
                    )
                    g.save()
                    success += 1
                except:
                    failure += 1

            index += 1 # index increasing

        data = {
            'deleted': deleted_numbers,
            'success': success,
            'failure': failure,
            'is_valid': is_valid,
            'msg': message
        }

        return data

    # saving btsip from io page
    def save_btsip(self, filehandler):
        sheet = filehandler.get_sheet()
        sheet.map(self.cleanse_func)
        success = 0
        failure = 0
        index = 1
        is_valid = True
        deleted_numbers = 0


        for row in sheet:

            if index == 1:
                col = 0
                for col_name in self.ipdata_columns:
                    if ( col_name.upper() != row[col].strip().upper() ):
                        is_valid = False
                        break
                    col += 1

                #if any column is invalid, it will break the loop
                #also if colum is found valid, then it will delte all the existing records
                if is_valid == True:
                    deleted = BscSiteDatabase.objects.all().delete() #deleting Gcells
                    deleted_numbers = deleted[0]
                    # SiteMeta.
                    self.keep_log(self.btsip_meta_key, "{:%d-%m-%Y}".format(timezone.now()), 'Model: BscSiteDatabase')
                else:
                    break

            else:
                try:
                    ip = BscSiteDatabase(bscname=row[0], bts=row[1], btscomtype=row[2], btsip=row[3], bscip=row[4])
                    ip.save()
                    success += 1
                except:
                    failure += 1

            index += 1

        data = {
            'deleted': deleted_numbers,
            'success': success,
            'failure': failure,
            'is_valid': is_valid
        }
        return data

    # keeping trcase when saving Gcell & IP data
    def keep_log(self, key, value, comments):
        try:
            obj = SiteMeta.objects.get(meta_key=key)
            obj.meta_value = value
            obj.remarks = comments
            obj.save()
        except SiteMeta.DoesNotExist:
            obj = SiteMeta(meta_key=key, meta_value=value, remarks=comments)
            obj.save()

        # cell cleaning of excel files
    def cleanse_func(self, v):
        v = str(v).replace("&nbsp;", "")
        v = v.rstrip().strip()
        return v


#helping NearestSites view to fetch customzied query
class NearestSitesHelper:
    # returnlist of nearest sites based on distance
    def get_nearest_sites(self, lat, lon, site, query_limit=100):
        pico = None
        try:
            if str(site[-1]).upper() == 'P':
                pico = 'pico'
        except:
            pico = None
        try:
            if (pico == 'pico'):
                result = Gcell.objects.raw(
                    "select distinct on(distance_miles, sitename) id, bscname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles from newsite_gcell where bscname like %s order by 7 asc limit %s",
                    [lon, lat, '%HBS%', query_limit])
            else:
                result = Gcell.objects.raw(
                    "select distinct on(distance_miles, sitename) id, bscname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles from newsite_gcell order by 7 asc limit %s",
                    [lon, lat, query_limit])

            data = []
            self.bss_info = {}
            for x in result:
                d = {
                    'site': x.sitename,
                    'bsc': x.bscname,
                    'lac': x.lac,
                    'lat': x.latitude,
                    'lon': x.longitude,
                    'distance': round(x.distance_miles, 2)
                }
                data.append(d)
        except:
            data = []
        return data;

    #return bsc->lac->traffic/trx summation
    def get_bsc_info(self, bsc):
        result = Gcell.objects.raw(("select distinct(lac) as id, sum(trx) as aggtrx, sum(traffic) as aggtraffic from newsite_gcell where bscname like '{}' group by 1").format(bsc))
        data=[]
        total_traffic = 0
        total_trx = 0
        for x in result:
            d = {
                'lac':  x.id,
                'trx': x.aggtrx,
                'traffic': x.aggtraffic
            }
            data.append(d)
            total_traffic += x.aggtraffic
            total_trx += x.aggtrx

        total = {
            'lac': 'Total',
            'trx': total_trx,
            'traffic': total_traffic
        }
        data.append(total)
        return data


#helping About View at about page
class AboutHelper:
    #gcell related information
    def get_gcell_info(self):
        try:
            gcell_info = Gcell.objects.raw("select count(distinct(bscname)) as id, count(distinct(sitename)) as s, count(distinct(cellname)) as c from newsite_gcell")
            gcell = {
                'last_updated': SiteMeta.objects.get( meta_key = ImportExportHelper.gcell_meta_key ).meta_value,
                'bsc_count': gcell_info[0].id,
                'site_count': gcell_info[0].s,
                'cell_count': gcell_info[0].c
            }
        except:
            gcell = {
                'last_updated': 'Data Not found',
                'bsc_count': 'Data Not found',
                'site_count': 'Data Not found',
                'cell_count': 'Data Not found'
            }
        return gcell

    #return bts IP information
    #it is required for keeping load balace in HW bsc
    def get_btsip_info(self):
        try:
            ip_info = BscSiteDatabase.objects.raw("select count(distinct(bscname)) as id, count(distinct(bts)) as s from newsite_bscsitedatabase")
            bts_ip_data = {
                'last_updated': SiteMeta.objects.get( meta_key = ImportExportHelper.btsip_meta_key ).meta_value,
                'hw_bsc_count': ip_info[0].id,
                'hw_site_count': ip_info[0].s
            }
        except:
            bts_ip_data = {
                'last_updated': 'Data Not found',
                'hw_bsc_count': 'Data Not found',
                'hw_site_count': 'Data Not found'
            }

        return bts_ip_data

    #return recently planned AOIP sites
    def get_recent_plans(self, limit=100):
        try:
            recent_plans = AoipDatabase.objects.all().order_by('-plan_date')[:limit]
            plans = []
            for plan in recent_plans:
                coordinate = self.get_coordinate(plan.sitename)
                d = {'cnpwr': plan.cnp_wr, 'sitename':  plan.sitename, 'bsc': plan.bscname, 'lac':plan.lac, 'subrack_slot': plan.subrack_slot, 'date': plan.plan_date, 'user': plan.username, 'coordinate': coordinate }
                plans.append(d)

            recently_panned = {
                'heads': ['CNP WR', 'Site Name', 'Co-ordinate', 'BSC', 'LAC', 'Subrack & Slot', 'Plan Date', 'Plan by'],
                'plans': plans
            }
        except:
            recently_panned = {
                'heads': ['CNP WR', 'Site Name', 'BSC', 'LAC', 'Subrack & Slot', 'Plan Date', 'Plan by'],
                'plans': []
            }
        return recently_panned

    #return lat, long formatted
    def get_coordinate(self, site = None):
        try:
            info = Coordinates.objects.get(sitename = site)
            ss = str ( round( info.latitude, 4 ) ) + ', ' + str ( round(info.longitude, 4) )
        except:
            ss = None
        return ss

#it will help generate serach results
class SearchHelper:
    # returing nearest sites based on search criteria
    def get_nearest_sites(self, s, query_limit = 400):

        data = []
        lat, lon = 0, 0
        if ',' in s:
            lat, lon = s.split(',')
        else:
            gcell = Gcell.objects.filter(sitename=s.upper()).distinct('sitename')
            if gcell.count() > 0:
                lat = gcell[0].latitude
                lon = gcell[0].longitude

        if float(lat) > 20 and float(lon) > 85:
            result = Gcell.objects.raw(
                "select distinct on(distance_miles, sitename) id, bscname, sitename, lac, latitude, longitude, (point(longitude,latitude) <@> point(%s,%s)) as distance_miles from newsite_gcell order by 7 asc limit %s",
                [lon, lat, query_limit])
            for x in result:
                d = {
                    'site': x.sitename,
                    'bsc': x.bscname,
                    'lac': x.lac,
                    'lat': x.latitude,
                    'lon': x.longitude,
                    'distance': round(x.distance_miles, 2)
                }
                data.append(d)

        return data

    #it also includes traffic & TRX analylsis
    def get_bsc_info(self, bsc):
        result = Gcell.objects.raw(
            "select distinct(lac) as id, sum(trx) as aggtrx, sum(traffic) as aggtraffic from newsite_gcell where bscname like %s group by 1",
            [bsc])
        data = []
        total_traffic = 0
        total_trx = 0
        for x in result:
            d = {
                'lac': x.id,
                'trx': x.aggtrx,
                'traffic': x.aggtraffic
            }
            data.append(d)
            total_traffic += x.aggtraffic
            total_trx += x.aggtrx

        total = {
            'lac': 'Total',
            'trx': total_trx,
            'traffic': total_traffic
        }
        data.append(total)
        return data


#helping downloads
class DownloadHelper:
    def get_whole_latlon(self):
        final_data = [['Site', 'Latitude', 'Longitude']]
        try:
            result = Coordinates.objects.all().order_by('sitename')
            for d in result:
                final_data.append([d.sitename, d.latitude, d.longitude])
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data

    #return whole gcell
    def get_whole_gcell(self):
        final_data = [ ['BSC_SITE', 'DIST', 'SITENAME', 'CELLNAME', 'CI', 'BSCNAME', 'LAC', 'ACTSTATUS', 'LAT', 'LON', 'TRX', 'TRAFFIC'] ]
        try:
            result = Gcell.objects.all().order_by('bscname', 'sitename', 'cellname')
            for d in result:
                final_data.append( [d.bsc_site, d.dist, d.sitename, d.cellname, d.ci, d.bscname, d.lac, d.activestatus, d.latitude, d.longitude, d.trx, d.traffic] )
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return final_data
