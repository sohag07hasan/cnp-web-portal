from django.db import models
from datetime import date

#gcell info which is used for new site plan
class Gcell(models.Model):
    bsc_site = models.CharField(max_length=500, default=None, null=True, blank=True)
    dist = models.CharField(max_length=500, default=None, null=True, blank=True)
    sitename = models.CharField(max_length=500)
    cellname = models.CharField(max_length=500)
    ci = models.IntegerField(default=0)
    bscname = models.CharField(max_length=500)
    lac = models.IntegerField(default=0)
    activestatus = models.CharField(max_length=500, default=None, null=True, blank=True)
    #territory_id = models.IntegerField(default=None, null=True, blank=True)
    #territory = models.CharField(max_length=200, default=None, null=True, blank=True)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    trx = models.IntegerField(default=0, null=True, blank=True)
    traffic = models.FloatField(default=0, null=True, blank=True)

    def __str__(self):
        return self.cellname


#bsc database to keep ip address based on FG2C cards for huawei
class BscDatabase(models.Model):
    bscname = models.CharField(max_length=500)
    vendor = models.CharField(max_length=500, default=None, null=True, blank=True)
    subrack_slot = models.CharField(max_length=500)
    main_address = models.GenericIPAddressField(protocol='both', unpack_ipv4=True)
    gateway_address = models.GenericIPAddressField(protocol='both', unpack_ipv4=True)
    subnet_mask = models.GenericIPAddressField(protocol='both', unpack_ipv4=True)
    network_address = models.GenericIPAddressField(protocol='both', unpack_ipv4=True)

    def __str__(self):
        return self.bscname


#Ericsson database
class EricssonBscDatabase(models.Model):
    bscname = models.CharField(max_length=500)
    vendor = models.CharField(max_length=500, default=None, null=True, blank=True)
    subrack_slot = models.CharField(max_length=500, default=None, null=True, blank=True)
    main_address = models.CharField(max_length=500, default=None)
    gateway_address = models.CharField(max_length=500, default=None)
    subnet_mask = models.CharField(max_length=500, default=None)
    network_address = models.CharField(max_length=500, default=None)

    def __str__(self):
        return self.bscname


#HW Aoip Database
class BscSiteDatabase(models.Model):
    bscname = models.CharField(max_length=500)
    bts = models.CharField(max_length=500, default='NA')
    btscomtype = models.CharField(max_length=500, default='PORTIP')
    btsip = models.GenericIPAddressField(protocol='both', unpack_ipv4=True)
    bscip = models.GenericIPAddressField(protocol='both', unpack_ipv4=True)

    def __str__(self):
        return self.bscname


#aoip database
class AoipDatabase(models.Model):
    sitename = models.CharField(max_length=500)
    bscname = models.CharField(max_length=500)
    lac = models.IntegerField(default=0000)
    subrack_slot = models.CharField(max_length=500)
    bsc_ip = models.CharField(max_length=500)
    bsc_mask = models.CharField(max_length=500)
    bsc_gateway = models.CharField(max_length=500)
    cnp_wr = models.CharField(max_length=500, default=None, null=True, blank=True)
    cnp_wr_no = models.BigIntegerField(default=None, null=True, blank=True)
    ipwave_wr = models.CharField(default='NA', max_length=500)
    username = models.CharField(default='NA', max_length=500)
    plan_date = models.DateField(default=date.today)
    remark = models.TextField(default=None, null=True, blank=True)

    def __str__(self):
        return self.sitename


#site settings
class SiteMeta(models.Model):
    meta_key = models.CharField(max_length=500, default=None, blank=True, null=True)
    meta_value = models.CharField(max_length=500, default=None, blank=True, null=True)
    remarks = models.CharField(max_length=500, default=None, blank=True, null=True)


#Latlong database
class Coordinates(models.Model):
    sitename = models.CharField(max_length=500, default=None, blank=True, null=True)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)

    def __str__(self):
        return self.sitename
