/**
 * Created by mahibul.hasan on 19/04/2017.
 */

jQuery(document).ready(function ($) {
    var files;
    $('input[type=file]').on('change', prepareUpload);
    function prepareUpload(event){
      files = event.target.files;
    }

    $('form#ioform').on( 'submit', upLoadFiles );
    function upLoadFiles(event) {
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening
        var data = new FormData($('form#ioform')[0]);
        $.each(files, function(key, value){
            data.append(key, value);
        });
        data.append('type', $('#id_type').val()); // including excel type
        process_ajax(data, submitForm);
    }
   //function to handle ajax
     function process_ajax(input_data, success_function){
        var csrftoken = Cookies.get('csrftoken');
        $.ajax({
            xhr: function () {
              var xhr = new window.XMLHttpRequest();
              //Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                      $('div.progress').show();
                      var percentComplete = evt.loaded / evt.total * 100;
                      $('div.progress-bar').html(percentComplete + '%');
                      $('div.progress-bar').css({'width': percentComplete + '%'});

                  }
                }, false);
                return xhr;
            },
            url: NewSitePlanIO.ajax_url,
            method: 'POST',
            dataType: 'json',
            cache: false,
            async: true,
            processData: false,
            contentType: false,
            data: input_data,
            headers:{'X-CSRFToken': csrftoken},
            success: function (ajax_data) {
                success_function(ajax_data, input_data);
            },
            error: function () {
                alert('Error: Please check excel file again and try!');
            }
        });
     }
     function submitForm( ajax_data, input_data ){
         alert(ajax_data.success);
     }

})