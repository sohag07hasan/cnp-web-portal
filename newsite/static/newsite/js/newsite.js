 jQuery(document).ready(function ($) {

        var map;
        var all_bsc_info = [];

        function initMap(input_data, data){
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: new google.maps.LatLng(input_data.lat, input_data.lon),
                mapTypeId: 'terrain'
            });

            //creating marker of new site
            var marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: NewSitePlan.static_url + 'blue-dot.png',
                map: map,
                title: input_data.site
            });

            //color block
            var colors = ['blue', 'red', 'yellow', 'green', 'purple', 'oceanblue', 'pink', 'maron', 'grey', 'deepgreen', 'parrot', 'navyblue', 'oceanblue'];
            var color_index = 0;
            lac_list=[];
            color_list=[];

            for (var i=0; i<data.length; i++){
                if(!lac_list.includes(data[i].lac)){
                    lac_list.push(data[i].lac);
                    color_list.push(colors[color_index]);
                    color_index ++;
                    if(color_index == colors.length) color_index = 0; // forcely making color index zero
                }

                var latLng = new google.maps.LatLng(data[i].lat,data[i].lon);
                var marker_title = data[i].site + ' ' + data[i].bsc + ' ' + data[i].lac;
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: marker_title,
                    //icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
                    icon: NewSitePlan.static_url + color_list[lac_list.indexOf(data[i].lac)] + '.png'
                });


                //var input_button = document.createElement('span');
                var info_content = '<span class="infowindow-title">'+data[i].site+'</span> <input type="button" class="select_site input_button" value="Update Plan" /><br/>';
                info_content += '<div class="infowindow_bsc_info"><span class="infowindow-title-meta">'+data[i].bsc + ' ' + data[i].lac+'</span>';
                info_content += '</strong><br/>' +generate_info_window_content(data[i]);
                info_content += '</div>';
                var infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, 'click', (function(marker, info_content, map, d) {
                    return function () {
                        infowindow.setContent(info_content);
                        infowindow.open(map, marker);
                        setTimeout(function () {
                            $('.select_site').on('click', function (e) {
                                $('#proposed_site').html(d.site);
                                $('#proposed_bsc').html(d.bsc);
                                $('#proposed_lac').html(d.lac);
                                $('#proposed_distance').html(parseFloat(d.distance).toFixed(2));

                                /*
                                $(":input[name='proposed_site']").val(d.site);
                                $(":input[name='proposed_bsc']").val(d.bsc);
                                $(":input[name='proposed_lac']").val(d.lac);
                                */

                            });
                        }, 50);  //Wait for 0.05s for dom to be ready
                    }

                })(marker, info_content, map, data[i]));

            }

        }


        /**
         * Generate a title that will show when someone clicks a site icon on google map
         * */
        function generate_info_window_content(data){
            var bsc_info = get_bsc_information(data.bsc);
            var content = '';
            if(bsc_info.length > 0){
                content += '<table class="google_windows">';
                content += '<thead><tr><th>LAC</th><th>TRX</th><th>Traffic</th></tr></thead>';
                for(i=0; i<bsc_info.length; i++){
                    if(i == bsc_info.length-1){
                        content += '<tfoot><tr><th>'+bsc_info[i].lac+'</th><th>'+bsc_info[i].trx+'</th><th>'+parseFloat(bsc_info[i].traffic).toFixed(2)+'</th></tr></tfoot>';
                    }
                    else if(data.lac == bsc_info[i].lac){
                        content += '<tr class="same_lac"><td>'+bsc_info[i].lac+'</td><td>'+bsc_info[i].trx+'</td><td>'+parseFloat(bsc_info[i].traffic).toFixed(2)+'</td></tr>';
                    }
                    else{
                        content += '<tr><td>'+bsc_info[i].lac+'</td><td>'+bsc_info[i].trx+'</td><td>'+parseFloat(bsc_info[i].traffic).toFixed(2)+'</td></tr>';
                    }
                }
                content += '</table>';
            }
            return content;
        }


        //function to handle ajax
         function process_ajax(method, input_data, success_function){
            $.ajax({
                url: NewSitePlan.ajax_url,
                method: method,
                dataType: 'json',
                async: input_data.async,
                data: input_data,
                success: function (ajax_data) {
                    success_function(ajax_data, input_data);
                },
                error: function () {
                    alert('Error: Please clear browser cache and try again!');
                }
            });
         }

         /**
          * Processing ajax return info
          * */
         function process_nearest_sites(ajax_data, input_data){
             var map_data = ajax_data;
             if(ajax_data.length > 0){
                $('#proposed_site').html(ajax_data[0].site);
                $('#proposed_bsc').html(ajax_data[0].bsc);
                $('#proposed_lac').html(ajax_data[0].lac);
                $('#proposed_distance').html(parseFloat(ajax_data[0].distance).toFixed(2));
             }
             $('.search-result').removeClass('display_none'); // display query results

             initMap(input_data, map_data);
             $('div#map').css({'border': '1px solid blue'});
         }


         /**
          * this handles bsc information
          * */
         function process_bsc_data(ajax_data, input_data){
            if(ajax_data.length > 0){
               all_bsc_info[input_data.bscname] = ajax_data;
            }
         }


         /**
          * returns the traffic summary of a BSC ( lac wise )
          * */
         function get_bsc_information(bssname){
            if(all_bsc_info[bssname] == undefined) {
                var data = {
                    'hook': 'get_bsc_info',
                    'bscname': bssname,
                    'async': false
                }
                process_ajax('get', data, process_bsc_data);
            }
             return all_bsc_info[bssname];
         }

        //trigger the whole thing when an user clicks here
        $("input[name='submit_button']").click(function () {
            var data = {
                'site': $("input[name='site_name']").val(),
                'lat': $("input[name='site_lat']").val(),
                'lon': $("input[name='site_lon']").val(),
                'hook': 'nearestsites',
                'async': true
            }
            process_ajax('get', data, process_nearest_sites);
        });

        /**
         * Saving individual plan
         * */
        $("#save_plan").click(function () {
            var site_name = $("input[name='site_name']").val();
            var proposed_site  = $('#proposed_site').html();
            var proposed_bsc = $('#proposed_bsc').html();
            var proposed_lac = $('#proposed_lac').html();
            var icon_url = NewSitePlan.static_url + 'delete_icon.png';

            var new_tr = '<tr class="final_plan_site">';
            new_tr += '<td>' + site_name + '<input type="hidden" name="site_name" value="' + site_name + '">' + '</td>';
            new_tr += '<td>' + proposed_site + '<input type="hidden" name="proposed_site" value="' + proposed_site + '">' + '</td>';
            new_tr += '<td>' + proposed_bsc + '<input type="hidden" name="proposed_bsc" value="' + proposed_bsc + '">' + '</td>';
            new_tr += '<td>' + proposed_lac + '<input type="hidden" name="proposed_lac" value="' + proposed_lac + '">' + '</td>';
            new_tr += '<td>' + '<img class="delete_final_plan" alt="Delete" src="' + icon_url + '" />' + '</td>';
            new_tr += '</tr>';
            $('#final_plan_last_tr').before(new_tr);

            $('img.delete_final_plan').off('click');
            $('img.delete_final_plan').on('click',function () {
                result = window.confirm("Do you really want to delete?")
                if (result) {
                  $(this).parents('tr.final_plan_site').remove();
                }
            });

        });


 });
