 jQuery(document).ready(function ($) {

        var map;
        var all_bsc_info = [];
        function initMap(input_data, data){

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: new google.maps.LatLng(input_data.lat, input_data.lon),
                mapTypeId: 'terrain'
            });

            //creating marker of new site
            var marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: NewSitePlan.static_url + 'images/google_icons/blue-dot.png',
                map: map,
                title: input_data.site
            });

            //color block
            var colors = ['blue', 'red', 'yellow', 'green', 'purple', 'oceanblue', 'pink', 'maron', 'grey', 'deepgreen', 'parrot', 'navyblue', 'oceanblue'];
            var color_index = 0;
            lac_list=[];
            color_list=[];


            for (var i=0; i<data.length; i++){
                if(!lac_list.includes(data[i].lac)){
                    lac_list.push(data[i].lac);
                    color_list.push(colors[color_index]);
                    color_index ++;
                    if(color_index == colors.length) color_index = 0; // forcely making color index zero
                }

                var latLng = new google.maps.LatLng(data[i].lat,data[i].lon);
                var marker_title = data[i].site + ' ' + data[i].bsc + ' ' + data[i].lac;
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: marker_title,
                    icon: NewSitePlan.static_url + 'images/google_icons/' + color_list[lac_list.indexOf(data[i].lac)] + '.png'
                });


               // var info_content = '<h4>'+data[i].site+ ' - '+data[i].bsc+' - '+data[i].lac+'</h4>';
                var info_content = '<h4>'+data[i].site+ ' - '+data[i].bsc+' - <input type="button" class="btn btn-success select_site" value="Update Plan" /></h4>';
                info_content += generate_info_window_content(data[i]);

                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(marker, 'click', (function(marker, info_content, map, d) {
                    return function () {
                        infowindow.setContent(info_content);
                        infowindow.open(map, marker);
                        setTimeout(function () {
                            $('.select_site').on('click', function (e) {
                                var row = $('#'+input_data.active_row);
                                row.find('td.nearest_site').html(d.site);
                                row.find('td.proposed_bsc').find('span').html(d.bsc);
                                row.find('td.proposed_lac').find('span').html(d.lac);
                                row.find('td.nearest_site_distance').html(d.distance);
                                //input values
                                row.find("input[name='bsc']").val(d.bsc);
                                row.find("input[name='lac']").val(d.lac);
                                row.addClass('success');
                                row.removeClass('active');
                            });
                        }, 50);  //Wait for 0.05s for dom to be ready
                    }

                })(marker, info_content, map, data[i]));

            }

        }


         // Generate a title that will show when someone clicks a site icon on google map
         function generate_info_window_content(data){
            var bsc_info = get_bsc_information(data.bsc);
            var content = '';
            if(bsc_info.length > 0){
                content += '<table class="table table-sm">';
                content += '<thead><tr><th>LAC</th><th>TRX</th><th>Traffic</th></tr></thead>';
                for(i=0; i<bsc_info.length; i++){
                    if(i == bsc_info.length-1){
                        content += '<tfoot><tr><th>'+bsc_info[i].lac+'</th><th>'+bsc_info[i].trx+'</th><th>'+parseFloat(bsc_info[i].traffic).toFixed(2)+'</th></tr> <!-- <tr>  <td><input type="button" class="btn btn-success select_site" value="Update Plan" /></td></tr> --> </tfoot>';
                    }
                    else if(data.lac == bsc_info[i].lac){
                        content += '<tr class="active"><td>'+bsc_info[i].lac+'</td><td>'+bsc_info[i].trx+'</td><td>'+parseFloat(bsc_info[i].traffic).toFixed(2)+'</td></tr>';
                    }
                    else{
                        content += '<tr><td>'+bsc_info[i].lac+'</td><td>'+bsc_info[i].trx+'</td><td>'+parseFloat(bsc_info[i].traffic).toFixed(2)+'</td></tr>';
                    }
                }
                content += '</table>';
            }
            return content;
        }

        //function to handle ajax
         function process_ajax(method, input_data, success_function){
            $.ajax({
                url: NewSitePlan.ajax_url,
                method: method,
                dataType: 'json',
                async: input_data.async,
                data: input_data,
                success: function (ajax_data) {
                    success_function(ajax_data, input_data);
                },
                error: function () {
                    alert('Error: Please check excel file again and try!');
                }
            });
         }

         //process ajax and initiate map
         function process_nearest_sites(ajax_data, input_data){
             initMap(input_data, ajax_data);
             $('div#map').css({'border': '1px solid blue'});
         }

         //this will handle bsc information
         function process_bsc_data(ajax_data, input_data){
            if(ajax_data.length > 0){
               all_bsc_info[input_data.bscname] = ajax_data;
            }
         }

         //ajax query to give bsc info
         function get_bsc_information(bssname){
            if(all_bsc_info[bssname] == undefined) {
                var data = {
                    'hook': 'get_bsc_info',
                    'bscname': bssname,
                    'async': false
                }
                process_ajax('get', data, process_bsc_data);
            }
             return all_bsc_info[bssname];
         }

        //trigger the whole thing when an user clicks here
        $("button.justify-individual-plan").click(function () {
            var row_id = $(this).attr('name');
            //alert(row_id);
            var row = $('#'+row_id);
            //alert(row.html());
            row.find('img.ajax_loader').removeClass('display_none');
            row.addClass('active');

            var data = {
                'site': row.find("input[name='site']").val(),
                'lat': row.find('td.input_lat').text(),
                'lon':  row.find('td.input_lon').text(),
                'hook': 'nearestsites',
                'active_row': row_id,
                'async': true
            };


            process_ajax('get', data, process_nearest_sites);
            row.find('img.ajax_loader').addClass('display_none');
            return false;
        });

 });
