/**
 * Created by mahibul.hasan on 24/04/2017.
 */
jQuery(document).ready(function ($) {
    $('.secondary_navigation').find('li').click(function (evt) {
        var div_id = $(this).attr('tab');
        $('.nav_details').addClass('display_none');
        $('.secondary_navigation').find('li').removeClass('active');
        $(this).addClass('active');
        $('#'+div_id).removeClass('display_none');
        return false;
    });
});
