from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
from django.views import View
import django_excel as excel
from .forms import UploadFileForm, FinalPlanDonwload, DataIO, SearchSites, Rehoming
from django.contrib.auth.mixins import LoginRequiredMixin
from .helper import NewSitePlanHelper, NearestSitesHelper, AboutHelper, SearchHelper, ImportExportHelper, DownloadHelper


#index page
class NewSitePlan(LoginRequiredMixin, View):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        form = UploadFileForm(initial={'plan_uploaded':'Y'})
        context = {'title': 'CNP Plan: 2G Site'}
        return render(request, 'newsite/index.html', {'form': form, 'context': context})

    def post(self, request):
        self.request = request
        context = {'title': 'CNP Plan: 2G Site'}
        if request.POST.get('plan_uploaded') == 'Y' and request.FILES['file']:
            form = UploadFileForm(request.POST, request.FILES)
            planed_form = FinalPlanDonwload

            if form.is_valid():
                data = NewSitePlanHelper().process_latlongfile( request.FILES['file'] )
                return render(request, 'newsite/map.html', {'data': data, 'form': planed_form, 'context': context})

            else:
                form = UploadFileForm
                return render(request, 'newsite/index.html', {'form': form, 'context': context})

        else:
            form = FinalPlanDonwload(request.POST)
            if form.is_valid():
                header = ['WRNO', 'Site Name', 'Target NE(BSC/RNC Name)', 'Subrack,Slot', 'RNC/BSC FG2C (Main) IP Address', 'Subnet mask RNC', 'BSC/RNC Gateway', 'LAC']
                d = [header]
                sites = request.POST.getlist('site')
                proposed_bscs = request.POST.getlist('bsc')
                proposed_lacs = request.POST.getlist('lac')
                ipwave_wrs = request.POST.getlist('ipwave_wr')
                cnp_wrs = request.POST.getlist('cnp_wr')
                lats = request.POST.getlist('lat')
                lons = request.POST.getlist('lon')

                for i in range(len(sites)):
                    bss_info = NewSitePlanHelper().get_bsc_subrack_and_ip(proposed_bscs[i])
                    if len(bss_info) > 0:
                        d.append([ipwave_wrs[i], sites[i], bss_info[0], bss_info[1], bss_info[2], bss_info[4], bss_info[3], proposed_lacs[i]])
                        NewSitePlanHelper().save_log(sites[i], bss_info, ipwave_wrs[i], cnp_wrs[i], proposed_lacs[i], self.request.user.username)  # saving log to database
                        NewSitePlanHelper().save_coordinates(sites[i], lats[i], lons[i])  # saving co-ordinates

                year = "{:%y}".format(timezone.now())
                attachment_name = "WORK_REQUEST_NO_new_site_{}-{}-AbisOIP.xls".format(cnp_wrs[0], year)
                return excel.make_response_from_array(d, 'xls', file_name=attachment_name)
            else:
                return HttpResponse(form.errors)


# ajax handling
class NearestSites(View):
    def get(self, request):
        self.request = request
        if request.GET.get('hook') == 'nearestsites':
            limit = 100
            lat = request.GET.get('lat', None)
            lon = request.GET.get('lon', None)
            site = request.GET.get('site', None)
            data = NearestSitesHelper().get_nearest_sites( lat, lon, site, limit);

        elif request.GET.get('hook') == 'get_bsc_info':
            data = NearestSitesHelper().get_bsc_info( request.GET.get('bscname', None) )

        else:
            data = []
        return JsonResponse(data, safe=False)  # default function to handle json form django 1.8



# about page
class About(View):
    def get(self, request):
        context = {
            'title': 'CNP Plan: about',
            'bts_ip_data': AboutHelper().get_btsip_info(),
            'gcell': AboutHelper().get_gcell_info() ,
            'recently_panned': AboutHelper().get_recent_plans(100)
        }
        return render(request, 'newsite/about.html', {'context': context})


# data import/export
class ImportExport(LoginRequiredMixin, View):
    login_url = '/login/'
    redirect_field_name = 'next'
    permission_denied_message = 'You are not authorized..'

    def get(self, request):
        form = DataIO
        context = {'title': 'CNP Plan: IO', 'form': form }
        return render(request, 'newsite/io.html', {'context': context})

    def post(self, request):
        form = DataIO(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['type'] == 'gcell':
                data = ImportExportHelper().save_gcell( request.FILES['file'] )
            else:
                data = ImportExportHelper().save_btsip( request.FILES['file'] )
        else:
            data = {
                'is_valid': False
            }

        response = JsonResponse(data)
        #response.set_cookie(key='row_count', value=success, path='/')
        return response


#Handling Search functionality at search page
class Search(View):
    def get(self, request):
        if request.is_ajax():
            data = []
            if request.GET.get('hook', None) == 'search':
                form = SearchSites(request.GET)
                if form.is_valid():
                    limit = 350
                    data = SearchHelper().get_nearest_sites( form.cleaned_data['s'].strip(), limit )
                    return JsonResponse(data, safe=False)
            else:
                bsc = request.GET.get('bscname')
                data = SearchHelper().get_bsc_info( request.GET.get('bscname') )

            return JsonResponse(data, safe=False)

        else:
            form = SearchSites
            context = { 'title': 'CNP Plan: Search', 'form': form }
            return render(request, 'newsite/search.html', {'context': context})


#dashboard view
class Dashboard(View):
    def get(self, request):
        context = {'title': 'CNP Plan: Dashboard'}
        return render(request, 'newsite/dashboard.html', {'context': context})


class Rehome(View):
    def get(self, request):
        form = Rehoming
        context = {'title': 'CNP Plan: Rehome', 'form': form}
        return render(request, 'newsite/rehome.html', {'context': context})

    def post(self, request):
        pass


#class Download
class Download(View):
    def get(self, request, type=None):
        download_date = "{:%Y-%m-%d}".format(timezone.now())
        if type == 'latlon':
            data = DownloadHelper().get_whole_latlon()
            return excel.make_response_from_array(data, 'xlsx', file_name='LatLon_DB_' + download_date)
        elif type == 'gcell':
            data = DownloadHelper().get_whole_gcell()
            return excel.make_response_from_array(data, 'xlsx', file_name='GCELL_' + download_date)
        else:
            return HttpResponseRedirect(reverse('newsite:io'))