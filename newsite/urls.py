from django.conf.urls import url
from .views import NearestSites, NewSitePlan, About, ImportExport, Search, Dashboard, Rehome, Download

app_name = 'newsite'
urlpatterns = [
    url(r'^$', NewSitePlan.as_view(), name='new-site-plan'),
    url(r'^nearestsite/$', NearestSites.as_view(), name='nearest-sites'),
    url(r'^about/$', About.as_view(), name='about'),
    url(r'^io/$', ImportExport.as_view(), name='io'),
    url(r'^search/$', Search.as_view(), name='search'),
    url(r'^dashboard/$', Dashboard.as_view(), name='dashboard'),
    url(r'^rehome/$', Rehome.as_view(), name='rehome'),
    url(r'^download/(?P<type>[a-z]+)/$', Download.as_view(), name='download')
]