from django.shortcuts import render
from django.views import View

# Create your views here.
class Home(View):
    def get(self, request):
        context = {'title': 'Champions Never Panic'}
        return render(request, 'log/home.html', {'context': context})