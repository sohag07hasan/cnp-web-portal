"""cnp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from log.forms import LoginForm

urlpatterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^newsite/', include('newsite.urls')),
    url(r'^report/', include('report.urls')),
    url(r'^nodeb/', include('nodeb.urls')),
    url(r'^process/', include('process.urls')),
    url(r'^igw/', include('igw.urls')),
    url(r'', include('log.urls')),
    url(r'^login/', auth_views.login, name='login', kwargs={'template_name': 'log/login.html', 'authentication_form': LoginForm}),
    url(r'^logout/', auth_views.logout, name='logout', kwargs={'next_page': '/login'} ),
]
