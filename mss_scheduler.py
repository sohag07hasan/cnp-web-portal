import sys, os, dateparser
from datetime import date, timedelta, datetime

sys.path.append(os.path.join(os.path.dirname(__file__), 'cnp')) # adding cnp folder/project
os.environ["DJANGO_SETTINGS_MODULE"] = "cnp.settings" # cnp settings

import django
from django.conf import settings
from django.db.models import Max, Sum, Count, Avg, F, Value
django.setup()

from report.models import SummaryTraffic #importing summary traffic


class MssScheduler:
    def __init__(self):
        self.path = ''
