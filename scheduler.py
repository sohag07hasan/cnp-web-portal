'''

Gazipur: site 2
Savar: Site 1
Jessore: Site 3

SQL = ATL.objects.valuels('day').fiilter(day__range=(fromday, today)).annotate(total_in=F('site_1_in')+F('site_2_in')).order_by('-total_in')[0]
this will return the max total in value of the day

 N = ATL.objects.filter(day='2018-02-05').values('site_1_in', 'site_2_in', 'site_1_out', 'site_2_out').annotate(total_in=F('site_1_in')+F('site_2_in')).order_by('-total_in')

'''

import sys, os, dateparser
from datetime import date, timedelta, datetime

sys.path.append(os.path.join(os.path.dirname(__file__), 'cnp')) # adding cnp folder/project
os.environ["DJANGO_SETTINGS_MODULE"] = "cnp.settings" # cnp settings

import django
from django.conf import settings
from django.db.models import Max, Sum, Count, Avg, F, Value
django.setup()

from igw.models import ATL, LevelThree, FiberAtHome, BtCl, BdIx, Summit, NovoNix, DailyPeakThroughput, Capacity, MangoTel
#from igw.helper import AtlHelper, SummitHelper, LevelThreeHelper, BtclHelper, FiberAtHomeHelper

class IgwReportAutomation:
    def __init__(self):
        pass

    #make daily throughput report
    def generate_daily_throughput_report(self, iigs, fromDay = None, toDay = None):
        info = {'atl': ATL, 'btcl': BtCl, 'summit': Summit, 'fiberathome': FiberAtHome, 'levelthree': LevelThree, 'bdix': BdIx, 'novonix': NovoNix, 'mangotel': MangoTel}

        #iigs_traffic = self.get_pick_throughput(info, fromDay, toDay)
        iigs_traffic = self.get_pick_throughput(info, fromDay, toDay, iigs)

        for iig, datas in iigs_traffic.items():
            for data in datas:
                #print(data)
                try:
                    obj = DailyPeakThroughput.objects.get( day=data['day'], vendor=data['vendor'] )
                    obj.gp_in = data['gp_in']
                    obj.gp_out = data['gp_out']
                    obj.sav_in = data['sav_in']
                    obj.sav_out = data['sav_out']
                    obj.js_in = data['js_in']
                    obj.js_out = data['js_out']
                    obj.total_in = data['total_in']
                    obj.total_out = data['total_out']
                    obj.save()
                except:
                    obj = DailyPeakThroughput(**data)
                    obj.save()

        print("report created")

        self.update_capacity()


    #update capacity
    def update_capacity(self):
        print("Updating capacity....")

        today = date.today()
        last_day_info = Capacity.objects.values('day').order_by('-day')[:1]
        last_day = last_day_info[0].get('day')
        if last_day < today:
            try:
                entries = Capacity.objects.values('atl', 'levelthree', 'summit', 'fiberathome', 'btcl', 'bdix', 'novonix', 'ggc', 'fna', 'akamai', 'mangotel').filter(day = last_day)
                current_capacity = entries[0]
                while (today >= last_day):
                    last_day = last_day + timedelta(1)
                    current_capacity['day'] = last_day.strftime("%Y-%m-%d")
                    obj = Capacity(**current_capacity)
                    obj.save()
            except:
                print("Unexpected error:", sys.exc_info()[0])
                raise
        print("Capacity Updated")



    #get pick thorughput
    def get_pick_throughput(self, info = {}, fromDay = None, toDay = None, iigs=[]):
        final_data = {}

        for iig, IIG in info.items():
            if iig in iigs:
                final_data[iig] = []
                try:
                    #results = IIG.objects.values('day').filter(day__range=(fromDay, toDay)).annotate(gp_in=Max('site_1_in'), gp_out=Max('site_1_in'), sav_in=Max('site_2_in'), sav_out=Max('site_2_out'))
                    #results = IIG.objects.filter(day__range=(fromDay, toDay)).values('day', 'site_1_in', 'site_2_in', 'site_1_out', 'site_2_out').annotate(total_in=F('site_1_in') + F('site_2_in'), total_out=F('site_1_out') + F('site_2_out')).order_by('day', '-total_in').distinct('day')
                    results = IIG.objects.filter(day__range=(fromDay, toDay)).values('day', 'site_1_in', 'site_2_in', 'site_1_out', 'site_2_out', 'site_3_in', 'site_3_out').annotate(total_in=F('site_1_in') + F('site_2_in') + F('site_3_in'), total_out=F('site_1_out') + F('site_2_out') + F('site_3_out')).order_by('day', '-total_in').distinct('day')
                    for r in results:
                        data = {
                            'day': r.get('day').strftime("%Y-%m-%d"),
                            'vendor': iig,
                            'gp_in': r.get('site_2_in'),
                            'gp_out': r.get('site_2_out'),
                            'sav_in': r.get('site_1_in'),
                            'sav_out': r.get('site_1_out'),
                            'js_in': r.get('site_3_in'),
                            'js_out': r.get('site_3_out'),
                            'total_in': r.get('total_in'),
                            'total_out': r.get('total_out')
                        }
                        final_data[iig].append(data)

                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    raise

        return final_data




if __name__ == "__main__":
    try:
        day1 = sys.argv[1]
        fromDay = dateparser.parse(day1).strftime("%Y-%m-%d")
    except:
        day1 = yesterday = date.today() - timedelta(1)
        fromDay = day1.strftime("%Y-%m-%d")
    try:
        day2 = sys.argv[2]
        toDay = dateparser.parse(day2).strftime("%Y-%m-%d")
    except:
        toDay = fromDay

    try:
        iig = sys.argv[3]
    except:
        #info = {'atl': ATL, 'btcl': BtCl, 'summit': Summit, 'fiberathome': FiberAtHome, 'levelthree': LevelThree,'bdix': BdIx, 'novonix': NovoNix}
        iig = 'atl-summit-btcl-fiberathome-levelthree-bdix-novonix-mangotel'

    print("Generating Reports: " + fromDay + " -> " + toDay)
    Report = IgwReportAutomation()
    Report.generate_daily_throughput_report(iig.split('-'), fromDay, toDay)
